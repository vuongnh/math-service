### 1. Build jar file application
1. buid with profile dev (application-dev.yml)
```
mvn -Pdev package
```
2. buid with profile prod (application-prod.yml)
```
mvn -Pprod package
```
### 2. Build docker image
```
docker build -t $(name) .
```
### 3. Tag docker image
```
docker tag $(name) {path-repository}/$(name):$(version)
```
### 4. Push docker image to dockerhub
```
docker push {path-repository}/$(name):$(version)
```
### 5. Deploy on server
```
docker run -d -p $(port host):$(port container) --name=$(container-name) --env-file $(path to env file) --mount type=bind,src=/home/tuannp/studynow/logs/math_service,target=/app/storage/logs/ tinyporo/$(name):$(version)
```
