FROM openjdk:8-jre-alpine
MAINTAINER LIENLQ3 <lienlq3@topica.edu.vn>
EXPOSE $PORT
WORKDIR /app
COPY deploy/* /app/
CMD java $JAVA_OPTS -jar /app/mathservice-1.0.jar --spring.config.location=/app/application.yml