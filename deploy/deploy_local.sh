
export NAME=math_service
export VERSION=1.2.7


# build exec jar
mvn clean install
cp target/*.jar deploy/

# build docker
docker build -t $NAME:$VERSION .
