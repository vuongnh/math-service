export BASE_DIR=/home/lienlq3/services/math
export NAME=math_service_dev
export VERSION=1.0.1
export ENV_PATH=$BASE_DIR/deploy/env_file.conf

echo $ENV_PATH

# build exec jar
mvn clean install

cp target/*.jar deploy/

# commit
git add .
git commit -m "Forward dashboard api for logic 2"
git push --set-upstream origin dev

# copy to server
ssh math "mkdir $BASE_DIR"
scp -r deploy math:$BASE_DIR
scp Dockerfile math:$BASE_DIR

# build docker
ssh math "cd $BASE_DIR && docker build -t $NAME:$VERSION ."

# run docker
ssh math "docker stop $NAME"
ssh math "docker rm $NAME"
ssh math "docker run -d --net=host --name=$NAME --env-file=$ENV_PATH $NAME:$VERSION"
ssh math "docker logs -f $NAME"