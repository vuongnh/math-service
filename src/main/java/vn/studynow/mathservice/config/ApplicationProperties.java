package vn.studynow.mathservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Data
public class ApplicationProperties {

    private LearningService learningService = new LearningService();
    private TrackingService trackingService = new TrackingService();
    private SearchService searchService = new SearchService();
    private AbTestingService abTestingService = new AbTestingService();
    private NotificationService notificationService = new NotificationService();
    private QandaService qandaService = new QandaService();
    private BookService bookService = new BookService();
    private BrahmService brahmService = new BrahmService();

    public static class LearningService {

        private String host;
        private String apiGetLearningNodesLevel0;
        private String apiGetLearningNodeMaterials;
        private String apiGetMaterialDetail;
        private String apiGetQuizDetail;
        private String apiGetLearningPath;
        private String apiGetEntranceExam;
        private String apiGetFinalExam;
        private String apiGetPracticeMotivation;
        private String apiGetEntranceExamMotivation;
        private String apiGetFinalExamMotivation;
        private String apiGetEntranceExamPopup;
        private String apiGetLearningNodesLevel6;
        private String apiGetLearningNodeLevel61Motivation;
        private String apiGetLearningNodeLevel62Motivation;
        private String apiGetLearningNodeLevel63Motivation;
        private String apiGetUserAvatar;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getApiGetLearningNodesLevel0() {
            return host + apiGetLearningNodesLevel0;
        }

        public void setApiGetLearningNodesLevel0(String apiGetLearningNodesLevel0) { this.apiGetLearningNodesLevel0 = apiGetLearningNodesLevel0; }

        public String getApiGetLearningNodeMaterials() { return host + apiGetLearningNodeMaterials; }

        public void setApiGetLearningNodeMaterials(String apiGetLearningNodeMaterials) { this.apiGetLearningNodeMaterials = apiGetLearningNodeMaterials; }

        public String getApiGetQuizDetail() {
            return host + apiGetQuizDetail;
        }

        public void setApiGetQuizDetail(String apiGetQuizDetail) {
            this.apiGetQuizDetail = apiGetQuizDetail;
        }

        public String getApiGetLearningPath() {
            return host + apiGetLearningPath;
        }

        public void setApiGetLearningPath(String apiGetLearningPath) {
            this.apiGetLearningPath = apiGetLearningPath;
        }

        public String getApiGetEntranceExam() {
            return host + apiGetEntranceExam;
        }

        public void setApiGetEntranceExam(String apiGetEntranceExam) {
            this.apiGetEntranceExam = apiGetEntranceExam;
        }

        public String getApiGetFinalExam() {
            return host + apiGetFinalExam;
        }

        public void setApiGetFinalExam(String apiGetFinalExam) {
            this.apiGetFinalExam = apiGetFinalExam;
        }

        public String getApiGetMaterialDetail() { return host + apiGetMaterialDetail; }

        public void setApiGetMaterialDetail(String apiGetMaterialDetail) { this.apiGetMaterialDetail = apiGetMaterialDetail; }

        public String getApiGetEntranceExamMotivation() { return host + apiGetEntranceExamMotivation; }

        public void setApiGetEntranceExamMotivation(String apiGetEntranceExamMotivation) { this.apiGetEntranceExamMotivation = apiGetEntranceExamMotivation; }

        public String getApiGetFinalExamMotivation() { return host + apiGetFinalExamMotivation; }

        public void setApiGetFinalExamMotivation(String apiGetFinalExamMotivation) { this.apiGetFinalExamMotivation = apiGetFinalExamMotivation; }

        public String getApiGetPracticeMotivation() { return host + apiGetPracticeMotivation; }

        public void setApiGetPracticeMotivation(String apiGetPracticeMotivation) { this.apiGetPracticeMotivation = apiGetPracticeMotivation; }

        public String getApiGetEntranceExamPopup() { return host + apiGetEntranceExamPopup; }

        public void setApiGetEntranceExamPopup(String apiGetEntranceExamPopup) { this.apiGetEntranceExamPopup = apiGetEntranceExamPopup; }

        public void setApiGetLearningNodesLevel6(String apiGetLearningNodesLevel6) {
            this.apiGetLearningNodesLevel6 = apiGetLearningNodesLevel6;
        }

        public String getApiGetLearningNodeLevel61Motivation() {
            return host +  apiGetLearningNodeLevel61Motivation;
        }

        public void setApiGetLearningNodeLevel61Motivation(String apiGetLearningNodeLevel61Motivation) {
            this.apiGetLearningNodeLevel61Motivation = apiGetLearningNodeLevel61Motivation;
        }

        public String getApiGetLearningNodeLevel62Motivation() {
            return host + apiGetLearningNodeLevel62Motivation;
        }

        public void setApiGetLearningNodeLevel62Motivation(String apiGetLearningNodeLevel62Motivation) {
            this.apiGetLearningNodeLevel62Motivation = apiGetLearningNodeLevel62Motivation;
        }

        public String getApiGetLearningNodeLevel63Motivation() {
            return host + apiGetLearningNodeLevel63Motivation;
        }

        public void setApiGetLearningNodeLevel63Motivation(String apiGetLearningNodeLevel63Motivation) {
            this.apiGetLearningNodeLevel63Motivation = apiGetLearningNodeLevel63Motivation;
        }

        public String getApiGetLearningNodesLevel6() {
            return host + apiGetLearningNodesLevel6;
        }

        public String getApiGetUserAvatar() {
            return host + apiGetUserAvatar;
        }

        public void setApiGetUserAvatar(String apiGetUserAvatar) {
            this.apiGetUserAvatar = apiGetUserAvatar;
        }
    }

    public static class TrackingService {
        private String host;
        private String apiTrackingGetEntranceExam;

        public void setHost(String host) {
            this.host = host;
        }

        public String getHost() {
            return host;
        }

        public void setApiTrackingGetEntranceExam(String apiTrackingGetEntranceExam) {
            this.apiTrackingGetEntranceExam = apiTrackingGetEntranceExam;
        }

        public String getApiTrackingGetEntranceExam() {
            return host + apiTrackingGetEntranceExam;
        }
    }

    public static class SearchService {
        private String host;
        private String apiMathConcept;
        private String apiSuggestConcept;
        private String apiViewConcept;
        private String apiGetConcept;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getApiMathConcept() {
            return host + apiMathConcept;
        }

        public void setApiMathConcept(String apiMathConcept) {
            this.apiMathConcept = apiMathConcept;
        }

        public String getApiSuggestConcept() {
            return host + apiSuggestConcept;
        }

        public void setApiSuggestConcept(String apiSuggestConcept) {
            this.apiSuggestConcept = apiSuggestConcept;
        }

        public String getApiViewConcept() {
            return host + apiViewConcept;
        }

        public void setApiViewConcept(String apiViewConcept) {
            this.apiViewConcept = apiViewConcept;
        }

        public String getApiGetConcept() {
            return host + apiGetConcept;
        }

        public void setApiGetConcept(String apiGetConcept) {
            this.apiGetConcept = apiGetConcept;
        }
    }

    @Data
    public static class AbTestingService {
        private String host;
    }

    @Data
    public static class NotificationService {
        private String host;
    }

    @Data
    public static class QandaService {
        private String host;
    }

    @Data
    public static class BookService {
        private String host;
    }

    @Data
    public static class BrahmService {
        private String host;
    }
}
