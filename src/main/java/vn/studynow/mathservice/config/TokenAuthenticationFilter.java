package vn.studynow.mathservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.apache.commons.codec.binary.Base64;
import vn.studynow.mathservice.common.constant.Parameter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    @Autowired
    ObjectMapper objectMapper;

    private final String POST_METHOD = "POST";
    private final String GET_METHOD = "GET";
    private final String PUT_METHOD = "PUT";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        CustomRequestWrapper customRequestWrapper = new CustomRequestWrapper(request);
        try {
            String jwt = getJwtFromRequest(request);
            if (jwt != null && !"".equals(jwt)) {
                String[] split_string = jwt.split("\\.");
                if (split_string != null && split_string.length > 1) {
                    String base64EncodedBody = split_string[1];
                    Base64 base64Url = new Base64(true);
                    String body = new String(base64Url.decode(base64EncodedBody));
                    HashMap hmBody = objectMapper.readValue(body, HashMap.class);
                    String bodyRequest;

                    switch (request.getMethod()) {
                        case POST_METHOD:
                            bodyRequest = IOUtils.toString(customRequestWrapper.getReader());
                            changeUserIdBody(bodyRequest, hmBody, customRequestWrapper);
                            break;
                        case GET_METHOD:
                            customRequestWrapper.addParameter(Parameter.REQUEST_PARAM_USER_ID, String.valueOf(hmBody.get(Parameter.SUB)));
                            break;
                        case PUT_METHOD:
                            bodyRequest = IOUtils.toString(customRequestWrapper.getReader());
                            changeUserIdBody(bodyRequest, hmBody, customRequestWrapper);
                            break;
                        default:
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
        }
        filterChain.doFilter(customRequestWrapper, response);
    }

    private void changeUserIdBody(String body, HashMap hmJwt, CustomRequestWrapper customRequestWrapper) {
        try {
            if (!"".equals(body)) {
                HashMap hm = objectMapper.readValue(body, HashMap.class);
                hm.put(Parameter.REQUEST_PARAM_USER_ID, String.valueOf(hmJwt.get(Parameter.SUB)));
                body = objectMapper.writeValueAsString(hm);
                customRequestWrapper.resetInputStream(body.getBytes(StandardCharsets.UTF_8));
            }
        } catch (Exception e) {
            logger.error("Error at changeUserIdBody: " + e.getMessage());
        }
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
}

