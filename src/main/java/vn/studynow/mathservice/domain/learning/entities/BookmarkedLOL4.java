package vn.studynow.mathservice.domain.learning.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@Document(collection = "bookmarked_lol4")
@Data
public class BookmarkedLOL4 {

    @Id
    private String _id;

    @NotNull
    @Field(name = "user_id")
    @JsonProperty(value = "user_id")
    private String userId;

//    @NotNull
//    @Field(name = "icon_url")
//    @JsonProperty(value = "icon_url")
//    private String iconUrl;

    @NotNull
    @Field(name = "lol_name")
    @JsonProperty(value = "lol_name")
    private String lolName;


    @NotNull
    @Field(name = "bookmarked")
    @JsonProperty(value = "bookmarked")
    private boolean bookmarked = true;

    @JsonIgnore
    @Field(name = "created_at")
    private Date createdAt = new Date();


}
