package vn.studynow.mathservice.domain.tracking.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import vn.studynow.mathservice.payload.tracking.QuestionParams;
import vn.studynow.mathservice.payload.tracking.ViewAnswerParams;
import vn.studynow.mathservice.util.DateTimeUtils;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
public class TrackingResult {

    @NotNull(message = "event_name must be not null")
    private String eventName;

    @NotNull
    private Long code;
    private String userId;
    private String deviceId;
    private String deviceType;
    private Integer grade = 12;
    private Integer subjectId = 1;
    private String cycleId;

    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @JsonProperty("lol3_uid")
    private String lol3Uid;

    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @JsonProperty("lol6_1_uid")
    private String lol61Uid;

    @JsonProperty("lol6_2_uid")
    private String lol62Uid;

    @JsonProperty("lol6_3_uid")
    private String lol63Uid;

    private List<QuestionParams> questionParams;
    private Integer totalTimeAnswer;
    private Integer timeLimit;

    @JsonProperty("num_answer")
    private Integer numAnswer;

    @JsonProperty("num_question")
    private Integer numQuestion;

    @JsonProperty("num_correct_answer")
    private Integer numCorrectAnswer;

    @JsonProperty("point")
    private Float point;

    @JsonProperty("material_id")
    private String materialId;


    @JsonProperty("material_code")
    private String materialCode;

    @JsonProperty("view_answer_params")
    private List<ViewAnswerParams> viewAnswerParams;

    private Long eventTimestamp;

    @JsonProperty("version")
    private String version;

    @JsonProperty("created_at")
    private String createAt;

    @JsonProperty("event_date")
    private String eventDate;

    @JsonProperty("study_type")
    private String studyType;

    @JsonProperty("app_version")
    private String appVersion;
}