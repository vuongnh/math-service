package vn.studynow.mathservice.domain.tracking.services;

import org.springframework.http.HttpMethod;
import vn.studynow.mathservice.domain.tracking.entities.TrackingEvent;
import vn.studynow.mathservice.domain.tracking.entities.TrackingResult;

import javax.servlet.http.HttpServletRequest;

public interface TrackingService {

    String TOPIC_TRACKING_EVENT = "TRACKING_EVENT";
    String TOPIC_TRACKING_RESULT = "TRACKING_RESULT_EPQ";


    void sendEventMessage(TrackingEvent trackingEvent);
    void sendResultMessage(TrackingResult trackingResult);
    void sendMessage(String topic, String data);
    void sendMessage(String topic, Object  data);

    void sendMessageRequest(HttpServletRequest request, HttpMethod method, String body) throws Exception;
    void sendMessageRequestWithoutException(HttpServletRequest request, HttpMethod method, String body);

}
