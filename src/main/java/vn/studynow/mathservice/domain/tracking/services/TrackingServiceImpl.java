package vn.studynow.mathservice.domain.tracking.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.common.RequestInfoExtractor;
import vn.studynow.mathservice.domain.tracking.entities.TrackingEvent;
import vn.studynow.mathservice.domain.tracking.entities.TrackingResult;
import vn.studynow.mathservice.exception.BadRequestException;
import vn.studynow.mathservice.util.DateTimeUtils;
import vn.studynow.mathservice.util.kafka.Producer;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class TrackingServiceImpl implements TrackingService {

    @Autowired
    Producer producer;

    @Override
    public void sendEventMessage(TrackingEvent trackingEvent) {
        producer.sendMessage2(TOPIC_TRACKING_EVENT, trackingEvent);
    }

    @Override
    public void sendResultMessage(TrackingResult trackingResult) {
        producer.sendMessage2(TOPIC_TRACKING_RESULT, trackingResult);
    }

    @Override
    public void sendMessage(String topic, String data) {
        producer.sendMessage2(topic, data);
    }

    @Override
    public void sendMessage(String topic, Object data) {
        producer.sendMessage2(topic, data);
    }

    @SneakyThrows
    @Override
    public void sendMessageRequest(HttpServletRequest request, HttpMethod method, String body) throws RuntimeException {
        RequestInfoExtractor requestInfoExtractor = new RequestInfoExtractor();
        requestInfoExtractor.extract(request, method, body);
        Map requestData = requestInfoExtractor.getData();

        String eventName = (String) requestData.getOrDefault("event_name", null);
        if (eventName == null) throw new IllegalArgumentException("Missing event_name field");

        requestData.put("created_at", DateTimeUtils.formatDateTime());
        requestData.put("event_date", DateTimeUtils.formatDate());
        requestData.put("timestamp_app", System.currentTimeMillis() / 1000);

        sendMessage(eventName,   requestData );
    }

    @Override
    public void sendMessageRequestWithoutException(HttpServletRequest request, HttpMethod method, String body) {
        try {
            sendMessageRequest(request, method, body);
        } catch (Exception e) {
        }
    }

}
