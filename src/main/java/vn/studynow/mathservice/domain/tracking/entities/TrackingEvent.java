package vn.studynow.mathservice.domain.tracking.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import vn.studynow.mathservice.payload.tracking.VideoParams;
import vn.studynow.mathservice.payload.tracking.ViewAnswerParams;
import vn.studynow.mathservice.util.DateTimeUtils;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
public class TrackingEvent {
    @NotNull(message = "event_name must be not null")
    @JsonProperty("event_name")
    private String eventName;

    @NotNull
    private Long code;

    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @JsonProperty("lol3_uid")
    private String lol3Uid;

    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @JsonProperty("lol6_1_uid")
    private String lol61Uid;

    @JsonProperty("lol6_2_uid")
    private String lol62Uid;

    @JsonProperty("lol6_3_uid")
    private String lol63Uid;

    @JsonProperty("grade")
    private Integer grade = 12;

    @JsonProperty("subject_id")
    private Integer subjectId = 1;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("pre_code")
    private Long preCode;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @JsonProperty("video_params")
    private List<VideoParams> videoParams;

    @JsonProperty("view_answer_params")
    private List<ViewAnswerParams> viewAnswerParams;

    @JsonProperty("version")
    private String version;

    @JsonProperty("total_view_time")
    private Integer totalViewTime;

    @JsonProperty("created_at")
    private String createAt;

    @JsonProperty("event_date")
    private String eventDate;

    @JsonProperty("study_type")
    private String studyType;

    @JsonProperty("app_version")
    private String appVersion;
}

/**
 code
 event_date
 event_name
 cycle_id
 lol0_uid
 lol1_uid
 lol2_uid
 lol3_uid
 lol4_uid

 lol6_1_uid
 lol6_2_uid
 lol6_3_uid
 grade
 subject_id
 created_at
 updated_at

 version
 device_type
 event_timestamp
 pre_id
 video_params









 device_id
 user_id
 material_id
 view_answer_params


 total_view_time
 */