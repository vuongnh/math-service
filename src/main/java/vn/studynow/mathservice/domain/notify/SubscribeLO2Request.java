package vn.studynow.mathservice.domain.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class SubscribeLO2Request {


    @NotEmpty(message = "user_id is required")
    @JsonProperty("user_id")
    private String user_id;

    @NotEmpty(message = "topic_id is required")
    @JsonProperty("uid")
    private String uid;

    @JsonProperty("types_name")
    @NotEmpty(message = "types_name is required")
    private String types_name;


    @JsonProperty("subscribe")
    @NotNull(message = "subscribe is required")
    private Boolean subscribe;

}
