package vn.studynow.mathservice.domain.notify;

public interface NotificationService {

    void subscribeComingSoonObject(SubscribeNotificationRequest request);

}
