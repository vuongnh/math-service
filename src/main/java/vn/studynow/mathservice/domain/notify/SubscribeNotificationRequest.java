package vn.studynow.mathservice.domain.notify;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class SubscribeNotificationRequest {


    @NotEmpty(message = "user_id is required")
    @JsonProperty("user_id")
    private String user_id;

    @NotEmpty(message = "topic_id is required")
    @JsonProperty("topic_id")
    private String topic_id;

    @JsonProperty("topic_type")
    private String topic_type;

    @JsonProperty("subscribe")
    @NotEmpty(message = "subscribe is required")
    private Boolean subscribe;

}
