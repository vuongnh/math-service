package vn.studynow.mathservice.domain.notify;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.service.restrequest.RestRequest;

import java.util.Map;

@Service
public class NotificationServiceImpl implements NotificationService {

    public static final String NOTIFICATION_SUBSCRIBE_COMING_SOON = "/api/v1/coming-soon/subscribe";

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    RestRequest restRequest;

    @Autowired
    ObjectMapper mapper;

    @Override
    public void subscribeComingSoonObject(SubscribeNotificationRequest subscribeLO2Request) {
        String url = applicationProperties.getNotificationService().getHost() + NOTIFICATION_SUBSCRIBE_COMING_SOON;

        restRequest.post(url, mapper.convertValue(subscribeLO2Request, Map.class));
    }
}
