package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AnswerChoice {

    @JsonProperty("answer_of_user")
//    @NotNull(message = "answer_of_user must be not null")
    private String userAnswer;

    @JsonProperty("correct_answer")
    @NotNull(message = "correct_answer must be not null")
    private String correctAnswer;
}
