package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SubmitPractice6Request extends SubmitPracticeRequest{

    @NotNull(message = "lol0_uid must be not null")
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull(message = "lol1_uid must be not null")
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull(message = "lol2_uid must be not null")
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @JsonProperty("lol3_uid")
    @NotNull(message = "lol3_uid must be not null")
    private String lol3Uid;

    @JsonProperty("lol4_uid")
    @NotNull(message = "lol4_uid must be not null")
    private String lol4Uid;

    @JsonProperty("lol6_1_uid")
//    @NotNull(message = "lol6_1_uid must be not null")
    private String lol61Uid;

    @JsonProperty("lol6_2_uid")
//    @NotNull(message = "lol6_1_uid must be not null")
    private String lol62Uid;

    @JsonProperty("lol6_3_uid")
//    @NotNull(message = "lol6_1_uid must be not null")
    private String lol63Uid;


    @JsonProperty("cycle_node_level4_types_name")
    @NotNull(message = "cycle_node_level4_types_name must be not null")
    private String cycleNodeLevel4TypesName;

    @JsonProperty("cycle_node_level4_uid")
    @NotNull(message = "cycle_node_level4_uid must not be null")
    private String cycleNodeLevel4Uid;








}
