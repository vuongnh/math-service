package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.Answer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Quiz {

    @NotNull(message = "question_id must be not null")
    @JsonProperty("question_id")
    private String uid;

    @NotNull(message = "question_text must be not null")
    @JsonProperty("question_text")
    private String question;

    @JsonProperty("answer_text")
    @NotNull(message = "answer_text must be not null")
    private String answerDetail;

    @JsonProperty("answer_list")
    @NotNull(message = "answer_list must be not null")
    private List<@Valid Answer> listAnswerOfQuiz;

    @NotNull(message = "list_answer_choice must be not null")
    @JsonProperty("list_answer_choice")
    private List<@Valid AnswerChoice> answerChoices;

    @NotNull(message = "type_question must be not null")
    @JsonProperty("type_question")
    private String typeQuestion;

    @JsonProperty("lol_uid")
    @NotNull(message = "lolUid must be not null")
    private String lolUid;

//    @NotNull
    @JsonProperty("start_time")
    private Long startTime;

//    @NotNull
    @JsonProperty("end_time")
    private Long endTime;

    @JsonProperty("time_answer")
    private Integer timeAnswer;
}
