package vn.studynow.mathservice.payload.request.exam;

import lombok.Data;

@Data
public class Answer {
    private String uid;

    private String text;
}
