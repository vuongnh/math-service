package vn.studynow.mathservice.payload.request.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;
import vn.studynow.mathservice.payload.tracking.ViewAnswerParams;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SubmitLol2ViewAnswerContinue extends TrackingRequest {

    @NotNull
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @NotNull
    @JsonProperty("pre_code")
    private String preCode;

    @NotNull
    @JsonProperty("material_id")
    private String materialId;

    @NotNull
    @JsonProperty("view_answer_params")
    private List<ViewAnswerParams> viewAnswerParams;


    public String getLol1Uid() {
        return lol1Uid;
    }

    public void setLol1Uid(String lol1Uid) {
        this.lol1Uid = lol1Uid;
    }

    public String getLol2Uid() {
        return lol2Uid;
    }

    public void setLol2Uid(String lol2Uid) {
        this.lol2Uid = lol2Uid;
    }

    public List<ViewAnswerParams> getViewAnswerParams() {
        return viewAnswerParams;
    }

    public void setViewAnswerParams(List<ViewAnswerParams> viewAnswerParams) {
        this.viewAnswerParams = viewAnswerParams;
    }

    public String getPreCode() {
        return preCode;
    }

    public void setPreCode(String preCode) {
        this.preCode = preCode;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }
}
