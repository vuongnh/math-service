package vn.studynow.mathservice.payload.request.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;
import vn.studynow.mathservice.payload.tracking.VideoParams;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SubmitLol6EndVideo {
    @NotNull
    @JsonProperty("cycle_id")
    private String cycleId;

    @NotNull
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @NotNull
    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @NotNull
    @JsonProperty("lol6_uid")
    private String lol6Uid;

    @NotNull
    @JsonProperty("pre_code")
    private String preCode;

    private Integer grade = 12;

    @JsonProperty("subject_id")
    private Integer subjectId = 1;

    @JsonProperty("video_params")
    private List<VideoParams> videoParams;

    @NotNull
    @JsonProperty("material_id")
    private String materialId;

    @NotNull
    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @NotNull
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public Long getEventTimestamp() {
        return eventTimestamp;
    }

    public void setPreCode(String preCode) {
        this.preCode = preCode;
    }

    public String getPreCode() {
        return preCode;
    }

    public void setVideoParams(List<VideoParams> videoParams) {
        this.videoParams = videoParams;
    }

    public List<VideoParams> getVideoParams() {
        return videoParams;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public Integer getGrade() {
        return grade;
    }

    public String getLol4Uid() {
        return lol4Uid;
    }

    public void setLol4Uid(String lol4Uid) {
        this.lol4Uid = lol4Uid;
    }

    public String getLol1Uid() {
        return lol1Uid;
    }

    public void setLol1Uid(String lol1Uid) {
        this.lol1Uid = lol1Uid;
    }

    public String getLol2Uid() {
        return lol2Uid;
    }

    public void setLol2Uid(String lol2Uid) {
        this.lol2Uid = lol2Uid;
    }

    public String getLol0Uid() {
        return lol0Uid;
    }

    public void setLol0Uid(String lol0Uid) {
        this.lol0Uid = lol0Uid;
    }

    public String getLol6Uid() {
        return lol6Uid;
    }

    public void setLol6Uid(String lol6Uid) {
        this.lol6Uid = lol6Uid;
    }

    public String getCycleId() {
        return cycleId;
    }

    public void setCycleId(String cycleId) {
        this.cycleId = cycleId;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public void setEventTimestamp(Long eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }
}
