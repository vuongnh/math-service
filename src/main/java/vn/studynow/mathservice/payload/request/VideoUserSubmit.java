package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class VideoUserSubmit {

    @JsonProperty("user_id")
    @NotNull(message = "user_id is not null")
    private String userId;

    @NotNull(message = "uid is not null")
    private String uid;

    @NotNull(message = "types_name is not null")
    @JsonProperty("types_name")
    private String typesName;

    private Long time;

}
