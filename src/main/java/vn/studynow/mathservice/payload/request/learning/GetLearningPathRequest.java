package vn.studynow.mathservice.payload.request.learning;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import lombok.experimental.SuperBuilder;
import vn.studynow.mathservice.payload.BaseRequest;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Getter
@SuperBuilder
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetLearningPathRequest extends BaseRequest {
    protected String typesName;
    protected String uid;
    protected int isRelearn;

    @JsonProperty("new_lol4_count")
    protected int numberLol4;   // so lol4 trong ngay

}
