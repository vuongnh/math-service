package vn.studynow.mathservice.payload.request.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class SubmitLol4Done {
    @NotNull
    @JsonProperty("cycle_id")
    private String cycleId;

    @NotNull
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @NotNull
    @JsonProperty("lol4_uid")
    private String lol4Uid;

    private Integer grade = 12;

    @JsonProperty("subject_id")
    private Integer subjectId = 1;

    @NotNull
    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @NotNull
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setEventTimestamp(Long eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public Long getEventTimestamp() {
        return eventTimestamp;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public void setLol0Uid(String lol0Uid) {
        this.lol0Uid = lol0Uid;
    }

    public String getLol0Uid() {
        return lol0Uid;
    }

    public void setLol2Uid(String lol2Uid) {
        this.lol2Uid = lol2Uid;
    }

    public String getLol2Uid() {
        return lol2Uid;
    }

    public void setLol1Uid(String lol1Uid) {
        this.lol1Uid = lol1Uid;
    }

    public String getLol1Uid() {
        return lol1Uid;
    }

    public void setLol4Uid(String lol4Uid) {
        this.lol4Uid = lol4Uid;
    }

    public String getLol4Uid() {
        return lol4Uid;
    }

    public void setCycleId(String cycleId) {
        this.cycleId = cycleId;
    }

    public String getCycleId() {
        return cycleId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }
}
