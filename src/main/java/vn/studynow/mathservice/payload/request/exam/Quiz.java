package vn.studynow.mathservice.payload.request.exam;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quiz {

    @JsonProperty("question_image_url")
    private String questionImageUrl;

    @JsonProperty("answer_image_url")
    private String answerImageUrl;

    private Integer status;

    @JsonProperty("question_id")
    private String uid;

    @JsonProperty("question_text")
    private String question;

    @JsonProperty("answer_text")
    private String answerDetail;

    @JsonProperty("answer_list")
    private List<@Valid Answer> listAnswerOfQuiz;

    @JsonProperty("list_answer_choice")
    private List<AnswerChoice> answerChoices;

    @NotNull(message = "type_question must not be null")
    @JsonProperty("type_question")
    private String typeQuestion;

    @JsonProperty("lol_uid")
    private String lolUid;

    @JsonProperty("lol_type")
    @NotNull(message = "lol_type must not be null")
    private String lolType;

    @JsonProperty("start_time")
    private Long startTime;

    @JsonProperty("end_time")
    private Long endTime;

    @JsonProperty("time_answer")
    private Integer timeAnswer;
}
