package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.Answer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public abstract class SubmitQuizzesRequest {

    @NotNull(message = "event_name must be not null")
    @JsonProperty("event_name")
    private String eventName;

    @NotNull(message = "code must be not null")
    private Long code;

    @NotNull(message = "pre_code must be not null")
    @JsonProperty("pre_code")
    private Long preCode;

    @NotNull(message = "types_nmame must be not null")
    @JsonProperty("types_name")
    private String typesName;

    @NotNull(message = "material_code must be not null")
    @JsonProperty("material_code")
    private String materialCode;

    @NotNull(message = "cycle_id must be not null")
    @JsonProperty("cycle_id")
    private String cycleId;

    @NotNull(message = "ui  must be not null")
    private String uid;

    @NotNull(message = "question_params must be not null")
    @JsonProperty("question_params")
    private List<@Valid Quiz> questionParams;

    @NotNull(message = "total_time_answer must be not null")
    @JsonProperty("total_time_answer")
    private Integer time;

    @NotNull(message = "time_limit must be not null")
    @JsonProperty("time_limit")
    private Integer timeLimit;

    @NotNull(message = "cycle_node_types_name must be not null")
    @JsonProperty("cycle_node_types_name")
    private String cycleNodeTypesName;

    @NotNull(message = "cycle_node_uid must be not null")
    @JsonProperty("cycle_node_uid")
    private String cycleNodeUid;

    @NotNull(message = "grade must be not null")
    private Integer grade;

    @NotNull(message = "subject_id must be not null")
    @JsonProperty("subject_id")
    private Integer subjectId;

    @NotNull(message = "event_timestamp must be not null")
    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @JsonProperty("study_type")
    private String studyType;

    @JsonProperty("is_relearn")
    private Boolean isRelearn;

    @JsonProperty("app_version")
    private String appVersion;
}
