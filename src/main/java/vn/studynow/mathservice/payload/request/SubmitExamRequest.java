package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SubmitExamRequest extends SubmitQuizzesRequest{

    @NotNull(message = "cycle_node_level2_types_name must be not null")
    @JsonProperty("cycle_node_level2_types_name")
    private String cycleNodeLevel2TypesName;

    @NotNull(message = "cycle_node_level2_uid must be not null")
    @JsonProperty("cycle_node_level2_uid")
    private String cycleNodeLevel2Uid;

    @NotNull(message = "lol0_uid must be not null")
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull(message = "lol1_uid must be not null")
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @JsonProperty("lol2_uid")
    private String lol2Uid;

}
