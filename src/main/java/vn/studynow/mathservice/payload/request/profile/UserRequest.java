package vn.studynow.mathservice.payload.request.profile;

import lombok.Data;
import vn.studynow.mathservice.model.profile.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
public class UserRequest {
    private String avatar;
    private String name;

    @Pattern(regexp = "^0\\d{9}$", message = "Phone must be start with letter 0 and contains only 10 number letters, no special characters")
    private String phone;

    @Email(message = "Email should be valid")
    private String email;
    private String gender;

    private Date birthday;

    public void mappingData(User user) {
        if(name != null) user.setName(name);
        if(avatar != null) user.setAvatar(avatar);
        if(birthday != null) user.setBirthday(birthday);
        if(email != null) user.setEmail(email);
        if(gender != null) user.setGender(gender);
        if(phone != null) user.setPhone(phone);
    }
}
