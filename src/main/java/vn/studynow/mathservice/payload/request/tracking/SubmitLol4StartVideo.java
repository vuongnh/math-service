package vn.studynow.mathservice.payload.request.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class SubmitLol4StartVideo extends TrackingRequest {

    @NotNull
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @NotNull
    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @NotNull
    @JsonProperty("material_id")
    private String materialId;

    public String getLol1Uid() {
        return lol1Uid;
    }

    public void setLol1Uid(String lol1Uid) {
        this.lol1Uid = lol1Uid;
    }

    public String getLol2Uid() {
        return lol2Uid;
    }

    public void setLol2Uid(String lol2Uid) {
        this.lol2Uid = lol2Uid;
    }

    public String getLol4Uid() {
        return lol4Uid;
    }

    public void setLol4Uid(String lol4Uid) {
        this.lol4Uid = lol4Uid;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }
}
