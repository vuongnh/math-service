package vn.studynow.mathservice.payload.request.exam;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AnswerChoice {

    @JsonProperty("answer_of_user")
    private String userAnswer;

    @JsonProperty("correct_answer")
    private String correctAnswer;
}
