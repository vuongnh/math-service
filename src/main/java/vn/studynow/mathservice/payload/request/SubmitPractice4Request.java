package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SubmitPractice4Request extends SubmitPracticeRequest {

    @NotNull(message = "lol0_uid must be not null")
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull(message = "lol1_uid must be not null")
    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @NotNull(message = "lol2_uid must not be null")
    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @NotNull(message = "lol3_uid must not be null")
    @JsonProperty("lol3_uid")
    private String lol3Uid;

    @NotNull(message = "lol4_uid must be not null")
    @JsonProperty("lol4_uid")
    private String lol4Uid;

}
