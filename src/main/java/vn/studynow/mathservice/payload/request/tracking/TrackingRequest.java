package vn.studynow.mathservice.payload.request.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class TrackingRequest {

    @NotNull
    private String code;

    @NotNull
    @JsonProperty("cycle_id")
    private String cycleId;

    @NotNull
    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @NotNull
    private Integer grade = 12;

    @NotNull
    @JsonProperty("subject_id")
    private Integer subjectId = 1;

    @NotNull
    @JsonProperty("event_timestamp")
    private Long eventTimeStamp;


    public String getCycleId() {
        return cycleId;
    }

    public void setCycleId(String cycleId) {
        this.cycleId = cycleId;
    }

    public String getLol0Uid() {
        return lol0Uid;
    }

    public void setLol0Uid(String lol0Uid) {
        this.lol0Uid = lol0Uid;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Long getEventTimeStamp() {
        return eventTimeStamp;
    }

    public void setEventTimeStamp(Long eventTimeStamp) {
        this.eventTimeStamp = eventTimeStamp;
    }

    public String getCode() {
        return code;
    }
}
