package vn.studynow.mathservice.payload.request.exam;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Data
public class SubmitExam {

    @JsonProperty("_id")
    private String id;

    private Long code;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("material_code")
    private String materialCode;

    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("lol0_uid")
    private String lol0UId;

    @JsonProperty("lol1_uid")
    private String lol1UId;

    @JsonProperty("lol2_uid")
    private String lol2UId;

    @JsonProperty("lol3_uid")
    private String lol3UId;

    @JsonProperty("lol4_uid")
    private String lol4UId;

    @JsonProperty("lol6_1_uid")
    private String lol61UId;

    @JsonProperty("lol6_2_uid")
    private String lol62UId;

    @JsonProperty("lol6_3_uid")
    private String lol63UId;

    @JsonProperty("question_params")
    private List<@Valid Quiz> questionParams;

    @JsonProperty("total_time_answer")
    private Integer time;

    @JsonProperty("time_limit")
    private Integer timeLimit;

    @JsonProperty("num_answer")
    private Integer numAnswer;

    @JsonProperty("num_question")
    private Integer numQuestion;

    @JsonProperty("num_correct_answer")
    private Integer numCorrectAnswer;

    private Integer point;

    @JsonProperty("created_at")
    private Date createdAt;

    private String version;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("study_type")
    private String studyType;

    private Integer grade;

    @JsonProperty("subject_id")
    private String subjectId;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

}
