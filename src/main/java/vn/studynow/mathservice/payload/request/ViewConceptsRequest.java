package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ViewConceptsRequest {
    private String code;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    private Integer grade;

    @JsonProperty("subject_id")
    private String subjectId;

    @JsonProperty("search_name")
    private String searchName;

    @JsonProperty("pre_code")
    private String preCode;

    @JsonProperty("time_view")
    private Integer timeView;

    @NotNull(message = "event_name must not be null")
    @JsonProperty("event_name")
    private String eventName;
}
