package vn.studynow.mathservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SubmitJoinLearningNodeLv2 {
    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("types_name")
    private String typesName;

    private String uid;

}
