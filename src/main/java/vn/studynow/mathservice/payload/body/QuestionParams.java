package vn.studynow.mathservice.payload.body;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.Answer;

import java.util.List;

@Data
public class QuestionParams {
    @JsonProperty("question_id")
    private String questionId;

    @JsonProperty("question_image_url")
    private String questionImageUrl;

    @JsonProperty("answer_image_url")
    private String answerImageUrl;

    @JsonProperty("question_text")
    private String questionText;

    @JsonProperty("answer_text")
    private String answerText;

    @JsonProperty("answer_list")
    private List<Answer> answerList;

    @JsonProperty("correct_answer")
    private String correctAnswer;

    @JsonProperty("answer_of_user")
    private String answerOfUser;

    private int status;

    @JsonProperty("time_answer")
    private long timeAnswer;

    @JsonProperty("start_time")
    private long startTime;

    @JsonProperty("end_time")
    private long endTime;
}

