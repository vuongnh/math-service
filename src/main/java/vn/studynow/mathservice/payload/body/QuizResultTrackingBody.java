package vn.studynow.mathservice.payload.body;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.payload.tracking.QuestionParams;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class QuizResultTrackingBody {

    @JsonProperty("question_params")
    private List<QuestionParams> questionParams;
}