package vn.studynow.mathservice.payload.tracking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class VideoParams {
    private Integer start;
    private Integer pause;
    private Integer resume;

    @JsonProperty("rewind_from")
    private Integer rewindFrom;

    @JsonProperty("rewind_to")
    private Integer rewindTo;

    @JsonProperty("forward_from")
    private Integer forwardFrom;

    @JsonProperty("forward_to")
    private Integer forwardTo;

    private Integer end;

}
