package vn.studynow.mathservice.payload.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ViewAnswerParams {

    @JsonProperty("question_id")
    private String questionId;

    @JsonProperty("view_time")
    private Integer viewTime;

}
