package vn.studynow.mathservice.payload.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.Answer;
import vn.studynow.mathservice.payload.request.AnswerChoice;

import java.util.List;

@Data
public class QuestionParams {
    @JsonProperty("question_id")
    private String questionId;

    @JsonProperty("question_image_url")
    private String questionImageUrl;

    @JsonProperty("answer_image_url")
    private String answerImageUrl;

    @JsonProperty("question_text")
    private String questionText;

    @JsonProperty("answer_text")
    private String answerText;

    @JsonProperty("answer_list")
    private List<Answer> answerList;

    @JsonProperty("list_answer_choice")
    List<AnswerChoice> answerChoices;

    private Integer status;

    @JsonProperty("time_answer")
    private Integer timeAnswer;

    @JsonProperty("start_time")
    private Long startTime;

    @JsonProperty("end_time")
    private Long endTime;

    @JsonProperty("type_question")
    private String typeQuestion;

    @JsonProperty("lol_uid")
    private String lolUid;

}

