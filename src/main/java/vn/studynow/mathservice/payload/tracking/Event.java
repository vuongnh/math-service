package vn.studynow.mathservice.payload.tracking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Event {

    @NotNull(message = "event_name must be not null")
    @JsonProperty("event_name")
    private String eventName;

    @NotNull
    private Long code;

    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @JsonProperty("lol3_uid")
    private String lol3Uid;

    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @JsonProperty("lol6_1_uid")
    private String lol61Uid;

    @JsonProperty("lol6_2_uid")
    private String lol62Uid;

    @JsonProperty("lol6_3_uid")
    private String lol63Uid;

    @JsonProperty("grade")
    private Integer grade = 12;

    @JsonProperty("subject_id")
    private Integer subjectId = 1;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("pre_code")
    private Long preCode;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @JsonProperty("video_params")
    private List<VideoParams> videoParams;

    @JsonProperty("question_params")
    private List<QuestionParams> questionParams;

    @JsonProperty("material_code")
    private String materialCode;

    @JsonProperty("total_time_answer")
    private Integer totalTimeAnswer;

    @JsonProperty("num_answer")
    private Integer numAnswer;

    @JsonProperty("num_question")
    private Integer numQuestion;

    @JsonProperty("num_correct_answer")
    private Integer numCorrectAnswer;

    @JsonProperty("point")
    private Float point;

    @JsonProperty("time_limit")
    private Integer timeLimit;

    @JsonProperty("view_answer_params")
    private List<ViewAnswerParams> viewAnswerParams;

    @JsonProperty("study_type")
    private String studyType;

    @JsonProperty("app_version")
    private String appVersion;

}