package vn.studynow.mathservice.payload;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import vn.studynow.mathservice.common.constant.Parameter;

@Getter
@SuperBuilder
public class BaseRequest {
    protected String userId;
    protected String deviceId;
    protected String deviceType;


}
