package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.MaterialFinalExamMotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SubmitFinalExamResponse extends SubmitQuizzesResponse<MaterialFinalExamMotivationNode>{

    @JsonProperty("entrance_correct_answer")
    private Integer entranceCorrectAnswer;

    @JsonProperty("entrance_wrong_answer")
    private Integer entranceWrongAnswer;

}
