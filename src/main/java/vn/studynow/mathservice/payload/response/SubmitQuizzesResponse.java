package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.MaterialMotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SubmitQuizzesResponse<T extends MaterialMotivationNode> {
    private T motivation;

    @JsonProperty("correct_answer")
    private Integer correctAnswer;

    @JsonProperty("wrong_answer")
    private Integer wrongAnswer;

    private Float point;
}
