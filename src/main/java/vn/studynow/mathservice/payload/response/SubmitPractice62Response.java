package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vn.studynow.mathservice.model.MaterialPractice62MotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitPractice62Response extends SubmitQuizzesResponse<MaterialPractice62MotivationNode>{
}
