package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vn.studynow.mathservice.model.MaterialPractice63MotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitPractice63Response extends SubmitQuizzesResponse<MaterialPractice63MotivationNode>{
}
