package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vn.studynow.mathservice.model.MaterialEntranceExamMotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitEntranceExamResponse extends SubmitQuizzesResponse<MaterialEntranceExamMotivationNode>{
}
