package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.model.KnowledgeLevel4Node;
import vn.studynow.mathservice.model.MaterialEntranceExamMotivationNode;

import java.util.List;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LearningPathResponse {
    @JsonProperty("lol4_nodes")
    private List<KnowledgeLevel4Node> learningPath;

    @JsonProperty("has_final_node")
    private boolean hasLastNode = false;
}
