package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vn.studynow.mathservice.model.MaterialPractice4MotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitPractice4Response extends SubmitQuizzesResponse<MaterialPractice4MotivationNode>{
}
