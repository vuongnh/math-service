package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubmitExamResponse {
    @JsonProperty("correct_answer")
    private Integer correctAnswer;

    @JsonProperty("wrong_answer")
    private Integer wrongAnswer;

    private Float point;
}
