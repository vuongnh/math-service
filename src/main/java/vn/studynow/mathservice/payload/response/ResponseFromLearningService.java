package vn.studynow.mathservice.payload.response;

import lombok.Data;

@Data
public class ResponseFromLearningService<T> {
    Object message;
    T data;
}
