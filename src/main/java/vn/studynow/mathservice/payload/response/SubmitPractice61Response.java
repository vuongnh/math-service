package vn.studynow.mathservice.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vn.studynow.mathservice.model.MaterialPractice62MotivationNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitPractice61Response extends SubmitQuizzesResponse<MaterialPractice62MotivationNode>{
}
