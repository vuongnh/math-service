package vn.studynow.mathservice.payload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;
import vn.studynow.mathservice.exception.BadRequestException;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlers {

    protected Logger log = LoggerFactory.getLogger(ExceptionHandlers.class);

    @Value("${PROD_ENV:true}")
    private boolean prodEnv;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity handleUserNotFoundException(final Exception ex) {
        String errorMessage = "Error " + ex.toString() + " at " +  stackTraceToString(ex.getStackTrace()) ;
        log.error(errorMessage);
        if (prodEnv)
            return new ResponseEntity<>(new ApiDefaultResponse(false, "Internal Error", null), HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(new ApiDefaultResponse(false, errorMessage, null), HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ExceptionHandler(ResponseStatusException.class)
//    @ResponseBody
//    public ResponseEntity<Response> handleUserNotFoundException(final ResponseStatusException ex) {
//        String errorMessage =stackTraceToString(ex.getStackTrace()[0]) + ". Error " + ex.toString();
//        log.error(errorMessage);
//        return  ResponseEntity.status(ex.getStatus()).body (new UnknowExceptionResponse(errorMessage));
//    }
//
//    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
//    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
//    @ResponseBody
//    public Response handleMissingParamsException(final HttpRequestMethodNotSupportedException ex) {
//        return new Response().setMessage(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase()).setDescription(ex.getMessage());
//    }
//
//
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiDefaultResponse handleMissingParamsException(final MethodArgumentNotValidException ex) {
        //Get all errors
        List<String> errors2 = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> {
                    return fieldError.getField()  + " - " + fieldError.getDefaultMessage();
                })
                .collect(Collectors.toList());

        return new ApiDefaultResponse(false, errors2.toString(),  "400",null);
    }

    // Path variable missing
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiDefaultResponse handleMissingParamsException(final ConstraintViolationException ex) {
        log.error(ex.getMessage());
        return new ApiDefaultResponse(false, ex.getMessage(),  "400",null);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiDefaultResponse handleMissingParamsException(String message) {
        return new ApiDefaultResponse(false, message,  "400",null);
    }
//
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    @ResponseBody
//    public Response handleResultNotFoundException(String description) {
//        return new Response().setErrorCode(HttpStatus.NOT_FOUND.value())
//                .setMessage(HttpStatus.NOT_FOUND.getReasonPhrase()).setDescription(description);
//    }
//
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    @ResponseBody
//    public Response handleInternalErrorException(String description) {
//        return new Response().setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
//                .setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()).setDescription(description);
//    }
//
//
//    @ExceptionHandler(NotFoundException.class)
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Response handleNotFoundException(final NotFoundException ex) {
//        return new Response().setErrorCode(404).setMessage("Not Found").setDescription(ex.getMessage());
//    }
//
//    @ExceptionHandler(UnauthorizedException.class)
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Response handleUnauthorizedException(final UnauthorizedException ex) {
//        return new Response().setErrorCode(HttpStatusCommon.UNAUTHORIZED_CODE).setMessage(HttpStatusCommon.UNAUTHORIZED_MESSAGE).setDescription(ex.getMessage());
//    }
//
//    @ExceptionHandler(BadRequestException.class)
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Response handleUnauthorizedException(final BadRequestException ex) {
//        return new Response().setErrorCode(HttpStatusCommon.BAD_REQUEST_CODE).setMessage(HttpStatusCommon.BAD_REQUEST_MESSAGE).setDescription(ex.getMessage());
//    }
//
//    @ExceptionHandler(NotExecuteException.class)
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Response handleNotExecuteException(final NotExecuteException ex) {
//        return new Response().setErrorCode(HttpStatusCommon.NOT_EXECUTE_CODE).setMessage(HttpStatusCommon.NOT_EXECUTE_MESSAGE).setDescription(ex.getMessage());
//    }




    private String stackTraceToString(StackTraceElement[] elements) {
        int depth = 5;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < elements.length && i < depth ; i++) {
            StackTraceElement element = elements[i];
            String message = element == null ? "" :   element.getClassName() + "." + element.getMethodName() + "(" + element.getLineNumber() + ")";
            builder.append(message).append("\t");
        }
        return builder.toString();
    }
}

