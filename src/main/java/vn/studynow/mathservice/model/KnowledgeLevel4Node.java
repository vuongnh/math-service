package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.studynow.mathservice.common.constant.LearningNodeConstant;

import java.util.List;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KnowledgeLevel4Node extends KnowledgeTreeNode {
    @JsonProperty("child_nodes")
    private List<KnowledgeLevel6Node> childNodes;

    @JsonProperty("is_available")
    private Integer isAvailable;



    @JsonProperty("tracking_data")
    private TrackingData trackingData;


}
