package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

//@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = JsonDeserializer.None.class)
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MaterialFinalExamMotivationNode extends MaterialMotivationNode {
    private String content;
    private String title;
    private Button buttonNextLol2;
    private Button buttonStartLol2;
    private Button buttonShowDetailAnswer;
    private String correctAnswerColor;
    private String wrongAnswerColor;
    private String finalExamScore;
    private int entranceExamCorrectAnswer;
    private float entranceExamScore;
    private int entranceExamWrongAnswer;
    private String firstExamText;
    private String secondExamText;
}
