package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MathConcept {

    private String id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String thematic;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String chapter;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String lesson;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String term;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String content;

    @JsonProperty("knowledge_type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String knowledgeType;

    @JsonProperty("knowledge_name")
    private String knowledgeName;
}
