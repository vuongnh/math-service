package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import vn.studynow.mathservice.model.util.KnowledgeLevel6JsonDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = KnowledgeLevel6JsonDeserializer.class)
public class KnowledgeLevel6Node extends KnowledgeTreeNode {

    @JsonProperty("tracking_data")
    private TrackingData trackingData;
}
