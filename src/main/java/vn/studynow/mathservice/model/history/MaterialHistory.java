package vn.studynow.mathservice.model.history;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import vn.studynow.mathservice.model.Quiz;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MaterialHistory {
    @Id
    private String id;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("material_code")
    private String materialCode;

    @JsonProperty("time_limit")
    private int timeLimit;

    private List<Quiz> materials;



}
