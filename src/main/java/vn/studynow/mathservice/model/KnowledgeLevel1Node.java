package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KnowledgeLevel1Node extends KnowledgeTreeNode {
    @JsonProperty("child_nodes")
    private List<KnowledgeLevel2Node> childNodes;
}
