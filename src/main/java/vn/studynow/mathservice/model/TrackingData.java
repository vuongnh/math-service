package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingData {

    @JsonProperty("lol0_uid")
    private String lol0Uid;

    @JsonProperty("lol1_uid")
    private String lol1Uid;

    @JsonProperty("lol2_uid")
    private String lol2Uid;

    @JsonProperty("lol3_uid")
    private String lol3Uid;

    @JsonProperty("lol4_uid")
    private String lol4Uid;

    @JsonProperty("lol6_1_uid")
    private String lol61Uid;

}
