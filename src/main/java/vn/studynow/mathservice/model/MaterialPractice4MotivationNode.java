package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = JsonDeserializer.None.class)
@Data
public class MaterialPractice4MotivationNode extends MaterialPracticeMotivationNode {
    @JsonProperty("button_next_lol4")
    private Button buttonNextLol4;

    @JsonProperty("button_show_detail_answer")
    private Button buttonShowDetailAnswer;

    @JsonProperty("correct_answer_color")
    private String correctAnswerColor;

    @JsonProperty("wrong_answer_color")
    private String wrongAnswerColor;
}
