package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = JsonDeserializer.None.class)
@Data
public class MaterialQuizNode extends MaterialNode {
    private List<Quiz> materials;

    @JsonProperty("material_code")
    private String materialCode;

    @JsonProperty("cycle_node_types_name")
    private String cycleNodeTypesName;

    @JsonProperty("cycle_node_uid")
    private String cycleNodeUid;

    @JsonProperty("time_limit")
    private int timeLimit;
}
