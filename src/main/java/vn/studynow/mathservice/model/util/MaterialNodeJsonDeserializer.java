package vn.studynow.mathservice.model.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vn.studynow.mathservice.common.constant.MaterialConstant;
import vn.studynow.mathservice.model.*;

import java.io.IOException;
import java.util.Map;

@SuppressWarnings("unchecked")
public class MaterialNodeJsonDeserializer extends JsonDeserializer<MaterialNode> {
    private final Logger log = LoggerFactory.getLogger(MaterialNodeJsonDeserializer.class);

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public MaterialNode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Map<String, Object> map = p.readValueAs(Map.class);

        String typesName = map.get(MaterialConstant.Properties.TYPES_NAME).toString();

        switch (typesName) {
            case MaterialConstant.TYPES_NAME_LECTURE_GROUP:
                return objectMapper.convertValue(map, MaterialVideoNode.class);
            case MaterialConstant.TYPES_NAME_PRACTICE_GROUP:
            case MaterialConstant.TYPES_NAME_EXAM_DRAG_DROP:
                return objectMapper.convertValue(map, MaterialPracticeDragAndDropNode.class);
            case MaterialConstant.TYPES_NAME_EXAM:
                return objectMapper.convertValue(map, MaterialExamNode.class);
            default:
                return null;
        }
    }
}
