package vn.studynow.mathservice.model.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vn.studynow.mathservice.common.constant.LearningNodeConstant;
import vn.studynow.mathservice.model.*;

import java.io.IOException;
import java.util.Map;

@SuppressWarnings("unchecked")
public class KnowledgeLevel6JsonDeserializer extends JsonDeserializer<KnowledgeLevel6Node> {
    private final Logger log = LoggerFactory.getLogger(KnowledgeLevel6JsonDeserializer.class);

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public KnowledgeLevel6Node deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Map<String, Object> map = p.readValueAs(Map.class);

        Object knowledge = map.get(LearningNodeConstant.Properties.KNOWLEDGE);
        String typesName = ((Map<String, Object>) knowledge).get(LearningNodeConstant.Properties.TYPES_NAME).toString();

        switch (typesName) {
            case LearningNodeConstant.TYPES_NAME_KNOWLEDGE61:
                return objectMapper.convertValue(map, KnowledgeLevel61Node.class);
            case LearningNodeConstant.TYPES_NAME_KNOWLEDGE62:
                return objectMapper.convertValue(map, KnowledgeLevel62Node.class);
            case LearningNodeConstant.TYPES_NAME_KNOWLEDGE63:
                return objectMapper.convertValue(map, KnowledgeLevel63Node.class);
            default:
                return null;
        }
    }
}
