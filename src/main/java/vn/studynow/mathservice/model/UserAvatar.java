package vn.studynow.mathservice.model;

import lombok.Data;

@Data
public class UserAvatar extends LearningNode {

    private String url;
}
