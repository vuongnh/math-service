package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KnowledgeTreeNode extends LearningNode {

    private Knowledge knowledge;

    @JsonProperty("pass_status")
    private int passStatus;
}
