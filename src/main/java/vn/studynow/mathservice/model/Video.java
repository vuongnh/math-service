package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Video extends LearningNode{

    private String url;

    private String description;

    private String thumbnail;
}
