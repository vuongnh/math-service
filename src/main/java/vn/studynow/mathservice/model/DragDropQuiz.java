package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DragDropQuiz extends LearningNode  {

    private String question;

    @JsonProperty("answer_detail")
    private String answerDetail;

    private List<Answer> answers;

    @JsonProperty("correct_answers")
    private List<String> correctAnswerUIds = new ArrayList<>();

    @JsonProperty("answers_count")
    private int answers_count;

    public void shuffleAnswer() {
        List<Answer> correctAnswers = getAnswers().subList(0, answers_count);
        correctAnswers.forEach(answer -> {
            correctAnswerUIds.add(answer.getUid());
        });
    }
}
