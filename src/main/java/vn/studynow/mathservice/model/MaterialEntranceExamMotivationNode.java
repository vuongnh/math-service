package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = JsonDeserializer.None.class)
@Data
public class MaterialEntranceExamMotivationNode extends MaterialMotivationNode {
    private String content;
    private String title;

    @JsonProperty("button_next_lol2")
    private Button buttonNextLol2;

    @JsonProperty("button_start_lol2")
    private Button buttonStartLol2;

    @JsonProperty("button_show_detail_answer")
    private Button buttonShowDetailAnswer;

    @JsonProperty("correct_answer_color")
    private String correctAnswerColor;

    @JsonProperty("wrong_answer_color")
    private String wrongAnswerColor;

    @JsonProperty("entrance_exam_score")
    private String entranceExamScore;

    @JsonProperty("is_pass")
    private Integer isPass;

}
