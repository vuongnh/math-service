package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MaterialEntranceExamMotivationPopupNode extends MaterialMotivationPopupNode {
    @JsonProperty("button_start")
    private Button buttonStart;

    private String title;

    private String content;

    @JsonProperty("icon_url")
    private String iconUrl;
}
