package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class LearningNode {

    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("types_name")
    private String typesName;

    @JsonProperty("node_types")
    private String nodeType;

    private String uid;

    private String name;
}
