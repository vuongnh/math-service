package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Quiz extends LearningNode {
    private String question;

    @JsonProperty("answer_detail")
    private String answerDetail;

    private List<Answer> answers;

    @JsonProperty("correct_answer")
    private String correctAnswer;

    @JsonProperty("lol_uid")
    private String lolUid;


}
