package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import vn.studynow.mathservice.model.util.MaterialNodeJsonDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = MaterialNodeJsonDeserializer.class)
public class MaterialNode extends LearningNode {
}
