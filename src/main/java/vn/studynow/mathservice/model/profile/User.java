package vn.studynow.mathservice.model.profile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class User {
    @Id
    private String id;

    private String avatar;
    private String name;
    private String phone;
    private String email;
    private String gender;
    private Date birthday;
}
