package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KnowledgeLevel2Node extends KnowledgeTreeNode {
    @JsonProperty("child_nodes")
    private List<KnowledgeLevel4Node> childNodes;

    @JsonProperty("tracking_data")
    private TrackingData trackingData;
}
