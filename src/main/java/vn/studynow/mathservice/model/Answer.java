package vn.studynow.mathservice.model;

import lombok.Data;

@Data
public class Answer {
    private String uid;

    private String text;
}
