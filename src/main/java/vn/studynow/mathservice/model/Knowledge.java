package vn.studynow.mathservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Knowledge {

    private String description;

    private String name;

    @JsonProperty("types_name")
    private String typesName;

    private String uid;

    @JsonProperty("icon_url")
    private String iconUrl;


}
