package vn.studynow.mathservice.model.abtesting;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.sql.Timestamp;
import java.util.Map;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Bucket {

    private String id;
    private String bucketKey;
    private String testingObjectKey;
    private boolean inCollection = false;
    private boolean conditionExpression;
    private int ratioBucket = 0;
    private int ratioValue = 0;
    private Map<String, Object> attributes;
    private Timestamp startTime;
    private Timestamp endTime;
    private Long userSize;
    private boolean active = true;
}
