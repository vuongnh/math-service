package vn.studynow.mathservice.service;

import vn.studynow.mathservice.payload.request.SubmitPractice4Request;
import vn.studynow.mathservice.payload.request.SubmitPractice6Request;
import vn.studynow.mathservice.payload.response.*;

public interface MaterialPracticeNodeService {

    SubmitPractice4Response submitPractice4(String userId, String deviceId, String deviceType, SubmitPractice4Request submitPractice4Request) throws Exception;
    SubmitPractice61Response submitLearningNodeLevel61Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception;
    SubmitPractice62Response submitLearningNodeLevel62Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception;
    SubmitPractice63Response submitLearningNodeLevel63Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception;
}
