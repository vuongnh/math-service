package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.KnowledgeLevel2Node;

public interface KnowledgeLv2NodeService {
    KnowledgeLevel2Node getRecommendLearningNodeLevel2(String userId, String deviceId, String deviceType)  throws Exception;

}
