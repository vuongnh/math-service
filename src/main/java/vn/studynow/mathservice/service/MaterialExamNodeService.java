package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.MaterialExamNode;
import vn.studynow.mathservice.payload.request.SubmitExamRequest;
import vn.studynow.mathservice.payload.response.SubmitEntranceExamResponse;
import vn.studynow.mathservice.payload.response.SubmitFinalExamResponse;
import vn.studynow.mathservice.service.impl.MaterialExamNodeServiceImpl;

public interface MaterialExamNodeService {

    MaterialExamNode getEntranceExam(MaterialExamNodeServiceImpl.ExamParam examParam) throws Exception;

    MaterialExamNode getFinalExam(MaterialExamNodeServiceImpl.ExamParam examParam) throws Exception;

    SubmitEntranceExamResponse submitEntranceExam(String userId, String deviceId, String deviceType,
                                                  SubmitExamRequest submitExamRequest);

    SubmitFinalExamResponse submitFinalExam(String userId, String deviceId, String deviceType,
                                            SubmitExamRequest submitExamRequest) throws Exception;

}
