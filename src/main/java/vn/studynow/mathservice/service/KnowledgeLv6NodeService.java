package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.KnowledgeLevel6Node;

import java.util.List;

public interface KnowledgeLv6NodeService {

    List<KnowledgeLevel6Node> getKnowledgeLevel6Nodes(String userId, String deviceId, String deviceType, String typesName, String uid)  throws Exception;
}
