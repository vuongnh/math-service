package vn.studynow.mathservice.service.abtesting;

import org.springframework.stereotype.Service;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.model.abtesting.Bucket;
import vn.studynow.mathservice.service.restrequest.RestRequest;

import java.util.HashMap;
import java.util.Map;

@Service
public class AbTestingServiceImpl implements AbTestingService {

    private final RestRequest restRequest;
    private final ApplicationProperties applicationProperties;

    public AbTestingServiceImpl(RestRequest restRequest, ApplicationProperties applicationProperties) {
        this.restRequest = restRequest;
        this.applicationProperties = applicationProperties;
    }


    @Override
    public Bucket findBucket(String testingObjectKey, String userId) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("testing_object", testingObjectKey);
            params.put("user_id", userId);

            Bucket bucket = restRequest.get(applicationProperties.getAbTestingService().getHost() + Urls.FIND_BUCKET,
                    params, Bucket.class);
            return bucket;
        } catch (Exception e) {
            return null;
        }

    }
}
