package vn.studynow.mathservice.service.abtesting;

import vn.studynow.mathservice.model.abtesting.Bucket;

public interface AbTestingService {
    Bucket findBucket(String testingObjectKey, String userId);
}
