package vn.studynow.mathservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.studynow.mathservice.domain.learning.entities.BookmarkedLOL4;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.payload.response.LearningPathResponse;

public interface KnowledgeLv4NodeService {
    Object getLearningPath(GetLearningPathRequest getLearningPathRequest) throws Exception;

    void bookmarkLOL4(BookmarkedLOL4 bookmarkedLOL4);
    Page<BookmarkedLOL4> getBookmarkedLOL4s(String userId, Pageable pageable);
}
