package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.MathConcept;

import java.util.List;

public interface MathConceptService {

   List<MathConcept> getMathConceptRecommend(String knowledgeName) throws Exception;

   MathConcept findById(String id) throws Exception;
}
