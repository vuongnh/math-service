package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.MaterialEntranceExamMotivationPopupNode;
import vn.studynow.mathservice.repository.MaterialMotivationNodeRepository;
import vn.studynow.mathservice.service.MaterialMotivationPopupNodeService;

import java.util.Optional;

@Service
public class MaterialMotivationPopupNodeServiceImpl implements MaterialMotivationPopupNodeService {

    private final Logger log = LoggerFactory.getLogger(MaterialMotivationPopupNodeServiceImpl.class);

    @Autowired
    MaterialMotivationNodeRepository materialMotivationNodeRepository;

    public MaterialEntranceExamMotivationPopupNode getMaterialEntranceExamMotivationPopup(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception{
        try {
            Optional<MaterialEntranceExamMotivationPopupNode> optionalMaterialEntranceExamMotivationPopupNode = materialMotivationNodeRepository.getMaterialEntranceExamMotivationPopupNode(userId, deviceId, deviceType, typesName, uid);

            return optionalMaterialEntranceExamMotivationPopupNode.orElse(null);
        } catch (Exception e) {
            log.error("Error at getMaterialEntranceExamMotivationPopup:" + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }
}
