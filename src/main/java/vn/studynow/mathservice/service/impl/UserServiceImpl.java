package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.UserAvatar;
import vn.studynow.mathservice.model.profile.User;
import vn.studynow.mathservice.payload.request.profile.CreateUserRequest;
import vn.studynow.mathservice.payload.request.profile.UpdateUserRequest;
import vn.studynow.mathservice.repository.UserAvatarRepository;
import vn.studynow.mathservice.repository.UserRepository;
import vn.studynow.mathservice.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserAvatarRepository userAvatarRepository;

    @Autowired
    private Environment environment;

    public User createUser(String userId, CreateUserRequest createUserRequest) throws Exception {
        try {
            Optional<User> optionalUser = userRepository.findById(userId);

            if(optionalUser.isPresent()) {
                throw new Exception("User id existed!");
            } else {
                User user = new User();

                user.setId(userId);
                createUserRequest.mappingData(user);

                userRepository.save(user);

                return user;
            }
        } catch (Exception e) {
            log.error("error at createUser: " + e.getMessage());

            throw e;
        }
    }

    public User updateUser(String userId, UpdateUserRequest updateUserRequest) throws Exception {
        try {
            Optional<User> optionalUser = userRepository.findById(userId);

            if(optionalUser.isPresent()) {
                User user = optionalUser.get();

                String currentName = user.getName();

                if(currentName != null && !currentName.equals("")) {
                    updateUserRequest.setName(currentName);
                }

                updateUserRequest.mappingData(user);

                userRepository.save(user);

                return user;
            } else {
                throw new Exception("User id not exist!");

            }
        } catch (Exception e) {
            log.error("error at updateUser: " + e.getMessage());

            throw e;
        }
    }

    public void deleteUser(String userId) throws Exception {
        try {
            String[] profiles = environment.getActiveProfiles();

            for (String profile: profiles) {
                if(profile.equals("prod")) {
                    throw new Exception("Can not delete user in prod env!");
                }
            }
            userRepository.deleteById(userId);
        } catch (Exception e) {
            log.error("error at deleteUser: " + e.getMessage());

            throw e;
        }
    }

    public User getUser(String userId) throws Exception {
        try {
            Optional<User> optionalUser = userRepository.findById(userId);

            if(optionalUser.isPresent()) {
                return optionalUser.get();
            } else {
                throw new Exception("User id not exist!");

            }
        } catch (Exception e) {
            log.error("error at getUser: " + e.getMessage());

            throw e;
        }
    }

    public List<UserAvatar> getUserAvatar(String userId) throws Exception {
        try {
            return userAvatarRepository.getUserAvatar();
        } catch (Exception e) {
            log.error("error at getUserAvatar: " + e.getMessage());

            throw e;
        }
    }

}
