package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.common.constant.TrackingConstant;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.payload.request.SubmitPractice4Request;
import vn.studynow.mathservice.payload.request.SubmitPractice6Request;
import vn.studynow.mathservice.payload.response.*;
import vn.studynow.mathservice.payload.tracking.Event;
import vn.studynow.mathservice.service.MaterialPracticeNodeService;
import vn.studynow.mathservice.util.MaterialUtil;
import vn.studynow.mathservice.util.TrackingUtil;

@Service
public class MaterialPracticeNodeServiceImpl implements MaterialPracticeNodeService {

    private final Logger log = LoggerFactory.getLogger(MaterialPracticeNodeServiceImpl.class);


    @Autowired
    private MaterialUtil materialUtil;

    @Autowired
    private TrackingUtil trackingUtil;

    @Override
    public SubmitPractice4Response submitPractice4(String userId, String deviceId, String deviceType, SubmitPractice4Request submitPractice4Request) throws Exception {
        try {
            Event event = new Event();

            trackingUtil.setLol4EventParams(event,
                    submitPractice4Request.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitPractice4Request.getCycleId(),
                    submitPractice4Request.getLol0Uid(),
                    submitPractice4Request.getLol1Uid(),
                    submitPractice4Request.getLol2Uid(),
                    submitPractice4Request.getLol3Uid(),
                    submitPractice4Request.getLol4Uid(),
                    submitPractice4Request.getEventTimestamp(),
                    submitPractice4Request.getGrade(),
                    submitPractice4Request.getSubjectId(),
                    submitPractice4Request.getPreCode(),
                    submitPractice4Request.getUid());

//            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitPractice4Request, SubmitPractice4Response.class, MaterialPractice4MotivationNode.class, event, TrackingConstant.LOL4_SUBMIT_PRACTICE);

            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitPractice4Request,
                    SubmitPractice4Response.class, MaterialPractice4MotivationNode.class,event,
                    TrackingConstant.LOL4_SUBMIT_PRACTICE);
        } catch (Exception e) {
            log.error("error at submitPractice4: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public SubmitPractice61Response submitLearningNodeLevel61Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception {
        try {
            Event event = new Event();

            trackingUtil.setLol61EventParams(event,
                    submitPractice6Request.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitPractice6Request.getCycleId(),
                    submitPractice6Request.getLol0Uid(),
                    submitPractice6Request.getLol1Uid(),
                    submitPractice6Request.getLol2Uid(),
                    submitPractice6Request.getLol3Uid(),
                    submitPractice6Request.getLol4Uid(),
                    submitPractice6Request.getLol61Uid(),
                    submitPractice6Request.getEventTimestamp(),
                    submitPractice6Request.getGrade(),
                    submitPractice6Request.getSubjectId(),
                    submitPractice6Request.getPreCode(),
                    submitPractice6Request.getUid());

            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType,
                    submitPractice6Request, SubmitPractice61Response.class,
                    MaterialPractice61MotivationNode.class, event, TrackingConstant.LOL6_1_SUBMIT_QUIZ);
        } catch (Exception e) {
            log.error("error at submitLearningNodeLevel62Practice: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public SubmitPractice62Response submitLearningNodeLevel62Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception {
        try {
            Event event = new Event();

            trackingUtil.setLol62EventParams(event,
                    submitPractice6Request.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitPractice6Request.getCycleId(),
                    submitPractice6Request.getLol0Uid(),
                    submitPractice6Request.getLol1Uid(),
                    submitPractice6Request.getLol2Uid(),
                    submitPractice6Request.getLol3Uid(),
                    submitPractice6Request.getLol4Uid(),
                    submitPractice6Request.getLol62Uid(),
                    submitPractice6Request.getEventTimestamp(),
                    submitPractice6Request.getGrade(),
                    submitPractice6Request.getSubjectId(),
                    submitPractice6Request.getPreCode(),
                    submitPractice6Request.getUid());

            return  materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitPractice6Request,
                    SubmitPractice62Response.class, MaterialPractice62MotivationNode.class, event, TrackingConstant.LOL6_2_SUBMIT_QUIZ);
        } catch (Exception e) {
            log.error("error at submitLearningNodeLevel62Practice: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public SubmitPractice63Response submitLearningNodeLevel63Practice(String userId, String deviceId, String deviceType, SubmitPractice6Request submitPractice6Request) throws Exception {
        try {
            Event event = new Event();

            trackingUtil.setLol63EventParams(event,
                    submitPractice6Request.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitPractice6Request.getCycleId(),
                    submitPractice6Request.getLol0Uid(),
                    submitPractice6Request.getLol1Uid(),
                    submitPractice6Request.getLol2Uid(),
                    submitPractice6Request.getLol3Uid(),
                    submitPractice6Request.getLol4Uid(),
                    submitPractice6Request.getLol63Uid(),
                    submitPractice6Request.getEventTimestamp(),
                    submitPractice6Request.getGrade(),
                    submitPractice6Request.getSubjectId(),
                    submitPractice6Request.getPreCode(),
                    submitPractice6Request.getUid());

            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitPractice6Request,
                    SubmitPractice63Response.class, MaterialPractice63MotivationNode.class, event, TrackingConstant.LOL6_3_SUBMIT_QUIZ);
        } catch (Exception e) {
            log.error("error at submitLearningNodeLevel63Practice: " + e.getMessage());
            throw e;
        }
    }
}
