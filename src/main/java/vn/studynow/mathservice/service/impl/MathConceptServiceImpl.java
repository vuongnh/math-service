package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.MathConcept;
import vn.studynow.mathservice.repository.MathConceptRepository;
import vn.studynow.mathservice.service.MathConceptService;

import java.util.List;

@Service
public class MathConceptServiceImpl implements MathConceptService {

    private final Logger log = LoggerFactory.getLogger(MathConceptServiceImpl.class);

    @Autowired
    MathConceptRepository mathConceptRepository;


    @Override
    public List<MathConcept> getMathConceptRecommend(String knowledgeName) throws Exception {
        try {
            return mathConceptRepository.getMathConceptRecommend(knowledgeName);
        } catch (Exception e){

            log.error("error at getMathConceptRecommend: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public MathConcept findById(String id) throws Exception {
        try {
            return mathConceptRepository.findById(id).orElse(null);
        } catch (Exception e){

            log.error("error at findById: " + e.getMessage());
            throw e;
        }
    }
}
