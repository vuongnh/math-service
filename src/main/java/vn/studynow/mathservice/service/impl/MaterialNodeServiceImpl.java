package vn.studynow.mathservice.service.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.model.history.MaterialHistory;
import vn.studynow.mathservice.repository.MaterialHistoryRepository;
import vn.studynow.mathservice.repository.MaterialNodeRepository;
import vn.studynow.mathservice.service.MaterialNodeService;
import vn.studynow.mathservice.util.ListUtil;
import vn.studynow.mathservice.util.MaterialUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class MaterialNodeServiceImpl implements MaterialNodeService {
    private final Logger log = LoggerFactory.getLogger(MaterialNodeServiceImpl.class);

    @Autowired
    private MaterialNodeRepository materialNodeRepository;

    @Autowired
    private MaterialUtil materialUtil;

    @Autowired
    private MaterialHistoryRepository materialHistoryRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private ListUtil listUtil;

    @Override
    public List<MaterialNode> getLearningNodeLevel4Materials(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception {
        try {
            return getMaterials(userId, deviceId, deviceType, typesName, uid);
        } catch (Exception e) {
            log.error("Error at getLearningNodeLevel4Materials:" + e.getMessage());

            throw e;
        }
    }

    @Override
    public List<MaterialNode> getLearningNodeLevel6Materials(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception {
        try {
            return getMaterials(userId, deviceId, deviceType, typesName, uid);
        } catch (Exception e) {
            log.error("Error at getLearningNodeLevel6Materials:" + e.getMessage());

            throw e;
        }
    }

    private List<MaterialNode> getMaterials(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception {
        try {
            return materialNodeRepository.getMaterials(userId, deviceId, deviceType, typesName, uid);
        } catch (Exception e) {
            log.error("Error at getMaterials:" + e.getMessage());

            throw e;
        }
    }
}
