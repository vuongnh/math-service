package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.KnowledgeLevel6Node;
import vn.studynow.mathservice.repository.KnowledgeLv6NodeRepository;
import vn.studynow.mathservice.service.KnowledgeLv6NodeService;

import java.util.List;

@Service
public class KnowledgeLv6NodeServiceImpl implements KnowledgeLv6NodeService {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv6NodeServiceImpl.class);

    @Autowired
    KnowledgeLv6NodeRepository knowledgeLv6NodeRepository;

    @Override
    public List<KnowledgeLevel6Node> getKnowledgeLevel6Nodes(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception{
        try {
            return knowledgeLv6NodeRepository.getKnowledgeLevel6Nodes(userId, deviceId, deviceType, typesName, uid);

        } catch (Exception e) {
            log.error("Error at getKnowledgeLevel6Nodes:" + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }
}
