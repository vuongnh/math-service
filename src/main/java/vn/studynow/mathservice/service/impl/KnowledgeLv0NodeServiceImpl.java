package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.model.KnowledgeLevel0Node;
import vn.studynow.mathservice.repository.KnowledgeLv0NodeRepository;
import vn.studynow.mathservice.service.KnowledgeLv0NodeService;

import java.util.List;

@Service
public class KnowledgeLv0NodeServiceImpl implements KnowledgeLv0NodeService {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv0NodeServiceImpl.class);

    @Autowired
    KnowledgeLv0NodeRepository knowledgeLv0NodeRepository;

    public Object getKnowledgeLevel0Nodes(String userId, String deviceId, String deviceType)  {
        return knowledgeLv0NodeRepository.getKnowledgeLv0Nodes(userId, deviceId, deviceType);
    }
}
