package vn.studynow.mathservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.common.constant.LearningNodeConstant;
import vn.studynow.mathservice.domain.learning.entities.BookmarkedLOL4;
import vn.studynow.mathservice.model.abtesting.Bucket;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.repository.KnowledgeLv4NodeRepository;
import vn.studynow.mathservice.repository.learning.BookmarkedLOL4Repository;
import vn.studynow.mathservice.service.KnowledgeLv4NodeService;
import vn.studynow.mathservice.service.abtesting.AbTestingService;

@Service
public class KnowledgeLv4NodeServiceImpl implements KnowledgeLv4NodeService {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv4NodeServiceImpl.class);

    @Autowired
    private KnowledgeLv4NodeRepository knowledgeLv4NodeRepository;

    @Autowired
    private BookmarkedLOL4Repository bookmarkedLOL4Repository;

    @Autowired
    AbTestingService abTestingService;

    @Override
    public Object getLearningPath(GetLearningPathRequest getLearningPathRequest) throws Exception {
//        Bucket bucket = abTestingService.findBucket("lol4_a_day", getLearningPathRequest.getUserId());
//
        int numberLol4 = 100;
//        if(bucket != null && bucket.getAttributes().containsKey("number"))
//            numberLol4 = (Integer) bucket.getAttributes().get("number");

        getLearningPathRequest.setNumberLol4(numberLol4);

        return knowledgeLv4NodeRepository.getKnowledgeLevel4Nodes(getLearningPathRequest).getData();
    }

    @Override
    public void bookmarkLOL4(BookmarkedLOL4 bookmarkedLOL4) {
        bookmarkedLOL4Repository.save(bookmarkedLOL4);
    }

    @Override
    public Page<BookmarkedLOL4> getBookmarkedLOL4s(String userId, Pageable pageable) {
        return bookmarkedLOL4Repository.findByUserIdAndBookmarkedIs(userId, true, pageable);
    }

}
