package vn.studynow.mathservice.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.common.constant.LearningNodeConstant;
import vn.studynow.mathservice.model.KnowledgeLevel0Node;
import vn.studynow.mathservice.model.KnowledgeLevel1Node;
import vn.studynow.mathservice.model.KnowledgeLevel2Node;
import vn.studynow.mathservice.repository.KnowledgeLv0NodeRepository;
import vn.studynow.mathservice.service.KnowledgeLv2NodeService;

import java.util.List;

@Service
public class KnowledgeLv2NodeServiceImpl implements KnowledgeLv2NodeService {
    private final Logger log = LoggerFactory.getLogger(KnowledgeLv2NodeServiceImpl.class);

    @Autowired
    KnowledgeLv0NodeRepository knowledgeLv0NodeRepository;

    @Autowired
    ObjectMapper oMapper;

    @Override
    public KnowledgeLevel2Node getRecommendLearningNodeLevel2(String userId, String deviceId, String deviceType) throws Exception {
        try {
            Object knowledgeLevel0NodeObjects = knowledgeLv0NodeRepository.getKnowledgeLv0Nodes(userId, deviceId, deviceType);

            KnowledgeLevel0Node[] knowledgeLevel0Nodes = oMapper.convertValue(knowledgeLevel0NodeObjects, KnowledgeLevel0Node[].class);

            KnowledgeLevel2Node firstNotLearnNode = null;

            for (KnowledgeLevel0Node knowledgeLevel0Node : knowledgeLevel0Nodes) {
                List<KnowledgeLevel1Node> knowledgeLevel1Nodes = knowledgeLevel0Node.getChildNodes();

                for (KnowledgeLevel1Node knowledgeLevel1Node : knowledgeLevel1Nodes) {
                    List<KnowledgeLevel2Node> knowledgeLevel2Nodes = knowledgeLevel1Node.getChildNodes();

                    for (KnowledgeLevel2Node knowledgeLevel2Node : knowledgeLevel2Nodes) {
                        if (knowledgeLevel2Node.getPassStatus() == LearningNodeConstant.LEARNING_NODE_LEVEL2_LEARNING) {
                            return knowledgeLevel2Node;
                        } else if (knowledgeLevel2Node.getPassStatus() == LearningNodeConstant.LEARNING_NODE_LEVEL2_NOT_LEARN) {
                            firstNotLearnNode = knowledgeLevel2Node;
                        }
                    }
                }
            }

            return firstNotLearnNode;
        } catch (Exception e) {
            log.error("Error at getRecommendLearningNodeLevel2:" + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }
}
