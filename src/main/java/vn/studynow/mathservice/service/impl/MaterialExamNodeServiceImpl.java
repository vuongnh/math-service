package vn.studynow.mathservice.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import vn.studynow.mathservice.common.constant.MaterialConstant;
import vn.studynow.mathservice.common.constant.TrackingConstant;
import vn.studynow.mathservice.domain.tracking.entities.TrackingEvent;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.model.history.MaterialHistory;
import vn.studynow.mathservice.payload.request.SubmitExamRequest;
import vn.studynow.mathservice.payload.response.SubmitEntranceExamResponse;
import vn.studynow.mathservice.payload.response.SubmitFinalExamResponse;
import vn.studynow.mathservice.payload.tracking.Event;
import vn.studynow.mathservice.repository.MaterialExamNodeRepository;
import vn.studynow.mathservice.repository.MaterialHistoryRepository;
import vn.studynow.mathservice.service.MaterialExamNodeService;
import vn.studynow.mathservice.util.DateTimeUtils;
import vn.studynow.mathservice.util.MaterialUtil;
import vn.studynow.mathservice.util.TrackingUtil;

import java.util.Optional;

@Service
@Configurable
public class MaterialExamNodeServiceImpl implements MaterialExamNodeService {
    private final Logger log = LoggerFactory.getLogger(MaterialExamNodeServiceImpl.class);

    @Autowired
    private MaterialExamNodeRepository materialExamNodeRepository;

    @Autowired
    private MaterialUtil materialUtil;

    @Autowired
    private TrackingUtil trackingUtil;


    @Autowired
    TrackingService trackingService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MaterialHistoryRepository materialHistoryRepository;

    @Override
    public MaterialExamNode getEntranceExam(ExamParam examParam) throws Exception {
        try {
            examParam.setType(MaterialConstant.TYPE_ENTRANCE_EXAM);
            examParam.setEventName(TrackingConstant.LOL2_INPUT_GET_EXAM);
            return getExam(examParam);
        } catch (Exception e) {
            log.error("Error at getEntranceExam:" + e.getMessage());

            throw e;
        }
    }

    @Override
    public MaterialExamNode getFinalExam(ExamParam examParam) throws Exception {
        try {
            examParam.setType(MaterialConstant.TYPE_FINAL_EXAM);
            examParam.setEventName(TrackingConstant.LOL2_OUTPUT_GET_EXAM);
            return getExam(examParam);
        } catch (Exception e) {
            log.error("Error at getFinalExam:" + e.getMessage());

            throw e;
        }
    }

    @Data
    public static class ExamParam {
        String userId;
        String deviceId;
        String deviceType;
        String typesName;
        String uid;
        String cycleId;
        String lol0Uid;
        String lol1Uid;
        String lol2Uid;
        String lol3Uid;
        Integer grade;
        Integer subjectId;
        Long eventTimestamp;
        Long code;
        String type;
        String eventName;
        String studyType;
    }


    private MaterialExamNode getExam(ExamParam examParam) throws Exception {
        try {
            Optional<MaterialExamNode> optionalMaterialExamNode;

            switch (examParam.getType()) {
                case MaterialConstant.TYPE_ENTRANCE_EXAM:
                    optionalMaterialExamNode = materialExamNodeRepository.getEntranceExam(
                            examParam.getUserId(), examParam.getDeviceId(), examParam.getDeviceType(), examParam.getTypesName(), examParam.getUid()
                    );
                    break;
                case MaterialConstant.TYPE_FINAL_EXAM:
                    optionalMaterialExamNode = materialExamNodeRepository.getFinalExam(
                            examParam.getUserId(), examParam.getDeviceId(), examParam.getDeviceType(), examParam.getTypesName(), examParam.getUid());
                    break;
                default:
                    return null;
            }

            if(optionalMaterialExamNode.isPresent()) {
                MaterialExamNode materialExamNode = optionalMaterialExamNode.get();

                materialUtil.shuffleQuizzes(materialExamNode);

                saveMaterialExamHistory(examParam.getUserId(), examParam.getDeviceId(), materialExamNode);

                TrackingEvent trackingEvent = TrackingEvent.builder()
                        .code(examParam.getCode())
                        .eventName(examParam.getEventName())
                        .cycleId(examParam.getCycleId())
                        .lol0Uid(examParam.getLol0Uid())
                        .lol1Uid(examParam.getLol1Uid())
                        .lol2Uid(examParam.getLol2Uid())
                        .lol3Uid(examParam.getLol3Uid())
                        .grade(examParam.getGrade())
                        .subjectId(examParam.getSubjectId())
                        .version("1.0")
                        .deviceType(examParam.getDeviceType())
                        .eventTimestamp(examParam.getEventTimestamp())
                        .deviceId(examParam.getDeviceId())
                        .userId(examParam.getUserId())
                        .createAt(DateTimeUtils.formatDateTime())
                        .eventDate(DateTimeUtils.formatDate())
                        .studyType(examParam.getStudyType())
                        .build();

                trackingService.sendEventMessage(trackingEvent);
                trackingService.sendMessage(trackingEvent.getEventName(), trackingEvent);


                return materialExamNode;
            } else {
                throw new Exception("Can not get Exam!");
            }
        } catch (Exception e) {
            log.error("Error at getExam:" + e.getMessage());

            throw e;
        }
    }

    private void saveMaterialExamHistory(String userId, String deviceId, MaterialExamNode materialExamNode) {
        MaterialHistory materialHistory = new MaterialHistory();
        materialHistory.setUserId(userId);
        materialHistory.setDeviceId(deviceId);
        materialHistory.setMaterialCode(materialExamNode.getMaterialCode());
        materialHistory.setMaterials(materialExamNode.getMaterials());

        materialHistoryRepository.save(materialHistory);
    }

    @Override
    public SubmitEntranceExamResponse submitEntranceExam(String userId, String deviceId, String deviceType, SubmitExamRequest submitExamRequest) {
            Event event = new Event();

            trackingUtil.setLol2EventParams(event,
                    submitExamRequest.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitExamRequest.getCycleId(),
                    submitExamRequest.getLol0Uid(),
                    submitExamRequest.getLol1Uid(),
                    submitExamRequest.getLol2Uid(),
                    submitExamRequest.getEventTimestamp(),
                    submitExamRequest.getGrade(),
                    submitExamRequest.getSubjectId(),
                    submitExamRequest.getPreCode(),
                    submitExamRequest.getUid());


//            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitExamRequest, SubmitEntranceExamResponse.class, MaterialEntranceExamMotivationNode.class, event, TrackingConstant.LOL2_INPUT_SUBMIT_EXAM);
            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitExamRequest,
                    SubmitEntranceExamResponse.class, MaterialEntranceExamMotivationNode.class, event,
                    TrackingConstant.LOL2_INPUT_SUBMIT_EXAM );

    }

    @Override
    public SubmitFinalExamResponse submitFinalExam(String userId, String deviceId, String deviceType, SubmitExamRequest submitExamRequest) throws Exception {
        try {
            Event event = new Event();

            trackingUtil.setLol2EventParams(event,
                    submitExamRequest.getCode(),
                    userId,
                    deviceId,
                    deviceType,
                    submitExamRequest.getCycleId(),
                    submitExamRequest.getLol0Uid(),
                    submitExamRequest.getLol1Uid(),
                    submitExamRequest.getLol2Uid(),
                    submitExamRequest.getEventTimestamp(),
                    submitExamRequest.getGrade(),
                    submitExamRequest.getSubjectId(),
                    submitExamRequest.getPreCode(),
                    submitExamRequest.getUid());

//            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType, submitExamRequest, SubmitFinalExamResponse.class, MaterialFinalExamMotivationNode.class, event, TrackingConstant.LOL2_OUTPUT_SUBMIT_EXAM);
            return materialUtil.getQuizAnswerMotivation(userId, deviceId, deviceType,
                    submitExamRequest, SubmitFinalExamResponse.class, MaterialFinalExamMotivationNode.class,event,
                    TrackingConstant.LOL2_OUTPUT_SUBMIT_EXAM);
        } catch (Exception e) {
            log.error("error at submitFinalExam: " + e.getMessage());
            throw e;
        }
    }
}
