package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.MaterialEntranceExamMotivationPopupNode;

public interface MaterialMotivationPopupNodeService {

    MaterialEntranceExamMotivationPopupNode getMaterialEntranceExamMotivationPopup(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception;
}
