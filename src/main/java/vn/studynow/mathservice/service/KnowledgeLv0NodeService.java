package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.KnowledgeLevel0Node;

import java.util.List;

public interface KnowledgeLv0NodeService {
    Object getKnowledgeLevel0Nodes(String userId, String deviceId, String deviceType);
}
