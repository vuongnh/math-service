package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.MaterialNode;

import java.util.List;

public interface MaterialNodeService {

    List<MaterialNode> getLearningNodeLevel4Materials(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception;
    List<MaterialNode> getLearningNodeLevel6Materials(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception;
}
