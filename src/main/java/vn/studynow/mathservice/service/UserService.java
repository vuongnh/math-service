package vn.studynow.mathservice.service;

import vn.studynow.mathservice.model.UserAvatar;
import vn.studynow.mathservice.model.profile.User;
import vn.studynow.mathservice.payload.request.profile.CreateUserRequest;
import vn.studynow.mathservice.payload.request.profile.UpdateUserRequest;

import java.util.List;

public interface UserService {
    User createUser(String userId, CreateUserRequest createUserRequest) throws Exception;
    User updateUser(String userId, UpdateUserRequest updateUserRequest) throws Exception;
    void deleteUser(String userId) throws Exception;
    User getUser(String userId) throws Exception;
    List<UserAvatar> getUserAvatar(String userId) throws Exception;
}
