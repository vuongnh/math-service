package vn.studynow.mathservice.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.studynow.mathservice.common.constant.MaterialConstant;
import vn.studynow.mathservice.domain.tracking.entities.TrackingResult;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.payload.body.QuizResultTrackingBody;
import vn.studynow.mathservice.payload.request.Quiz;
import vn.studynow.mathservice.payload.request.*;
import vn.studynow.mathservice.payload.response.SubmitQuizzesResponse;
import vn.studynow.mathservice.payload.tracking.Event;
import vn.studynow.mathservice.payload.tracking.QuestionParams;
import vn.studynow.mathservice.repository.MaterialHistoryRepository;
import vn.studynow.mathservice.repository.MaterialMotivationNodeRepository;
import vn.studynow.mathservice.repository.MaterialNodeRepository;
import vn.studynow.mathservice.repository.impl.MaterialMotivationNodeRepoImpl;
import vn.studynow.mathservice.util.kafka.Producer;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class MaterialUtil {
    private final Logger log = LoggerFactory.getLogger(MaterialUtil.class);

    @Autowired
    private ListUtil listUtil;

    @Autowired
    private MaterialNodeRepository materialNodeRepository;

    @Autowired
    private MaterialMotivationNodeRepository materialMotivationNodeRepository;

    @Autowired
    private Producer producer;

    @Autowired
    private TrackingService trackingService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MaterialHistoryRepository materialHistoryRepository;

    public void shuffleQuizzes(MaterialQuizNode materialQuizNode) {
        List<vn.studynow.mathservice.model.Quiz> quizzes = materialQuizNode.getMaterials();

        quizzes = listUtil.shuffleList(quizzes);

        for (vn.studynow.mathservice.model.Quiz quiz : quizzes) {
            shuffleAnswers(quiz);
        }

        materialQuizNode.setMaterials(quizzes);
    }

    public void shuffleAnswers(vn.studynow.mathservice.model.Quiz quiz) {
        List<Answer> answers = quiz.getAnswers();

        if (!answers.isEmpty()) {
            Answer correctAnswer = answers.get(0);

            answers = listUtil.shuffleList(answers);

            quiz.setCorrectAnswer(correctAnswer.getUid());
            quiz.setAnswers(answers);
        }
    }

    public <S extends SubmitQuizzesRequest,
            R extends SubmitQuizzesResponse,
            M extends MaterialMotivationNode>
    R getQuizAnswerMotivation(String userId,
                              String deviceId,
                              String deviceType,
                              S submitRequest,
                              Class<R> responseType,
                              Class<M> motivationType,
                              Event event,
                              String topicName) {



            //        String typesName = submitRequest.getTypesName();
//        String uid = submitRequest.getUid();
            List<Quiz> userSubmits = submitRequest.getQuestionParams();
            String cycleNodeTypesName = submitRequest.getCycleNodeTypesName();
            String cycleNodeUid = submitRequest.getCycleNodeUid();
            Integer time = submitRequest.getTime();
            Integer timeLimit = submitRequest.getTimeLimit();

            String materialCode = submitRequest.getMaterialCode();

            String cycleNodeLevel2Uid = null;
            String cycleNodeLevel2TypesName = null;
            String cycleNodeLevel4Uid = null;
            String cycleNodeLevel4TypesName = null;

            if (submitRequest instanceof SubmitExamRequest) {
                cycleNodeLevel2Uid = ((SubmitExamRequest) submitRequest).getCycleNodeLevel2Uid();
                cycleNodeLevel2TypesName = ((SubmitExamRequest) submitRequest).getCycleNodeLevel2TypesName();
            }

            if (submitRequest instanceof SubmitPractice6Request) {
                SubmitPractice6Request submitPractice6Request = (SubmitPractice6Request) submitRequest;
                cycleNodeLevel4Uid = submitPractice6Request.getCycleNodeLevel4Uid();
                cycleNodeLevel4TypesName = submitPractice6Request.getCycleNodeLevel4TypesName();
            }

            int answeredQuestion = userSubmits.size();
            int totalQuestion = answeredQuestion;
            int correctAnswerCount = 0;

            List<QuestionParams> questionParamsList = new ArrayList<>();

            for (Quiz userSubmit : userSubmits) {

                List<AnswerChoice> listAnswerChoice = userSubmit.getAnswerChoices();

                boolean isCorrectAnswer = true;
                for (AnswerChoice answerChoice : listAnswerChoice) {
                    if (answerChoice.getUserAnswer() == null || !answerChoice.getUserAnswer().equals(answerChoice.getCorrectAnswer())) {
                        isCorrectAnswer = false;
                        break;
                    }
                }

                QuestionParams questionParams = new QuestionParams();

                if (isCorrectAnswer) {
                    correctAnswerCount++;
                    questionParams.setStatus(MaterialConstant.ANSWER_QUIZ_TRUE_STATUS);
                } else {
                    questionParams.setStatus(MaterialConstant.ANSWER_QUIZ_WRONG_STATUS);
                }

                questionParams.setQuestionId(userSubmit.getUid());
                questionParams.setQuestionText(userSubmit.getQuestion());
                questionParams.setAnswerText(userSubmit.getAnswerDetail());
                questionParams.setAnswerList(userSubmit.getListAnswerOfQuiz());
                questionParams.setAnswerChoices(userSubmit.getAnswerChoices());
                questionParams.setLolUid(userSubmit.getLolUid());
                questionParams.setTypeQuestion(userSubmit.getTypeQuestion());
                questionParams.setStartTime(userSubmit.getStartTime());
                questionParams.setEndTime(userSubmit.getEndTime());
                questionParams.setTimeAnswer(userSubmit.getTimeAnswer());

                questionParamsList.add(questionParams);

            }

            QuizResultTrackingBody quizResultTrackingBody = new QuizResultTrackingBody();
            quizResultTrackingBody.setQuestionParams(questionParamsList);

            MaterialMotivationNode materialMotivationNode;

            if (motivationType == MaterialEntranceExamMotivationNode.class) {
                materialMotivationNode =
                        getMaterialEntranceExamMotivationNode(userId, deviceId,
                                deviceType, cycleNodeLevel2Uid, cycleNodeLevel2TypesName,
                                cycleNodeUid, cycleNodeTypesName, totalQuestion, correctAnswerCount,
                                materialCode, timeLimit, time, quizResultTrackingBody);
            } else if (motivationType == MaterialFinalExamMotivationNode.class) {
                materialMotivationNode = getMaterialFinalExamMotivationNode(userId, deviceId,
                        deviceType, cycleNodeLevel2Uid, cycleNodeLevel2TypesName,
                        cycleNodeUid, cycleNodeTypesName, totalQuestion, correctAnswerCount,
                        materialCode, timeLimit, time, quizResultTrackingBody);
            } else if (motivationType == MaterialPractice4MotivationNode.class) {
                materialMotivationNode = getMaterialPractice4MotivationNode(userId,
                        deviceId, deviceType, cycleNodeUid, cycleNodeTypesName,
                        totalQuestion, answeredQuestion, correctAnswerCount,
                        materialCode, time, quizResultTrackingBody);
            } else if (motivationType == MaterialPractice63MotivationNode.class ||
                    motivationType == MaterialPractice62MotivationNode.class ||
                    motivationType == MaterialPractice61MotivationNode.class) {

                MaterialMotivationNodeRepoImpl.PracticeMotivationParam param = new MaterialMotivationNodeRepoImpl.PracticeMotivationParam();
                param.setUserId(userId);
                param.setDeviceId(deviceId);
                param.setDeviceType(deviceType);
                param.setCycleNodeUid(cycleNodeUid);
                param.setCycleNodeTypesName(cycleNodeTypesName);
                param.setLol4CycleUid(cycleNodeLevel4Uid);
                param.setLol4CycleTypesName(cycleNodeLevel4TypesName);
                param.setTotalQuestion(totalQuestion);
                param.setAnsweredQuestion(answeredQuestion);
                param.setCorrectAnswer(correctAnswerCount);
                param.setMaterialCode(materialCode);
                param.setTimeElapsed(time);
                param.setStudyType(submitRequest.getStudyType());
                param.setCode(submitRequest.getCode());


                if (motivationType == MaterialPractice63MotivationNode.class) {
                    materialMotivationNode = getMaterialPractice63MotivationNode(param, quizResultTrackingBody);
                } else if (motivationType == MaterialPractice62MotivationNode.class) {
                    materialMotivationNode = getMaterialPractice62MotivationNode(param, quizResultTrackingBody);
                } else {
                    materialMotivationNode = getMaterialPractice61MotivationNode(param, quizResultTrackingBody);
                }
            } else {
                return null;
            }

            Float point = calculatePoint(correctAnswerCount, totalQuestion);

        R response = null;
        try {
            response = (responseType.getConstructor()).newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.error(e.getMessage());
            return null;
        }


        response.setMotivation(materialMotivationNode);
            response.setCorrectAnswer(correctAnswerCount);
            response.setWrongAnswer(totalQuestion - correctAnswerCount);
            response.setPoint(point);

        TrackingResult trackingResult = TrackingResult.builder()
                .code(submitRequest.getCode())
                .userId(userId)
                .deviceId(deviceId)
                .grade(submitRequest.getGrade())
                .subjectId(submitRequest.getSubjectId())
                .cycleId(submitRequest.getCycleId())
                .questionParams(questionParamsList)
                .totalTimeAnswer(time)
                .timeLimit(submitRequest.getTimeLimit())
                .numAnswer(answeredQuestion)
                .numQuestion(totalQuestion)
                .numCorrectAnswer(correctAnswerCount)
                .point(point)
                .materialCode(submitRequest.getMaterialCode())
                .version("1.0")
                .eventTimestamp(submitRequest.getEventTimestamp())
                .lol0Uid(event.getLol0Uid())
                .lol1Uid(event.getLol1Uid())
                .lol2Uid(event.getLol2Uid())
                .lol3Uid(event.getLol3Uid())
                .lol4Uid(event.getLol4Uid())
                .lol61Uid(event.getLol61Uid())
                .lol62Uid(event.getLol62Uid())
                .lol63Uid(event.getLol63Uid())
                .eventName(submitRequest.getEventName())
                .deviceType(deviceType)
                .createAt(DateTimeUtils.formatDateTime())
                .eventDate(DateTimeUtils.formatDate())
                .studyType(submitRequest.getStudyType())
                .materialCode(submitRequest.getMaterialCode())
                .appVersion(submitRequest.getAppVersion())
                .build();

        trackingService.sendResultMessage(trackingResult);
        trackingService.sendMessage(trackingResult.getEventName(), trackingResult);

        return response;

    }

    public Float calculatePoint(Integer numOfCorrectAnswer, Integer total) {

        DecimalFormat format = new DecimalFormat("#.##");

        Float point = ((float) numOfCorrectAnswer / (float) total) * MaterialConstant.MAX_POINT;
        return Float.valueOf(format.format(point));

    }


    private MaterialQuizNode getMaterialQuizNode(String userId, String deviceId, String deviceType, String typesName, String uid) {
        Optional<MaterialNode> optionalMaterialNode = materialNodeRepository.getMaterial(userId, deviceId, deviceType, typesName, uid);
        return (MaterialQuizNode) optionalMaterialNode.orElse(null);
    }

    private MaterialEntranceExamMotivationNode getMaterialEntranceExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body) {
        Optional<MaterialEntranceExamMotivationNode> optionalMaterialEntranceExamMotivationNode = materialMotivationNodeRepository.getMaterialEntranceExamMotivationNode(userId, deviceId, deviceType, cycleNodeLevel2Uid, cycleNodeLevel2TypesName, cycleNodeUid, cycleNodeTypesName, totalQuestion, correctAnswer, materialCode, timeLimit, timeElapsed, body);
        return optionalMaterialEntranceExamMotivationNode.orElse(null);
    }

    private MaterialFinalExamMotivationNode getMaterialFinalExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body) {
        Optional<MaterialFinalExamMotivationNode> optionalMaterialFinalExamMotivationNode = materialMotivationNodeRepository.getMaterialFinalExamMotivationNode(userId, deviceId, deviceType, cycleNodeLevel2Uid, cycleNodeLevel2TypesName, cycleNodeUid, cycleNodeTypesName, totalQuestion, correctAnswer, materialCode, timeLimit, timeElapsed, body);
        return optionalMaterialFinalExamMotivationNode.orElse(null);
    }

    private MaterialPractice4MotivationNode getMaterialPractice4MotivationNode(String userId, String deviceId, String deviceType, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer answeredQuestion, Integer correctAnswer, String materialCode, Integer timeElapsed, Object body) {
        Optional<MaterialPractice4MotivationNode> optionalMaterialPractice4MotivationNode = materialMotivationNodeRepository.getMaterialPractice4MotivationNode(userId, deviceId, deviceType, cycleNodeUid, cycleNodeTypesName, totalQuestion, answeredQuestion, correctAnswer, materialCode, timeElapsed, body);
        return optionalMaterialPractice4MotivationNode.orElse(null);
    }

    private MaterialPractice61MotivationNode getMaterialPractice61MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body) {
        try {
            Optional<MaterialPractice61MotivationNode> optionalMaterialPractice61MotivationNode =
                    materialMotivationNodeRepository.getMaterialPractice61MotivationNode(practiceMotivationParam, body);

            return optionalMaterialPractice61MotivationNode.orElse(null);
        } catch (Exception e) {
            log.error("error at getMaterialPractice61MotivationNode: " + e.getMessage());
            throw e;
        }
    }

    private MaterialPractice62MotivationNode getMaterialPractice62MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body) {
        try {
            Optional<MaterialPractice62MotivationNode> optionalMaterialPractice62MotivationNode =
                    materialMotivationNodeRepository.getMaterialPractice62MotivationNode(practiceMotivationParam, body);

            return optionalMaterialPractice62MotivationNode.orElse(null);
        } catch (Exception e) {
            log.error("error at getMaterialPractice62MotivationNode: " + e.getMessage());
            throw e;
        }
    }

    private MaterialPractice63MotivationNode getMaterialPractice63MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body) {
        try {
            Optional<MaterialPractice63MotivationNode> optionalMaterialPractice63MotivationNode =
                    materialMotivationNodeRepository.getMaterialPractice63MotivationNode(practiceMotivationParam, body);

            return optionalMaterialPractice63MotivationNode.orElse(null);
        } catch (Exception e) {
            log.error("error at getMaterialPractice63MotivationNode: " + e.getMessage());
            throw e;
        }
    }
}
