package vn.studynow.mathservice.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {
    static SimpleDateFormat formatter = new
            SimpleDateFormat("yyyyMMdd");

    static ZoneId zoneId = ZoneId.of("Asia/Bangkok");
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    public static String formatDate() {
        ZonedDateTime dateTime = ZonedDateTime.now(zoneId);
        return dateFormatter.format(dateTime);
    }


    public static String formatDateTime() {
        ZonedDateTime dateTime = ZonedDateTime.now(zoneId);
        return dateTimeFormatter.format(dateTime);
    }

}
