package vn.studynow.mathservice.util.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    private static final Logger log = LoggerFactory.getLogger(Producer.class);



    @Autowired
    private KafkaTemplate<String, String> kafkaTemplates;

    @Autowired
    private ObjectMapper mapper;

    public void sendMessage2(String topic, Object message) {
        try {
            kafkaTemplates.send(topic, mapper.writeValueAsString(message));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
