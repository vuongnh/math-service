package vn.studynow.mathservice.util;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import vn.studynow.mathservice.common.constant.ServiceConstant;

import java.sql.Timestamp;
import java.util.Map;
import java.util.Objects;

public class TrackingServiceUtils extends CallApiUtil {

    public static boolean isApiSuccess(ResponseEntity responseEntity){

        String result = Objects.requireNonNull(responseEntity.getBody()).toString();

        JSONObject jsonObject = new JSONObject(result);
        JSONObject message = (JSONObject) jsonObject.get(ServiceConstant.TrackingService.Response.MESSAGE);
        boolean success = message.getBoolean(ServiceConstant.TrackingService.Response.SUCCESS);

        return responseEntity.getStatusCode().is2xxSuccessful() && success;
    }

    public static Map<String, String> setCommonParams(Map<String, String> params, String userId, String deviceId, String deviceType, Integer grade, Integer subjectId, String version) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        params.put(ServiceConstant.TrackingService.Request.PARAMETER_USER_ID, userId);
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_DEVICE_ID, deviceId);
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_DEVICE_TYPE, deviceType);
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_GRADE, grade.toString());
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_SUBJECT_ID, subjectId.toString());
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_VERSION, version);
        params.put(ServiceConstant.TrackingService.Request.PARAMETER_TIMESTAMP, Long.toString(timestamp.getTime()));

        return params;
    }
}
