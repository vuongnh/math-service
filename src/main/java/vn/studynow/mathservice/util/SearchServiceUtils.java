package vn.studynow.mathservice.util;

import org.springframework.http.ResponseEntity;
import vn.studynow.mathservice.common.constant.ServiceConstant;

import java.util.Objects;

public class SearchServiceUtils extends CallApiUtil{

    public static boolean isApiSuccess(ResponseEntity responseEntity){

        String result = Objects.requireNonNull(responseEntity.getBody()).toString();

        String status = JsonParserUtils.getJsonString(result, ServiceConstant.LearningService.Response.SUCCESS);

        return responseEntity.getStatusCode().is2xxSuccessful() && status.equals("true");
    }
}
