package vn.studynow.mathservice.util;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ListUtil {

    public <T extends Object> List<T> shuffleList(List<T> list) {
        Collections.shuffle(list);

        return list;
    }

    public <T extends Object> T firstInList(List<T> list, Function<T, Boolean> function) {
        for (T item : list) {
            if(function.apply(item)) {
                return item;
            }
        }

        return null;
    }

    public <T extends Object> List<T> filterList(List<T> list, Function<T, Boolean> function) {
        Stream<T> filterStream = list.stream()
                .filter(function::apply);

        return filterStream.collect(Collectors.toList());
    }

}
