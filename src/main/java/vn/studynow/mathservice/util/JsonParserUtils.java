package vn.studynow.mathservice.util;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class JsonParserUtils {
    public static String getJsonString(String input, String key){
        JSONObject jsonResponse = new JSONObject(input);

        return jsonResponse.get(key).toString();
    }
}
