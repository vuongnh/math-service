package vn.studynow.mathservice.util;

import org.springframework.stereotype.Component;
import vn.studynow.mathservice.payload.tracking.Event;

@Component
public class TrackingUtil {
    public void setLol2EventParams (Event event,
                                    Long code,
                                      String userId,
                                      String deviceId,
                                      String deviceType,
                                      String cycleId,
                                      String lol0Uid,
                                      String lol1Uid,
                                      String lol2Uid,
                                      Long eventTimestamp,
                                      Integer grade,
                                      Integer subjectId,
                                      Long preCode,
                                      String materialId) {
        event.setCode(code);
        event.setCycleId(cycleId);
        event.setLol0Uid(lol0Uid);
        event.setLol1Uid(lol1Uid);
        event.setLol2Uid(lol2Uid);
        event.setDeviceType(deviceType);
        event.setUserId(userId);
        event.setDeviceId(deviceId);
        event.setEventTimestamp(eventTimestamp);
        event.setGrade(grade);
        event.setSubjectId(subjectId);
        event.setPreCode(preCode);
    }

    public void setLol4EventParams (Event event,
                                    Long code,
                                      String userId,
                                      String deviceId,
                                      String deviceType,
                                      String cycleId,
                                      String lol0Uid,
                                      String lol1Uid,
                                      String lol2Uid,
                                      String lol3Uid,
                                      String lol4Uid,
                                      Long eventTimestamp,
                                      Integer grade,
                                      Integer subjectId,
                                    Long preCode,
                                      String materialId) {
        setLol2EventParams(event, code, userId, deviceId, deviceType, cycleId, lol0Uid, lol1Uid, lol2Uid, eventTimestamp, grade, subjectId, preCode, materialId);
        event.setLol3Uid(lol3Uid);
        event.setLol4Uid(lol4Uid);
    }

    public void setLol61EventParams (Event event,
                                     Long code,
                                       String userId,
                                       String deviceId,
                                       String deviceType,
                                       String cycleId,
                                       String lol0Uid,
                                       String lol1Uid,
                                       String lol2Uid,
                                       String lol3Uid,
                                       String lol4Uid,
                                       String lol61Uid,
                                     Long eventTimestamp,
                                       Integer grade, Integer subjectId,
                                     Long preCode,
                                       String materialId) {
        setLol4EventParams(event, code, userId, deviceId, deviceType, cycleId, lol0Uid, lol1Uid, lol2Uid, lol3Uid, lol4Uid, eventTimestamp, grade, subjectId, preCode, materialId);
        event.setLol61Uid(lol61Uid);
    }

    public void setLol62EventParams (Event event,
                                     Long code,
                                       String userId,
                                       String deviceId,
                                       String deviceType,
                                       String cycleId,
                                       String lol0Uid,
                                       String lol1Uid,
                                       String lol2Uid,
                                       String lol3Uid,
                                       String lol4Uid,
                                       String lol62Uid,
                                     Long eventTimestamp,
                                       Integer grade,
                                       Integer subjectId,
                                     Long preCode,
                                       String materialId) {
        setLol4EventParams(event, code, userId, deviceId, deviceType, cycleId, lol0Uid, lol1Uid, lol2Uid, lol3Uid, lol4Uid, eventTimestamp, grade, subjectId, preCode, materialId);
        event.setLol62Uid(lol62Uid);
    }

    public void setLol63EventParams (Event event,
                                     Long code,
                                       String userId,
                                       String deviceId,
                                       String deviceType,
                                       String cycleId,
                                       String lol0Uid,
                                       String lol1Uid,
                                       String lol2Uid,
                                       String lol3Uid,
                                       String lol4Uid,
                                       String lol63Uid,
                                     Long eventTimestamp,
                                       Integer grade,
                                       Integer subjectId,
                                     Long preCode,
                                       String materialId) {
        setLol4EventParams(event, code, userId, deviceId, deviceType, cycleId, lol0Uid, lol1Uid, lol2Uid, lol3Uid, lol4Uid, eventTimestamp, grade, subjectId, preCode, materialId);
        event.setLol63Uid(lol63Uid);
    }
}
