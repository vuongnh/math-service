package vn.studynow.mathservice.util;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import vn.studynow.mathservice.common.constant.ServiceConstant;

import java.sql.Timestamp;
import java.util.Map;
import java.util.Objects;

public class LearningServiceUtils extends CallApiUtil {

    public static boolean isApiSuccess(ResponseEntity responseEntity){

        String result = Objects.requireNonNull(responseEntity.getBody()).toString();

        JSONObject jsonObject = new JSONObject(result);
        JSONObject message = (JSONObject) jsonObject.get(ServiceConstant.LearningService.Response.MESSAGE);
        boolean success = message.getBoolean(ServiceConstant.LearningService.Response.SUCCESS);

        return responseEntity.getStatusCode().is2xxSuccessful() && success;
    }

    public static Map<String, String> setCommonParams(Map<String, String> params, String userId, String deviceId, String deviceType) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        params.put(ServiceConstant.LearningService.Request.PARAMETER_USER_ID, userId);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_DEVICE_ID, deviceId);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_DEVICE_TYPE, deviceType);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_TIMESTAMP, Long.toString(timestamp.getTime() / 1000));

        return params;
    }

    public static Map<String, String> setCommonMaterialParams(Map<String, String> params, String typesName, String uid) {
        params.put(ServiceConstant.LearningService.Request.PARAMETER_TYPES_NAME, typesName);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_UID, uid);

        return params;
    }
}
