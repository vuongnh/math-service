package vn.studynow.mathservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.RequestInfoExtractor;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.domain.tracking.entities.TrackingEvent;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.exception.BadRequestException;
import vn.studynow.mathservice.payload.response.ApiSuccessfulResponse;
import vn.studynow.mathservice.payload.tracking.Event;
import vn.studynow.mathservice.util.DateTimeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


@RestController
@RequestMapping("/api/v1")
public class TrackingController {

    private final Logger log = LoggerFactory.getLogger(TrackingController.class);

    @Autowired
    TrackingService trackingService;

    @PostMapping(value = "/submit-tracking-event")
    public ResponseEntity<?> submitTrackingEvent(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                 @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                 @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                 @RequestBody @ApiParam(value = "body có cấu trúc như mô tả, có kiểu json") String body,
                                                 HttpMethod method, HttpServletRequest request, HttpServletResponse response
                                                 ) throws Exception {
        try {
            Event event = new ObjectMapper().readValue(body, Event.class);
            trackingService.sendEventMessage(TrackingEvent.builder()
                    .code(event.getCode())
                    .eventName(event.getEventName())
                    .cycleId(event.getCycleId())
                    .lol0Uid(event.getLol0Uid())
                    .lol1Uid(event.getLol1Uid())
                    .lol2Uid(event.getLol2Uid())
                    .lol3Uid(event.getLol3Uid())
                    .lol4Uid(event.getLol4Uid())
                    .lol61Uid(event.getLol61Uid())
                    .lol62Uid(event.getLol62Uid())
                    .lol63Uid(event.getLol63Uid())
                    .userId(userId)
                    .deviceId(deviceId)
                    .deviceType(deviceType)
                    .grade(event.getGrade())
                    .subjectId(event.getSubjectId())
                    .version("1.0")
                    .eventTimestamp(event.getEventTimestamp())
                    .preCode(event.getPreCode())
                    .videoParams(event.getVideoParams())
                    .viewAnswerParams(event.getViewAnswerParams())
                    .totalViewTime(event.getTotalTimeAnswer())
                    .createAt(DateTimeUtils.formatDateTime())
                    .eventDate(DateTimeUtils.formatDate())
                    .studyType(event.getStudyType())
                    .appVersion(event.getAppVersion())
                    .build());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        trackingService.sendMessageRequest(request, method, body);

        return ResponseEntity.ok(new ApiSuccessfulResponse());
    }
}
