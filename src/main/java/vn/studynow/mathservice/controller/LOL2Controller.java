package vn.studynow.mathservice.controller;

import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.domain.notify.NotificationService;
import vn.studynow.mathservice.domain.notify.SubscribeLO2Request;
import vn.studynow.mathservice.domain.notify.SubscribeNotificationRequest;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.payload.request.SubmitExamRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.payload.response.ApiSuccessfulResponse;
import vn.studynow.mathservice.payload.response.SubmitEntranceExamResponse;
import vn.studynow.mathservice.payload.response.SubmitFinalExamResponse;
import vn.studynow.mathservice.service.*;
import vn.studynow.mathservice.service.impl.MaterialExamNodeServiceImpl;
import vn.studynow.mathservice.service.restrequest.RestRequest;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/thematic/lol2")
public class LOL2Controller {
    private final Logger log = LoggerFactory.getLogger(LOL2Controller.class);

    @Autowired
    RestRequest forwarder;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    private KnowledgeLv2NodeService knowledgeLv2NodeService;





    @Autowired
    NotificationService notificationService;

    private Map<String, String> mappingUrl = new HashMap<>();

    @GetMapping(value = "/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_2_DESC)
    })
    public ApiDefaultResponse getKnowledgeLv0Nodes(
            @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
            @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
            @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
            HttpServletRequest request) {

        Object data =  ((Map)forwarder.getData(applicationProperties.getLearningService().getApiGetLearningNodesLevel0(),
                ImmutableMap.of(
                        ServiceConstant.LearningService.Request.PARAMETER_USER_ID, userId,
                        ServiceConstant.LearningService.Request.PARAMETER_DEVICE_ID, deviceId,
                        ServiceConstant.LearningService.Request.PARAMETER_DEVICE_TYPE , deviceType
                )
        )).get("cycle");
        return new ApiSuccessfulResponse(data);
    }


    @GetMapping(value = "/detail")
    public ResponseEntity<String> getLol2Detail
            (
                    @RequestParam String uid,
                    @RequestParam String types_name,
                    @RequestParam String user_id,
                    @RequestParam String device_id,
                    @RequestParam(required = false) String grade,
                    @RequestParam(required = false) String subject_id,
                    @RequestParam Long event_timestamp,
                    HttpServletRequest request,
                    HttpServletResponse response) throws Exception {

        return Forwarder.forwardWithPath(
                applicationProperties.getLearningService().getHost(),
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                HttpMethod.GET, request, "");


    }


    @PostMapping(value = "/subscribe-notification")
    public ResponseEntity<?> subscribeLOL2Notification(@Valid @RequestBody SubscribeLO2Request subscribeLO2Request) {

        SubscribeNotificationRequest subscribeNotificationRequest = new SubscribeNotificationRequest();
        subscribeNotificationRequest.setUser_id(subscribeLO2Request.getUser_id());
        subscribeNotificationRequest.setTopic_id(subscribeLO2Request.getTypes_name());
        subscribeNotificationRequest.setTopic_id(subscribeLO2Request.getUid());
        subscribeNotificationRequest.setSubscribe(subscribeLO2Request.getSubscribe());

        notificationService.subscribeComingSoonObject(subscribeNotificationRequest);
        return ResponseEntity.ok(new ApiSuccessfulResponse());

    }



    @GetMapping(value = "/suggestion")
    public ResponseEntity<?> getRecommendLearningNodeLevel2(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                                            @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                                            @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType) {
        try {
            KnowledgeLevel2Node recommendNode = knowledgeLv2NodeService.getRecommendLearningNodeLevel2(userId, deviceId, deviceType);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", recommendNode));
        } catch (Exception e) {
            log.error("Error at getRecommendLearningNodeLevel2:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }



    @Autowired
    MaterialExamNodeService materialExamNodeService;

    @GetMapping("/exam/entrance")
    public ResponseEntity<?> getEntranceExam(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                             @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                             @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                             @RequestParam(Parameter.TYPES_NAME) String typesName,
                                             @RequestParam(Parameter.UID) String uid,
                                             @RequestParam(Parameter.CYCLE_ID) String cycleId,
                                             @RequestParam(Parameter.LOL0_UID) String lol0Uid,
                                             @RequestParam(Parameter.LOL1_UID) String lol1Uid,
                                             @RequestParam(value = Parameter.LOL2_UID, required = false) String lol2Uid,
                                             @RequestParam(value = Parameter.LOL3_UID, required = false) String lol3Uid,
                                             @RequestParam(Parameter.GRADE) Integer grade,
                                             @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                             @RequestParam(Parameter.EVENT_TIMESTAMP) Long eventTimestamp,
                                             @RequestParam(Parameter.CODE) Long code,
                                             @RequestParam(value = Parameter.STUDY_TYPE, required = false)  String studyType) {
        try {
            MaterialExamNodeServiceImpl.ExamParam examParam = new MaterialExamNodeServiceImpl.ExamParam();
            examParam.setUserId(userId);
            examParam.setDeviceId(deviceId);
            examParam.setDeviceType(deviceType);
            examParam.setTypesName(typesName);
            examParam.setUid(uid);
            examParam.setCycleId(cycleId);
            examParam.setLol0Uid(lol0Uid);
            examParam.setLol1Uid(lol1Uid);
            examParam.setLol2Uid(lol2Uid);
            examParam.setLol3Uid(lol3Uid);
            examParam.setGrade(grade);
            examParam.setSubjectId(subjectId);
            examParam.setEventTimestamp(eventTimestamp);
            examParam.setCode(code);
            examParam.setStudyType(studyType);




            MaterialExamNode examNode = materialExamNodeService.getEntranceExam(examParam);
            return ResponseEntity.ok(new ApiDefaultResponse(true, "Ok", examNode));
        } catch (Exception e) {
            log.error("error at getEntranceExam: " + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/exam/final")
    public ResponseEntity<?> getFinalExam(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                          @RequestParam(Parameter.TYPES_NAME) String typesName,
                                          @RequestParam(Parameter.UID) String uid,
                                          @RequestParam(Parameter.CYCLE_ID) String cycleId,
                                          @RequestParam(Parameter.LOL0_UID) String lol0Uid,
                                          @RequestParam(Parameter.LOL1_UID) String lol1Uid,
                                          @RequestParam(value = Parameter.LOL2_UID, required = false) String lol2Uid,
                                          @RequestParam(value = Parameter.LOL3_UID, required = false) String lol3Uid,
                                          @RequestParam(Parameter.EVENT_TIMESTAMP) Long eventTimestamp,
                                          @RequestParam(Parameter.GRADE) Integer grade,
                                          @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                          @RequestParam(Parameter.CODE) Long code,
                                          @RequestParam(value = Parameter.STUDY_TYPE, required = false)  String studyType) {
        try {
            MaterialExamNodeServiceImpl.ExamParam examParam = new MaterialExamNodeServiceImpl.ExamParam();
            examParam.setUserId(userId);
            examParam.setDeviceId(deviceId);
            examParam.setDeviceType(deviceType);
            examParam.setTypesName(typesName);
            examParam.setUid(uid);
            examParam.setCycleId(cycleId);
            examParam.setLol0Uid(lol0Uid);
            examParam.setLol1Uid(lol1Uid);
            examParam.setLol2Uid(lol2Uid);
            examParam.setLol3Uid(lol3Uid);
            examParam.setGrade(grade);
            examParam.setSubjectId(subjectId);
            examParam.setEventTimestamp(eventTimestamp);
            examParam.setCode(code);
            examParam.setStudyType(studyType);

            MaterialExamNode materialExamNode = materialExamNodeService.getFinalExam(examParam);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "Ok", materialExamNode));
        } catch (Exception e) {
            log.error("error at getFinalExam: " + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/exam/entrance/submit")
    public ResponseEntity<?> submitEntranceExam(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                                @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                                @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                                @Valid @RequestBody SubmitExamRequest submitExamRequest){

        SubmitEntranceExamResponse submitPracticeResponse = materialExamNodeService.submitEntranceExam(userId, deviceId, deviceType, submitExamRequest);
        return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitPracticeResponse));
    }

    @PostMapping("/exam/final/submit")
    public ResponseEntity<?> submitFinalExam(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                             @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                             @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                             @Valid @RequestBody SubmitExamRequest submitExamRequest
                                             ){

        try {
            SubmitFinalExamResponse submitFinalExamResponse = materialExamNodeService.submitFinalExam(userId, deviceId, deviceType, submitExamRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitFinalExamResponse));
        }catch (Exception e){
            log.error("Error at submitFinalExam:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }





}
