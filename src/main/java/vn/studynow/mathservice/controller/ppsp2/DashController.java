package vn.studynow.mathservice.controller.ppsp2;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/ppsp2/dashboard")
public class DashController {

    private Map<String, String> mappingUrl = new HashMap<>();


    private String bookServiceHost;

    public DashController(TrackingService trackingService, ApplicationProperties applicationProperties) {
        initMappingUrl();
        bookServiceHost = applicationProperties.getBookService().getHost();
    }


    private void initMappingUrl() {
        mappingUrl.put("/api/v1/ppsp2/dashboard/knowledge-point", "/book/dash/lol1/get-knowledge-point");
        mappingUrl.put("/api/v1/ppsp2/dashboard/level-point", "/book/dash/lol1/get-level-point");
        mappingUrl.put("/api/v1/ppsp2/dashboard/capacity-point", "/book/dash/lol1/get-capacity-point");
        mappingUrl.put("/api/v1/ppsp2/dashboard/hard-point", "/book/dash/lol1/get-hard-point");
    }


    @GetMapping("/knowledge-point")
    public ResponseEntity getKnowledgePoint(@RequestBody(required = false) String body,
                                       HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(bookServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @GetMapping("/level-point")
    public ResponseEntity getLevelPoint(@RequestBody(required = false) String body,
                                       HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        return Forwarder.forwardWithPath(bookServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @GetMapping("/capacity-point")
    public ResponseEntity getCapacityPoint(@RequestBody(required = false) String body,
                                       HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        return Forwarder.forwardWithPath(bookServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @GetMapping("/hard-point")
    public ResponseEntity getHardPoint(@RequestBody(required = false) String body,
                                       HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        return Forwarder.forwardWithPath(bookServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }
}
