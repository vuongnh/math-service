package vn.studynow.mathservice.controller.ppsp2;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/ppsp2/learn")
public class LearnController {


    private Map<String, String> mappingUrl = new HashMap<>();
    private final TrackingService trackingService;
    private String brahmServiceHost;

    public LearnController(TrackingService trackingService,  ApplicationProperties applicationProperties) {
        this.trackingService = trackingService;
        initMappingUrl();
        brahmServiceHost = applicationProperties.getBrahmService().getHost();
    }

    private void initMappingUrl() {
        mappingUrl.put("/api/v1/ppsp2/learn/lol1s", "/lol1s/all");
        mappingUrl.put("/api/v1/ppsp2/learn/quizs", "/practice/questions");
        mappingUrl.put("/api/v1/ppsp2/learn/quizs/submit", "/practice/submit");
        mappingUrl.put("/api/v1/ppsp2/learn/quizs/help", "/practice/help");
    }

    @GetMapping("/lol1s")
    @ApiOperation("Forwarder : http://42.113.207.184:3412/apidocs/#/")
    public ResponseEntity getKnowledgePoint(@RequestBody(required = false) String body,
                                            HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(brahmServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

    @GetMapping("/quizs")
    @ApiOperation("Forwarder : http://42.113.207.184:3412/apidocs/#/")
    public ResponseEntity getQuiz(@RequestBody(required = false) String body,
                                            HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(brahmServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @PostMapping("/quizs/submit")
    @ApiOperation("Forwarder : http://42.113.207.184:3412/apidocs/#/")
    public ResponseEntity submitQuiz(@RequestBody(required = false) String body,
                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(brahmServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

    @GetMapping("/quizs/help")
    @ApiOperation("Forwarder : http://42.113.207.184:3412/apidocs/#/")
    public ResponseEntity getHelpOfQuiz(@RequestBody(required = false) String body,
                                     HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(brahmServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

    @PostMapping("/quizs/help")
    @ApiOperation("Forwarder : http://42.113.207.184:3412/apidocs/#/")
    public ResponseEntity submitHelpOfQuiz (@RequestBody(required = false) String body,
                                            HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        return Forwarder.forwardWithPath(brahmServiceHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


}
