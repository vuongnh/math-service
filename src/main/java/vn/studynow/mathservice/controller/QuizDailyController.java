package vn.studynow.mathservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/quiz")
public class QuizDailyController {
    @Value("${EXAM_SERVICE_HOST:}")
    private String examHost;

    private final TrackingService trackingService;

    private Map<String, String> mappingUrl = new HashMap<>();

    public QuizDailyController(TrackingService trackingService) {
        this.trackingService = trackingService;
        mappingUrl.put("/api/v1/quiz/daily-practice", "/corona/quiz/daily-practice");
        mappingUrl.put("/api/v1/quiz/daily-practice/submit", "/corona/quiz/result-submit");
        mappingUrl.put("/api/v1/quiz/daily-practice/result-history", "/corona/quiz/result-history");
        mappingUrl.put("/api/v1/quiz/daily-practice/video", "/corona/quiz/get-video");
    }


    @RequestMapping("/**")
    public ResponseEntity forward(@RequestBody(required = false) String body,
                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        trackingService.sendMessageRequestWithoutException(request, method, body);

        return Forwarder.forwardWithPath(examHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

}
