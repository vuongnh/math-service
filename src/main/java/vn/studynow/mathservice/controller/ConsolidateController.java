package vn.studynow.mathservice.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/consolidate")
public class ConsolidateController {

    @Value("${EXAM_SERVICE_HOST:}")
    private String examHost;

    private final TrackingService trackingService;

    private Map<String, String> mappingUrl = new HashMap<>();

    public ConsolidateController(TrackingService trackingService) {
        this.trackingService = trackingService;
        mappingUrl.put("/api/v1/consolidate/questions", "/consolidate/questions");
        mappingUrl.put("/api/v1/consolidate/questions/submit", "/consolidate/questions/submit");
    }


    @GetMapping("/questions")
    public ResponseEntity getQuestions(@RequestBody(required = false) String body,
                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        trackingService.sendMessageRequestWithoutException(request, method, body);

        return Forwarder.forwardWithPath(examHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @PostMapping("/questions/submit")
    public ResponseEntity submitQuestions(@RequestBody(required = false) String body,
                                                      HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        trackingService.sendMessageRequestWithoutException(request, method, body);


        return Forwarder.forwardWithPath(examHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


//    @RequestMapping("/**")
    public ResponseEntity forward(@RequestBody(required = false) String body,
                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        return Forwarder.forwardWithPath(examHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }
}
