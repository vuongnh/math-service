package vn.studynow.mathservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.RequestInfoExtractor;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.domain.learning.entities.BookmarkedLOL4;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.model.MaterialNode;
import vn.studynow.mathservice.payload.request.SubmitPractice4Request;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.payload.response.ApiSuccessfulResponse;
import vn.studynow.mathservice.payload.response.SubmitPractice4Response;
import vn.studynow.mathservice.service.KnowledgeLv4NodeService;
import vn.studynow.mathservice.service.MaterialNodeService;
import vn.studynow.mathservice.service.MaterialPracticeNodeService;
import vn.studynow.mathservice.service.restrequest.RestRequest;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/v2/thematic/lol4")
public class LOL4Controller {

    private final Logger log = LoggerFactory.getLogger(LOL4Controller.class);

    @Autowired
    private KnowledgeLv4NodeService knowledgeLv4NodeService;
    @Autowired
    private TrackingService trackingService;
    @Autowired
    MaterialPracticeNodeService materialPracticeNodeService;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    MaterialNodeService materialNodeService;

    @Autowired
    RestRequest restRequest;

    private Map<String, String> mappingUrl = new HashMap<>();

    public LOL4Controller() {
        mappingUrl.put("/api/v2/thematic/lol4/practice/submit", "/v2/cycles/practice/lol4/motivation");
        mappingUrl.put("/api/v2/thematic/lol4/bookmark/quiz", "/cycles/practice/lol4/bookmark");
        mappingUrl.put("/api/v2/thematic/lol4/qanda/send-question", "/topic/send-question");
        mappingUrl.put("/api/v2/thematic/lol4/qanda/notify", "/topic/show-ques-ans/notify");
        mappingUrl.put("/api/v2/thematic/lol4/qanda/quiz", "/topic/show-ques-ans/quiz");
    }

    @GetMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_4_DESC)
    })
    public ResponseEntity<?> getLearningPath(
            @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
            @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
            @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
            @ApiParam(defaultValue = "Cycle.Two")  @RequestParam(Parameter.TYPES_NAME) String typesName,
            @ApiParam(defaultValue = "44f111bf-c6ec-446d-b34c-75aac9d60be2")  @RequestParam(Parameter.UID) String uid,
            @ApiParam(defaultValue = "0") @RequestParam(Parameter.IS_RELEARN) int isRelearn) {
        try {
            GetLearningPathRequest getLearningPathRequest = GetLearningPathRequest.builder()
                    .userId(userId)
                    .deviceId(deviceId)
                    .deviceType(deviceType)
                    .typesName(typesName)
                    .isRelearn(isRelearn)
                    .uid(uid)
                    .build();
            Object  learningPathResponse = knowledgeLv4NodeService.getLearningPath(getLearningPathRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", learningPathResponse));
        } catch (Exception e) {
            log.error("Error at getLearningPath:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @PostMapping("/practice/submit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitPractice(@RequestBody(required = false) String body,
                                            HttpMethod method, HttpServletRequest request, HttpServletResponse response
                                            ) throws URISyntaxException {

        trackingService.sendMessageRequestWithoutException(request, method, body);

        return Forwarder.forwardWithPath(applicationProperties.getLearningService().getHost(),
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }




    @GetMapping("/materials")
    public ResponseEntity<?> getMaterialLOL4(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                          @ApiParam(defaultValue = "Cycle.Four")  @RequestParam(Parameter.TYPES_NAME) String typesName,
                                          @ApiParam(defaultValue = "aa9a9b48-14af-4689-8887-a6627dd7427c") @RequestParam(Parameter.UID) String uid){

        try {
            List<MaterialNode> materialNodes = materialNodeService.getLearningNodeLevel4Materials(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", materialNodes));

        } catch (Exception e) {
            log.error("Error at getMaterials:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bookmark")
    public ResponseEntity<?> getBookmarked( @RequestParam("user_id") String userId,
                                            @RequestParam(value = "size", defaultValue = "20", required = false) int size,
                                            @RequestParam(value = "page", defaultValue = "0", required = false) int page  ){
        Page<BookmarkedLOL4> bookmarkedLOL4s = knowledgeLv4NodeService.getBookmarkedLOL4s(userId, PageRequest.of(page, size, Sort.by("createdAt").descending()));
        return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", bookmarkedLOL4s));
    }

    @PostMapping("/bookmark")
    public ResponseEntity<?> bookmark(@RequestBody BookmarkedLOL4 bookmarkedLOL4){
        knowledgeLv4NodeService.bookmarkLOL4(bookmarkedLOL4);
        return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", null));
    }




    // http://42.113.207.184:1235/apidocs/#/bookmark_lol4/get_cycles_practice_lol4_bookmark
    @GetMapping("/bookmark/quiz")
    @ApiOperation("Forwarder : http://42.113.207.184:1235/apidocs/#/bookmark_lol4/get_cycles_practice_lol4_bookmark")
    public ResponseEntity<?> getQuizOfBookmarked( @RequestBody(required = false) String body,
                                            HttpMethod method, HttpServletRequest request, HttpServletResponse response ) throws URISyntaxException {
        return Forwarder.forwardWithPath(applicationProperties.getLearningService().getHost(),
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }


    @PostMapping("/bookmark/quiz")
    @ApiOperation("Forwarder : http://42.113.207.184:1235/apidocs/#/bookmark_lol4/get_cycles_practice_lol4_bookmark")
    public ResponseEntity<?> getMotivationOfBookmarked( @RequestBody(required = false) String body,
                                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response ) throws URISyntaxException {
        return Forwarder.forwardWithPath(applicationProperties.getLearningService().getHost(),
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

    @PostMapping("/qanda/send-question")
    @ApiOperation("Forwarder : http://dev.math.giaingay.io:8701/swagger-ui.html#/topic-controller/showQuesAnsQuizUsingGET")
    public ApiSuccessfulResponse sendQuestion( @RequestBody(required = false) String body,
                                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response ) throws URISyntaxException, JsonProcessingException {
//        return Forwarder.forwardWithPath(applicationProperties.getQandaService().getHost(),
//                mappingUrl.getOrDefault(request.getRequestURI(), ""),
//                method, request, body);

        RequestInfoExtractor requestInfoExtractor = new RequestInfoExtractor();
        requestInfoExtractor.extract(request, method, body);

        Object data =  (restRequest.postAndGetData(applicationProperties.getQandaService().getHost() +
                        mappingUrl.getOrDefault(request.getRequestURI(), ""),
                requestInfoExtractor.getData()
        ));

        return new ApiSuccessfulResponse(data);
    }

    @GetMapping("/qanda/notify")
    @ApiOperation("Forwarder : http://dev.math.giaingay.io:8701/swagger-ui.html#/topic-controller/showQuesAnsQuizUsingGET")
    public ApiSuccessfulResponse showQuestionNotify(  String quiz_id,
                                                  String user_id,
                                                  String topic_type,
                                           HttpMethod method, HttpServletRequest request, HttpServletResponse response ) throws URISyntaxException, JsonProcessingException {
//        return Forwarder.forwardWithPath(applicationProperties.getQandaService().getHost(),
//                mappingUrl.getOrDefault(request.getRequestURI(), ""),
//                method, request, "");


        RequestInfoExtractor requestInfoExtractor = new RequestInfoExtractor();
        requestInfoExtractor.extract(request, method, null);





        Object data =  (restRequest.getData(applicationProperties.getQandaService().getHost() +
                        mappingUrl.getOrDefault(request.getRequestURI(), ""),
                requestInfoExtractor.getData()
        ));


        return new ApiSuccessfulResponse(data);


    }


    @GetMapping("/qanda/quiz")
    @ApiOperation("Forwarder : http://dev.math.giaingay.io:8701/swagger-ui.html#/topic-controller/showQuesAnsQuizUsingGET")
    public ApiSuccessfulResponse showQuestionQuiz(
                                                 String quiz_id,
                                                 String user_id,
                                                 String topic_type,
                                                 HttpMethod method, HttpServletRequest request, HttpServletResponse response ) throws URISyntaxException {
//        log.info(applicationProperties.getQandaService().getHost());
//        ResponseEntity responseEntity =  Forwarder.forwardWithPath(applicationProperties.getQandaService().getHost(),
//                mappingUrl.getOrDefault(request.getRequestURI(), ""),
//                method, request, "");
//        log.info(responseEntity.toString());
//        return responseEntity;



        Object data =  (restRequest.getData(applicationProperties.getQandaService().getHost() +
                        mappingUrl.getOrDefault(request.getRequestURI(), ""),
                ImmutableMap.of(
                        "quiz_id", quiz_id,
                        "user_id", user_id,
                        "topic_type", topic_type
                )
        ));


        return new ApiSuccessfulResponse(data);

    }

}
