package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.KnowledgeLevel6Node;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.KnowledgeLv6NodeService;

import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class KnowledgeLv6NodeController {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv6NodeController.class);

    @Autowired
    private KnowledgeLv6NodeService knowledgeLv6NodeService;

    @GetMapping("/get-learning-nodes-level-6")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_6_DESC)
    })
    public ResponseEntity<?> getLearningNodesLevel6(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                    @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                    @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                    @RequestParam(Parameter.TYPES_NAME) String typesName,
                                                    @RequestParam(Parameter.UID) String uid){
        try {
            List<KnowledgeLevel6Node> learningNodes = knowledgeLv6NodeService.getKnowledgeLevel6Nodes(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", learningNodes));
        } catch (Exception e) {
            log.error("Error at getLearningNodesLevel6:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
