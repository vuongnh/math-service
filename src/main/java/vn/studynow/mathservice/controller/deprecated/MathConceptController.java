package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.payload.request.ViewConceptsRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.MathConceptService;
import vn.studynow.mathservice.service.restrequest.RestRequest;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/mathconcepts")
public class MathConceptController {

    private final Logger log = LoggerFactory.getLogger(MathConceptController.class);

    @Autowired
    MathConceptService mathConceptService;

    @Autowired
    RestRequest restRequest;

    @Autowired
    ApplicationProperties applicationProperties;

    @GetMapping("/search")
    public ResponseEntity<?> getMathConceptRecommend(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                     @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                     @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                     @RequestParam(Parameter.EVENT_NAME) String eventName,
                                                     @RequestParam(Parameter.SEARCH_NAME) String searchName,
                                                     @RequestParam(Parameter.GRADE) Integer grade,
                                                     @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                                     @RequestParam(Parameter.CODE) String code) {

        Map<String, Object> params = new HashMap<>();
        params.put(Parameter.REQUEST_PARAM_USER_ID, userId);
        params.put(Parameter.REQUEST_PARAM_DEVICE_ID, deviceId);
        params.put(Parameter.REQUEST_PARAM_DEVICE_TYPE, deviceType);
        params.put(Parameter.SEARCH_NAME, searchName);
        params.put(Parameter.GRADE, grade);
        params.put(Parameter.SUBJECT_ID, subjectId);
        params.put(Parameter.CODE, code);
        params.put(Parameter.EVENT_NAME, eventName);

//        Object mathConcepts = restRequest.get(applicationProperties.getSearchService().getApiSuggestConcept(), params, Object.class);
        Object mathConcepts = restRequest.getData(applicationProperties.getSearchService().getApiSuggestConcept(), params);

        return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", mathConcepts));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findMathConceptById(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                 @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                 @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                 @PathVariable(Parameter.ID) String id,
//                                                 @RequestParam(Parameter.ID) String rqId,
//                                                 @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String rqUserId,
//                                                 @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String rqDeviceId,
                                                 @RequestParam(Parameter.EVENT_NAME) String eventName,
                                                 @RequestParam(Parameter.GRADE) Integer grade,
                                                 @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                                 @RequestParam(Parameter.CODE) String code) {

        Map<String, Object> params = new HashMap<>();
        params.put(Parameter.ID, id);
        params.put(Parameter.REQUEST_PARAM_USER_ID, userId);
        params.put(Parameter.REQUEST_PARAM_DEVICE_ID, deviceId);
        params.put(Parameter.REQUEST_PARAM_DEVICE_TYPE, deviceType);
        params.put(Parameter.GRADE, grade);
        params.put(Parameter.SUBJECT_ID, subjectId);
        params.put(Parameter.CODE, code);
        params.put(Parameter.EVENT_NAME, eventName);

//        Object concepts = restRequest.get(applicationProperties.getSearchService().getApiGetConcept(), params, Object.class);
        Object concepts = restRequest.getData(applicationProperties.getSearchService().getApiGetConcept(), params);

        return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", concepts));
    }

    @PostMapping("/view-concepts")
    public ResponseEntity<?> viewConcepts(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                          @RequestBody ViewConceptsRequest request) {

        request.setDeviceId(deviceId);
        request.setUserId(userId);

        Object concepts = restRequest.postAndGetData(applicationProperties.getSearchService().getApiViewConcept(), request);

        return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", concepts));
    }
}
