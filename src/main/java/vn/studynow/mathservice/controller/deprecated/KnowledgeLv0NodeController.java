package vn.studynow.mathservice.controller.deprecated;

import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.domain.notify.NotificationService;
import vn.studynow.mathservice.domain.notify.SubscribeLO2Request;
import vn.studynow.mathservice.domain.notify.SubscribeNotificationRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.payload.response.ApiSuccessfulResponse;
import vn.studynow.mathservice.service.KnowledgeLv0NodeService;
import vn.studynow.mathservice.service.restrequest.RestRequest;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/v1")
public class KnowledgeLv0NodeController {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv0NodeController.class);

    @Autowired
    private KnowledgeLv0NodeService knowledgeLv0NodeService;

    @Autowired
    RestRequest forwarder;

    @Autowired
    ApplicationProperties applicationProperties;


    @Autowired
    NotificationService notificationService;

    private Map<String, String> mappingUrl = new HashMap<>();

    public KnowledgeLv0NodeController() {
        mappingUrl.put("/api/v1/lol2/detail", "/cycles/lol2/detail");
    }


    @GetMapping(value = "/get-learning-cycle-level-2")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_2_DESC)
    })
    public ApiDefaultResponse  getKnowledgeLv0Nodes(
            @ApiParam(defaultValue = "test")  @RequestHeader(Parameter.USER_ID) String userId,
            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
            @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType) {

        Object data =  ((Map)forwarder.getData(applicationProperties.getLearningService().getApiGetLearningNodesLevel0(),
                ImmutableMap.of(
                        ServiceConstant.LearningService.Request.PARAMETER_USER_ID, userId,
                        ServiceConstant.LearningService.Request.PARAMETER_DEVICE_ID, deviceId,
                        ServiceConstant.LearningService.Request.PARAMETER_DEVICE_TYPE , deviceType
                )
                )).get("cycle");


//        Object level0Nodes = knowledgeLv0NodeService.getKnowledgeLevel0Nodes(userId, deviceId, deviceType);

        return new ApiSuccessfulResponse(data);
    }



    @GetMapping(value = "/lol2/detail")
    public ResponseEntity<String>  getLol2Detail
            (
                                                     @RequestParam String uid,
                                                     @RequestParam String types_name,
                                                     @RequestParam String user_id,
                                                     @RequestParam String device_id,
                                                     @RequestParam(required = false) String grade,
                                                     @RequestParam(required = false) String subject_id,
                                                     @RequestParam Long event_timestamp,
             HttpServletRequest request,
             HttpServletResponse response) throws Exception {

        return Forwarder.forwardWithPath(
                applicationProperties.getLearningService().getHost(),
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                HttpMethod.GET, request, "");


    }


    @PostMapping(value = "/lol2/subscribe-notification")
    public ResponseEntity<?> subscribeLOL2Notification(@Valid @RequestBody SubscribeLO2Request subscribeLO2Request) {

        SubscribeNotificationRequest subscribeNotificationRequest = new SubscribeNotificationRequest();
        subscribeNotificationRequest.setUser_id(subscribeLO2Request.getUser_id());
        subscribeNotificationRequest.setTopic_id(subscribeLO2Request.getTypes_name());
        subscribeNotificationRequest.setTopic_id(subscribeLO2Request.getUid());
        subscribeNotificationRequest.setSubscribe(subscribeLO2Request.getSubscribe());

        notificationService.subscribeComingSoonObject(subscribeNotificationRequest);
        return ResponseEntity.ok(new ApiSuccessfulResponse());

    }

}
