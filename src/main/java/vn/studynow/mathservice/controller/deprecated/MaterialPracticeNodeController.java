package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.payload.request.SubmitPractice4Request;
import vn.studynow.mathservice.payload.request.SubmitPractice6Request;
import vn.studynow.mathservice.payload.response.*;
import vn.studynow.mathservice.service.MaterialPracticeNodeService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/")
public class MaterialPracticeNodeController {

    private final Logger log = LoggerFactory.getLogger(MaterialPracticeNodeController.class);

    @Autowired
    MaterialPracticeNodeService materialPracticeNodeService;

    @PostMapping("/submit-practice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitPractice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                            @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                            @Valid @RequestBody SubmitPractice4Request submitPractice4Request) {

        try {
            SubmitPractice4Response submitPractice4Response = materialPracticeNodeService.submitPractice4(userId, deviceId, deviceType, submitPractice4Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", submitPractice4Response));
        } catch (Exception e) {
            log.error("Error at submitPractice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/submit-learning-node-level-61-practice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel61Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @RequestBody SubmitPractice6Request submitPractice6Request){

        try {
            SubmitPractice61Response submitPractice61Response = materialPracticeNodeService.submitLearningNodeLevel61Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitPractice61Response));
        }catch (Exception e){
            log.error("Error at submitLearningNodeLevel61Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/submit-learning-node-level-62-practice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel62Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @Valid @RequestBody SubmitPractice6Request submitPractice6Request) {

        try {
            SubmitPractice62Response submitPractice62Response = materialPracticeNodeService.submitLearningNodeLevel62Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", submitPractice62Response));
        } catch (Exception e) {
            log.error("Error at submitLearningNodeLevel62Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/submit-learning-node-level-63-practice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel63Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @Valid @RequestBody SubmitPractice6Request submitPractice6Request) {

        try {
            SubmitPractice63Response submitPractice63Response = materialPracticeNodeService.submitLearningNodeLevel63Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", submitPractice63Response));
        } catch (Exception e) {
            log.error("Error at submitLearningNodeLevel63Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
