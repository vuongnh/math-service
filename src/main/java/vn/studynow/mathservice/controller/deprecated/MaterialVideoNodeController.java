package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.payload.request.VideoUserSubmit;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class MaterialVideoNodeController {
    private final Logger log = LoggerFactory.getLogger(MaterialVideoNodeController.class);

    @PostMapping("/submit-video")
    public ResponseEntity submitVideo(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                      @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                      @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                      @Valid @RequestBody VideoUserSubmit videoUserSubmit){
        try {
            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", userId));
        }catch (Exception e){
            log.error("Error at submitVideo:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
