package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.MaterialEntranceExamMotivationPopupNode;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.MaterialMotivationPopupNodeService;


@RestController
@RequestMapping("/api/v1")
public class MaterialMotivationPopupNodeController {

    private final Logger log = LoggerFactory.getLogger(MaterialMotivationPopupNodeController.class);

    @Autowired
    private MaterialMotivationPopupNodeService materialMotivationPopupNodeService;

    @GetMapping(value = "/get-entrance-exam-popup")
    public ResponseEntity<?> getEntranceExamPopup(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                  @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                  @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                  @RequestParam(Parameter.TYPES_NAME) String typesName,
                                                  @RequestParam(Parameter.UID) String uid) {
        try {
            MaterialEntranceExamMotivationPopupNode materialEntranceExamMotivationPopupNode = materialMotivationPopupNodeService.getMaterialEntranceExamMotivationPopup(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", materialEntranceExamMotivationPopupNode));
        }catch (Exception e){
            log.error("Error at getEntranceExamPopup:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
