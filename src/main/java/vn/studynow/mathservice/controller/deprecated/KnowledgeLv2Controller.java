package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.KnowledgeLevel2Node;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.KnowledgeLv2NodeService;

@RestController
@RequestMapping("/api/v1")
public class KnowledgeLv2Controller {
    private final Logger log = LoggerFactory.getLogger(KnowledgeLv2Controller.class);

    @Autowired
    private KnowledgeLv2NodeService knowledgeLv2NodeService;

    @GetMapping(value = "/get-recommend-learning-node-level-2")
    public ResponseEntity<?> getRecommendLearningNodeLevel2(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                            @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType) {
        try {
            KnowledgeLevel2Node recommendNode = knowledgeLv2NodeService.getRecommendLearningNodeLevel2(userId, deviceId, deviceType);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", recommendNode));
        } catch (Exception e) {
            log.error("Error at getRecommendLearningNodeLevel2:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


}
