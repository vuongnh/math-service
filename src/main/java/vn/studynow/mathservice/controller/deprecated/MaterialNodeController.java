package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.MaterialNode;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.MaterialNodeService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class MaterialNodeController {

    private final Logger log = LoggerFactory.getLogger(MaterialNodeController.class);

    @Autowired
    MaterialNodeService materialNodeService;

    @GetMapping("/get-materials")
    public ResponseEntity<?> getMaterials(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                          @ApiParam(defaultValue = "Cycle.Four")  @RequestParam(Parameter.TYPES_NAME) String typesName,
                                          @ApiParam(defaultValue = "aa9a9b48-14af-4689-8887-a6627dd7427c") @RequestParam(Parameter.UID) String uid){

        try {
            List<MaterialNode> materialNodes = materialNodeService.getLearningNodeLevel4Materials(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", materialNodes));

        } catch (Exception e) {
            log.error("Error at getMaterials:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/get-learning-node-level-6-materials")
    public ResponseEntity<?> getLearningNodeLevel6Materials(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                             @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                             @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                             @RequestParam(Parameter.TYPES_NAME) String typesName,
                                                             @RequestParam(Parameter.UID) String uid){

        try {
            List<MaterialNode> materialNodes = materialNodeService.getLearningNodeLevel6Materials(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", materialNodes));

        } catch (Exception e) {
            log.error("Error at getLearningNodeLevel6Materials:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
