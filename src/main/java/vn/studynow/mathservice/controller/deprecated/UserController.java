package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.UserAvatar;
import vn.studynow.mathservice.model.profile.User;
import vn.studynow.mathservice.payload.request.profile.CreateUserRequest;
import vn.studynow.mathservice.payload.request.profile.UpdateUserRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.UserService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(value = "/users")
    public ResponseEntity<?> createUser(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                        @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                        @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                        @Valid @RequestBody CreateUserRequest createUserRequest) {
        try {
            User user = userService.createUser(userId, createUserRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", user));
        }catch (Exception e){
            log.error("Error at createUser:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/users")
    public ResponseEntity<?> updateUser(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                        @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                        @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                        @RequestBody UpdateUserRequest updateUserRequest) {
        try {
            User user = userService.updateUser(userId, updateUserRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", user));
        }catch (Exception e){
            log.error("Error at updateUser:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/users")
    public ResponseEntity<?> deleteUser(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId) {
        try {
            userService.deleteUser(userId);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", null));
        }catch (Exception e){
            log.error("Error at deleteUser:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/users")
    public ResponseEntity<?> getUser(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                        @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                        @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType) {
        try {
            User user = userService.getUser(userId);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", user));
        }catch (Exception e){
            log.error("Error at getUser:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/users/avatars")
    public ResponseEntity<?> getUserAvatars(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                            @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType) {
        try {
            List<UserAvatar> userAvatars = userService.getUserAvatar(userId);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", userAvatars));
        }catch (Exception e){
            log.error("Error at getUser:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
