package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.MaterialExamNode;
import vn.studynow.mathservice.payload.request.SubmitExamRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.payload.response.SubmitEntranceExamResponse;
import vn.studynow.mathservice.payload.response.SubmitFinalExamResponse;
import vn.studynow.mathservice.service.MaterialExamNodeService;
import vn.studynow.mathservice.service.impl.MaterialExamNodeServiceImpl;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class MaterialExamNodeController {

    private final Logger log = LoggerFactory.getLogger(MaterialExamNodeController.class);

    @Autowired
    MaterialExamNodeService materialExamNodeService;

    @GetMapping("/get-entrance-exam")
    public ResponseEntity<?> getEntranceExam(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                             @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                             @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                             @RequestParam(Parameter.TYPES_NAME) String typesName,
                                             @RequestParam(Parameter.UID) String uid,
                                             @RequestParam(Parameter.CYCLE_ID) String cycleId,
                                             @RequestParam(Parameter.LOL0_UID) String lol0Uid,
                                             @RequestParam(Parameter.LOL1_UID) String lol1Uid,
                                             @RequestParam(value = Parameter.LOL2_UID, required = false) String lol2Uid,
                                             @RequestParam(value = Parameter.LOL3_UID, required = false) String lol3Uid,
                                             @RequestParam(Parameter.GRADE) Integer grade,
                                             @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                             @RequestParam(Parameter.EVENT_TIMESTAMP) Long eventTimestamp,
                                             @RequestParam(Parameter.CODE) Long code,
                                             @RequestParam(value = Parameter.STUDY_TYPE, required = false)  String studyType) {
        try {
            MaterialExamNodeServiceImpl.ExamParam examParam = new MaterialExamNodeServiceImpl.ExamParam();
            examParam.setUserId(userId);
            examParam.setDeviceId(deviceId);
            examParam.setDeviceType(deviceType);
            examParam.setTypesName(typesName);
            examParam.setUid(uid);
            examParam.setCycleId(cycleId);
            examParam.setLol0Uid(lol0Uid);
            examParam.setLol1Uid(lol1Uid);
            examParam.setLol2Uid(lol2Uid);
            examParam.setLol3Uid(lol3Uid);
            examParam.setGrade(grade);
            examParam.setSubjectId(subjectId);
            examParam.setEventTimestamp(eventTimestamp);
            examParam.setCode(code);
            examParam.setStudyType(studyType);




            MaterialExamNode examNode = materialExamNodeService.getEntranceExam(examParam);
            return ResponseEntity.ok(new ApiDefaultResponse(true, "Ok", examNode));
        } catch (Exception e) {
            log.error("error at getEntranceExam: " + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-final-exam")
    public ResponseEntity<?> getFinalExam(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                          @RequestParam(Parameter.TYPES_NAME) String typesName,
                                          @RequestParam(Parameter.UID) String uid,
                                          @RequestParam(Parameter.CYCLE_ID) String cycleId,
                                          @RequestParam(Parameter.LOL0_UID) String lol0Uid,
                                          @RequestParam(Parameter.LOL1_UID) String lol1Uid,
                                          @RequestParam(value = Parameter.LOL2_UID, required = false) String lol2Uid,
                                          @RequestParam(value = Parameter.LOL3_UID, required = false) String lol3Uid,
                                          @RequestParam(Parameter.EVENT_TIMESTAMP) Long eventTimestamp,
                                          @RequestParam(Parameter.GRADE) Integer grade,
                                          @RequestParam(Parameter.SUBJECT_ID) Integer subjectId,
                                          @RequestParam(Parameter.CODE) Long code,
                                          @RequestParam(value = Parameter.STUDY_TYPE, required = false)  String studyType) {
        try {
            MaterialExamNodeServiceImpl.ExamParam examParam = new MaterialExamNodeServiceImpl.ExamParam();
            examParam.setUserId(userId);
            examParam.setDeviceId(deviceId);
            examParam.setDeviceType(deviceType);
            examParam.setTypesName(typesName);
            examParam.setUid(uid);
            examParam.setCycleId(cycleId);
            examParam.setLol0Uid(lol0Uid);
            examParam.setLol1Uid(lol1Uid);
            examParam.setLol2Uid(lol2Uid);
            examParam.setLol3Uid(lol3Uid);
            examParam.setGrade(grade);
            examParam.setSubjectId(subjectId);
            examParam.setEventTimestamp(eventTimestamp);
            examParam.setCode(code);
            examParam.setStudyType(studyType);

            MaterialExamNode materialExamNode = materialExamNodeService.getFinalExam(examParam);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "Ok", materialExamNode));
        } catch (Exception e) {
            log.error("error at getFinalExam: " + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/submit-entrance-exam")
    public ResponseEntity<?> submitEntranceExam(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                @Valid @RequestBody SubmitExamRequest submitExamRequest){


        SubmitEntranceExamResponse submitPracticeResponse = materialExamNodeService.submitEntranceExam(userId, deviceId, deviceType, submitExamRequest);
        return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitPracticeResponse));

    }

    @PostMapping("/submit-final-exam")
    public ResponseEntity<?> submitFinalExam(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                             @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                             @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                             @Valid @RequestBody SubmitExamRequest submitExamRequest){

        try {
            SubmitFinalExamResponse submitFinalExamResponse = materialExamNodeService.submitFinalExam(userId, deviceId, deviceType, submitExamRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitFinalExamResponse));
        }catch (Exception e){
            log.error("Error at submitFinalExam:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
