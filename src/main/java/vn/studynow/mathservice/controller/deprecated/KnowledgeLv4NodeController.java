package vn.studynow.mathservice.controller.deprecated;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.payload.response.ApiDefaultResponse;
import vn.studynow.mathservice.service.KnowledgeLv4NodeService;


@RestController
@RequestMapping("/api/v1")
public class KnowledgeLv4NodeController {

    private final Logger log = LoggerFactory.getLogger(KnowledgeLv4NodeController.class);

    @Autowired
    private KnowledgeLv4NodeService knowledgeLv4NodeService;

    @GetMapping("/get-learning-path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_4_DESC)
    })
    public ResponseEntity<?> getLearningPath(
                                            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                            @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                            @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                            @ApiParam(defaultValue = "Cycle.Two")  @RequestParam(Parameter.TYPES_NAME) String typesName,
                                            @ApiParam(defaultValue = "44f111bf-c6ec-446d-b34c-75aac9d60be2")  @RequestParam(Parameter.UID) String uid,
                                            @ApiParam(defaultValue = "0") @RequestParam(Parameter.IS_RELEARN) int isRelearn) {
        try {
            GetLearningPathRequest getLearningPathRequest = GetLearningPathRequest.builder()
                    .userId(userId)
                    .deviceId(deviceId)
                    .deviceType(deviceType)
                    .typesName(typesName)
                    .isRelearn(isRelearn)
                    .uid(uid)
                    .build();
            Object  learningPathResponse = knowledgeLv4NodeService.getLearningPath(getLearningPathRequest);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", learningPathResponse));
        } catch (Exception e) {
            log.error("Error at getLearningPath:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
