package vn.studynow.mathservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.studynow.mathservice.domain.tracking.services.TrackingService;
import vn.studynow.mathservice.util.Forwarder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/exam")
public class ExamController {

    private final Logger log = LoggerFactory.getLogger(ExamController.class);

    private final TrackingService trackingService;

    @Value("${EXAM_SERVICE_HOST:}")
    private String examHost;

    private Map<String, String> mappingUrl = new HashMap<>();

    /**
     * Corona Swagger : http://171.244.140.52:1265/apidocs/#/
     * @param trackingService
     */
    public ExamController(TrackingService trackingService) {
        this.trackingService = trackingService;
        mappingUrl.put("/api/v1/exam/lols", "/corona/exam/get-knowledge");
        mappingUrl.put("/api/v1/exam", "/corona/exam/get-exam");
        mappingUrl.put("/api/v1/exam/submit", "/corona/exam/submit-result");
        mappingUrl.put("/api/v1/exam/mock", "/corona/exam/get-original-exam");
    }

    @RequestMapping("/**")
    public ResponseEntity forward(@RequestBody(required = false) String body,
                                  HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        trackingService.sendMessageRequestWithoutException(request, method, body);

        return Forwarder.forwardWithPath(examHost,
                mappingUrl.getOrDefault(request.getRequestURI(), ""),
                method, request, body);
    }

}
