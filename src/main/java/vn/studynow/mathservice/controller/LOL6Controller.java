package vn.studynow.mathservice.controller;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.common.constant.ApiDescriptionConstant;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.model.KnowledgeLevel6Node;
import vn.studynow.mathservice.model.MaterialNode;
import vn.studynow.mathservice.payload.request.SubmitPractice6Request;
import vn.studynow.mathservice.payload.response.*;
import vn.studynow.mathservice.service.KnowledgeLv6NodeService;
import vn.studynow.mathservice.service.MaterialNodeService;
import vn.studynow.mathservice.service.MaterialPracticeNodeService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/v2/thematic/lol6")
public class LOL6Controller {

    private final Logger log = LoggerFactory.getLogger(LOL6Controller.class);

    @Autowired
    private KnowledgeLv6NodeService knowledgeLv6NodeService;
    @Autowired
    MaterialPracticeNodeService materialPracticeNodeService;
    @Autowired
    MaterialNodeService materialNodeService;

    @GetMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_LEARNING_CYCLE_LEVEL_6_DESC)
    })
    public ResponseEntity<?> getLearningNodesLevel6(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                                    @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                                    @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                                    @RequestParam(Parameter.TYPES_NAME) String typesName,
                                                    @RequestParam(Parameter.UID) String uid){
        try {
            List<KnowledgeLevel6Node> learningNodes = knowledgeLv6NodeService.getKnowledgeLevel6Nodes(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", learningNodes));
        } catch (Exception e) {
            log.error("Error at getLearningNodesLevel6:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    @GetMapping("/materials")
    public ResponseEntity<?> getMaterials(@ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_USER_ID) String userId,
                                          @ApiParam(defaultValue = "test") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_ID) String deviceId,
                                          @ApiParam(defaultValue = "unknown") @RequestParam(Parameter.REQUEST_PARAM_DEVICE_TYPE) String deviceType,
                                          @ApiParam(defaultValue = "Cycle.Four")  @RequestParam(Parameter.TYPES_NAME) String typesName,
                                          @ApiParam(defaultValue = "aa9a9b48-14af-4689-8887-a6627dd7427c") @RequestParam(Parameter.UID) String uid){

        try {
            List<MaterialNode> materialNodes = materialNodeService.getLearningNodeLevel4Materials(userId, deviceId, deviceType, typesName, uid);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", materialNodes));

        } catch (Exception e) {
            log.error("Error at getMaterials:" + e.getMessage());

            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/lol61/practice/submit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel61Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @RequestBody SubmitPractice6Request submitPractice6Request){

        try {
            SubmitPractice61Response submitPractice61Response = materialPracticeNodeService.submitLearningNodeLevel61Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true,"ok", submitPractice61Response));
        }catch (Exception e){
            log.error("Error at submitLearningNodeLevel61Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/lol62/practice/submit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel62Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @Valid @RequestBody SubmitPractice6Request submitPractice6Request) {

        try {
            SubmitPractice62Response submitPractice62Response = materialPracticeNodeService.submitLearningNodeLevel62Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", submitPractice62Response));
        } catch (Exception e) {
            log.error("Error at submitLearningNodeLevel62Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/lol63/practice/submit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ApiDescriptionConstant.GET_PRACTICE_MOTIVATION_DESC)
    })
    public ResponseEntity<?> submitLearningNodeLevel63Practice(@ApiParam(defaultValue = "test") @RequestHeader(Parameter.USER_ID) String userId,
                                                               @ApiParam(defaultValue = "test") @RequestHeader(Parameter.DEVICE_ID) String deviceId,
                                                               @ApiParam(defaultValue = "unknown") @RequestHeader(Parameter.DEVICE_TYPE) String deviceType,
                                                               @Valid @RequestBody SubmitPractice6Request submitPractice6Request) {

        try {
            SubmitPractice63Response submitPractice63Response = materialPracticeNodeService.submitLearningNodeLevel63Practice(userId, deviceId, deviceType, submitPractice6Request);

            return ResponseEntity.ok(new ApiDefaultResponse(true, "ok", submitPractice63Response));
        } catch (Exception e) {
            log.error("Error at submitLearningNodeLevel63Practice:" + e.getMessage());
            return new ResponseEntity<>(new ApiDefaultResponse(false, e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
