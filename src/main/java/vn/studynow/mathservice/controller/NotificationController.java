package vn.studynow.mathservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.studynow.mathservice.domain.notify.NotificationService;
import vn.studynow.mathservice.domain.notify.SubscribeLO2Request;
import vn.studynow.mathservice.domain.notify.SubscribeNotificationRequest;
import vn.studynow.mathservice.payload.response.ApiSuccessfulResponse;


@RestController
@RequestMapping("/api/notification/v1")
public class NotificationController {

    private final Logger log = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    NotificationService notificationService;

    @PostMapping(value = "/subscribe-notification")
    public ResponseEntity<?> subscribe(SubscribeNotificationRequest subscribeLO2Request) {
        notificationService.subscribeComingSoonObject(subscribeLO2Request);
        return ResponseEntity.ok(new ApiSuccessfulResponse());
    }
}
