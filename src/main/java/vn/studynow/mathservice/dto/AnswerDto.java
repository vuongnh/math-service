package vn.studynow.mathservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnswerDto {
    private String uid;
    private String text;
    private Integer status;
}
