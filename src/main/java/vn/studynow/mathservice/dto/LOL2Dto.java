package vn.studynow.mathservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LOL2Dto extends BaseDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty("icon_url")
    private String iconUrl;

    @JsonProperty("status")
    private Integer passStatus;

    @Override
    @JsonProperty("uid")
    public String getId() {
        return super.getId();
    }

    @Override
    @JsonProperty("name")
    public String getName() {
        return super.getName();
    }
}
