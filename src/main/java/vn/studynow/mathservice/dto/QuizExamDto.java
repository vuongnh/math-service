package vn.studynow.mathservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuizExamDto extends BaseDto {

    @JsonProperty("question_image_url")
    private String imgId;

    @JsonProperty("answer_image_url")
    private String answerImageUrl;

    @JsonProperty("question_text")
    private String question;

    @JsonProperty("lol_type")
    private String lolType = "lol4";

    private String lolUId;

    @JsonProperty("lol_uid")
    public String getLolUId() {
        return super.getId().substring(0, 10);
    }

    @JsonProperty("question_type")
    private String typeQuestion = "Quiz";

    @JsonProperty("answers")
    private List<AnswerDto> answers = new ArrayList<>();

    @JsonIgnore
    private String answersA;

    @JsonIgnore
    private String answersB;

    @JsonIgnore
    private String answersC;

    @JsonIgnore
    private String answersD;

    @JsonProperty("answer_text")
    private String answerDetail;

    @Override
    @JsonIgnore
    public String getName() {
        return super.getName();
    }

    @Override
    @JsonProperty("question_id")
    public String getId() {
        return super.getId();
    }
}
