package vn.studynow.mathservice.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;
}
