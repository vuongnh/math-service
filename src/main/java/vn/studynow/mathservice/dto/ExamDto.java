package vn.studynow.mathservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class ExamDto extends BaseDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty("time_limit")
    private Integer timeLimit = 3600;

    @JsonProperty("num_question")
    private Integer numQuestion = 50;

    private Long code;

    @JsonProperty("event_name")
    private String eventName;

    @JsonProperty("exam_name")
    private String examName;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    private Integer grade;

    @JsonProperty("subject_id")
    private String subjectId;

    @JsonProperty("cycle_id")
    private String cycleId;

    @JsonProperty("question_params")
    private List<QuizExamDto> exams = new ArrayList<>();

    private String version;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @JsonProperty("study_type")
    private String studyType;

    @JsonProperty("material_code")
    private String materialCode;

    @Override
    @JsonProperty("_id")
    public String getId() {
        return super.getId();
    }

    @Override
    @JsonIgnore
    public String getName() {
        return super.getName();
    }
}
