package vn.studynow.mathservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LOLDto {
    private static final long serialVersionUID = 1L;

    private Long code;

    @JsonProperty("event_name")
    private String eventName;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("device_id")
    private String deviceId;

    private Integer grade;

    @JsonProperty("subject_id")
    private String subjectId;

    @JsonProperty("lol0_uid")
    private String lol0UId;

    @JsonProperty("list_lol1")
    private List<LOL1Dto> listLol1 = new ArrayList<>();

    private String version;

    @JsonProperty("device_type")
    private String deviceType;

    @JsonProperty("event_timestamp")
    private Long eventTimestamp;

    @JsonProperty("study_type")
    private String studyType;
}
