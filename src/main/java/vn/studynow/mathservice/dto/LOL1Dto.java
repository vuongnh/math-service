package vn.studynow.mathservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LOL1Dto extends BaseDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty("children")
    private List<LOL2Dto> listLol2 = new ArrayList<>();

    @Override
    @JsonProperty("uid")
    public String getId() {
        return super.getId();
    }

    @Override
    @JsonProperty("name")
    public String getName() {
        return super.getName();
    }

}
