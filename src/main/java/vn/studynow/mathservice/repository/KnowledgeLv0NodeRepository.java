package vn.studynow.mathservice.repository;

import vn.studynow.mathservice.model.KnowledgeLevel0Node;

import java.util.List;

public interface KnowledgeLv0NodeRepository {
    Object getKnowledgeLv0Nodes(String userId, String deviceId, String deviceType) ;
}

