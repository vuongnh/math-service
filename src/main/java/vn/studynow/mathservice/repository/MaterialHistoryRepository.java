package vn.studynow.mathservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.history.MaterialHistory;

import java.util.Optional;


@Repository
public interface MaterialHistoryRepository extends MongoRepository<MaterialHistory, String> {
    Optional<MaterialHistory> findMaterialHistoryByUserIdAndDeviceIdAndMaterialCode(String userId, String deviceId, String materialCode);
}
