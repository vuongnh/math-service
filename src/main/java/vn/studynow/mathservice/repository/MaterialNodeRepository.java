package vn.studynow.mathservice.repository;

import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.MaterialNode;

import java.util.List;
import java.util.Optional;


@Repository
public interface MaterialNodeRepository {
    Optional<MaterialNode> getMaterial(String userId, String deviceId, String deviceType, String typesName, String uid) ;
    List<MaterialNode> getMaterials(String userId, String deviceId, String deviceType, String typesName, String uid);
}
