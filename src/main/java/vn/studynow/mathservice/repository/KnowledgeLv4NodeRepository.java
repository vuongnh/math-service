package vn.studynow.mathservice.repository;

import vn.studynow.mathservice.model.KnowledgeLevel4Node;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.payload.response.LearningPathResponse;
import vn.studynow.mathservice.payload.response.ResponseFromLearningService;

import java.util.List;

public interface KnowledgeLv4NodeRepository {
    ResponseFromLearningService<LearningPathResponse> getKnowledgeLevel4Nodes(GetLearningPathRequest getLearningPathRequest) throws Exception;



}
