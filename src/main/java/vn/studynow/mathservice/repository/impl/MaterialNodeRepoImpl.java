package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.model.MaterialNode;
import vn.studynow.mathservice.repository.MaterialNodeRepository;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.util.*;

@Repository
public class MaterialNodeRepoImpl implements MaterialNodeRepository {

    private final Logger log = LoggerFactory.getLogger(MaterialNodeRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public Optional<MaterialNode> getMaterial(String userId, String deviceId, String deviceType, String typesName, String uid){

        Map<String, String> params = new HashMap<>();
        LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
        LearningServiceUtils.setCommonMaterialParams(params, typesName, uid);

        ResponseEntity responseEntity = LearningServiceUtils.callApi(
                applicationProperties.getLearningService().getApiGetMaterialDetail()
                , params
                , HttpMethod.GET
                ,null
        );

        if (LearningServiceUtils.isApiSuccess(responseEntity)) {
            String data = JsonParserUtils.getJsonString(
                    Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_MATERIAL_DETAIL_BODY_DATA);

            MaterialNode materialNode = null;
            try {
                materialNode = objectMapper.readValue(data, MaterialNode.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return Optional.ofNullable(materialNode);
        }
        return null;

    }

    @Override
    public List<MaterialNode> getMaterials(String userId, String deviceId, String deviceType, String typesName, String uid) {

        Map<String, String> params = new HashMap<>();
        LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
        LearningServiceUtils.setCommonMaterialParams(params, typesName, uid);

        ResponseEntity responseEntity = LearningServiceUtils.callApi(
                applicationProperties.getLearningService().getApiGetLearningNodeMaterials()
                , params
                , HttpMethod.GET
                , null
        );

        if (LearningServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_MATERIALS_BODY_DATA);
            try {
                return objectMapper.readValue(data, new TypeReference<List<MaterialNode>>() {});
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
        }

        return null;
    }
}
