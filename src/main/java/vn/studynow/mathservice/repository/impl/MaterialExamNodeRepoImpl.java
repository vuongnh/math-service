package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.model.MaterialExamNode;
import vn.studynow.mathservice.repository.MaterialExamNodeRepository;
import vn.studynow.mathservice.util.CallApiUtil;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Repository
public class MaterialExamNodeRepoImpl implements MaterialExamNodeRepository {


    private final Logger log = LoggerFactory.getLogger(MaterialExamNodeRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public Optional<MaterialExamNode> getEntranceExam(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception {
        try {
            String url = applicationProperties.getLearningService().getApiGetEntranceExam();

            MaterialExamNode materialExamNode = getExam(userId, deviceId, deviceType, typesName, uid, url);

            return Optional.ofNullable(materialExamNode);
        } catch (Exception e) {
            log.error("error at getEntranceExam: " + e.getMessage());

            throw e;
        }
    }

    @Override
    public Optional<MaterialExamNode> getFinalExam(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception {
        try {
            String url = applicationProperties.getLearningService().getApiGetFinalExam();

            MaterialExamNode materialExamNode = getExam(userId, deviceId, deviceType, typesName, uid, url);

            return Optional.ofNullable(materialExamNode);
        } catch (Exception e) {
            log.error("error at getFinalExam: " + e.getMessage());

            throw e;
        }
    }

    private MaterialExamNode getExam(String userId, String deviceId, String deviceType, String typesName, String uid, String url) throws Exception {
        try {
            Map<String, String> params = new HashMap<>();
            LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
            LearningServiceUtils.setCommonMaterialParams(params, typesName, uid);

            ResponseEntity responseEntity = CallApiUtil.callApi(
                    url,
                    params,
                    HttpMethod.GET
                    , null);

            if (LearningServiceUtils.isApiSuccess(responseEntity)) {
                String exam = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_EXAM_BODY_DATA);

                return new ObjectMapper().readValue(exam, MaterialExamNode.class);
            } else {
                throw new Exception("Invalid response!");
            }
        } catch (Exception e) {
            log.error("error at getExam: " + e.getMessage());

            throw e;
        }
    }
}
