package vn.studynow.mathservice.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.payload.request.learning.GetLearningPathRequest;
import vn.studynow.mathservice.payload.response.LearningPathResponse;
import vn.studynow.mathservice.payload.response.ResponseFromLearningService;
import vn.studynow.mathservice.repository.KnowledgeLv4NodeRepository;
import vn.studynow.mathservice.service.restrequest.RestRequest;

@Repository
public class KnowledgeLv4NodeRepoImpl implements KnowledgeLv4NodeRepository {

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    RestRequest restRequest;

    @Override
    public ResponseFromLearningService<LearningPathResponse> getKnowledgeLevel4Nodes(GetLearningPathRequest getLearningPathRequest) {
        return restRequest.get(
                applicationProperties.getLearningService().getApiGetLearningPath(),
                getLearningPathRequest, ResponseFromLearningService.class
        );
    }
}
