package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.model.UserAvatar;
import vn.studynow.mathservice.repository.UserAvatarRepository;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class UserAvatarRepoImpl implements UserAvatarRepository {

    private final Logger log = LoggerFactory.getLogger(UserAvatarRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public List<UserAvatar> getUserAvatar() throws Exception {
        try {
            Map<String, String> params = new HashMap<>();

            ResponseEntity responseEntity = LearningServiceUtils.callApi(
                    applicationProperties.getLearningService().getApiGetUserAvatar()
                    , params
                    , HttpMethod.GET
                    , null
            );

            if (LearningServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_USER_AVATAR);

                return objectMapper.readValue(data, new TypeReference<List<UserAvatar>>() {});
            } else {
                throw new Exception("Invalid response!");
            }
        } catch (Exception e) {
            log.error("Error at getUserAvatar:" + e.getMessage());

            throw e;
        }
    }
}
