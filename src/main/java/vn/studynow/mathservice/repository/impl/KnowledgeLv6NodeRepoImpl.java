package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.model.KnowledgeLevel4Node;
import vn.studynow.mathservice.model.KnowledgeLevel6Node;
import vn.studynow.mathservice.repository.KnowledgeLv6NodeRepository;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class KnowledgeLv6NodeRepoImpl implements KnowledgeLv6NodeRepository {

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    private final Logger log = LoggerFactory.getLogger(KnowledgeLevel4Node.class);

    @Override
    public List<KnowledgeLevel6Node> getKnowledgeLevel6Nodes(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception{
        try {
            Map<String, String> params = new HashMap<>();
            LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
            LearningServiceUtils.setCommonMaterialParams(params, typesName, uid);

            ResponseEntity responseEntity = LearningServiceUtils.callApi(
                    applicationProperties.getLearningService().getApiGetLearningNodesLevel6()
                    , params
                    , HttpMethod.GET
                    , null
            );
            if (LearningServiceUtils.isApiSuccess(responseEntity)) {

                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), "data");


                data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(data), ServiceConstant.LearningService.Response.API_GET_LEARNING_NODE_LEVEL6_BODY_DATA);

                return objectMapper.readValue(data, new TypeReference<List<KnowledgeLevel6Node>>() {});
            }  else {
                throw new Exception("Invalid response!");
            }
        } catch (Exception e){
            log.error("error at getKnowledgeLevel4Nodes: " + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }
}
