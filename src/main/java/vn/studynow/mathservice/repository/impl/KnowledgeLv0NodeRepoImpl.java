package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.repository.KnowledgeLv0NodeRepository;
import vn.studynow.mathservice.service.restrequest.RestRequest;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.util.HashMap;
import java.util.Map;

@Repository
public class KnowledgeLv0NodeRepoImpl implements KnowledgeLv0NodeRepository {

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RestRequest restRequest;


    @Override
    public Object getKnowledgeLv0Nodes(String userId, String deviceId, String deviceType){
        Map<String, String> params = new HashMap<>();
        LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
        return ((Map)restRequest.getData(applicationProperties.getLearningService().getApiGetLearningNodesLevel0(), params)).get("cycle");
    }
}
