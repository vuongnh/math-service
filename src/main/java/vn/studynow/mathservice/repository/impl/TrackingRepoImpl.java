package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.repository.TrackingRepository;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.TrackingServiceUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Repository
public class TrackingRepoImpl implements TrackingRepository {

    private final Logger log = LoggerFactory.getLogger(TrackingRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void trackingGetEntranceExam(String userId, String deviceId) throws Exception {
        try {
            String deviceType = "mobile";
            int grade = 12;
            int subjectId = 1;
            String version = "1.0";

            Map<String, String> params = new HashMap<>();
            TrackingServiceUtils.setCommonParams(params, userId, deviceId, deviceType, grade, subjectId, version);
            params.put(ServiceConstant.TrackingService.Request.PARAMETER_LOL0_UID, userId);
            params.put(ServiceConstant.TrackingService.Request.PARAMETER_LOL1_UID, userId);
            params.put(ServiceConstant.TrackingService.Request.PARAMETER_LOL2_UID, userId);


            ResponseEntity responseEntity = TrackingServiceUtils.callApi(
                    applicationProperties.getTrackingService().getApiTrackingGetEntranceExam()
                    , params
                    , HttpMethod.POST
                    , null
            );

            log.debug("Test {}", Objects.requireNonNull(responseEntity.getBody()).toString());

            if (TrackingServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_MATERIALS_BODY_DATA);
            } else {
                throw new Exception("Invalid response!");
            }
        } catch (Exception e) {
            log.error("Error at getMaterials:" + e.getMessage());

            throw e;
        }
    }
}
