package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.Parameter;
import vn.studynow.mathservice.common.constant.SearchServiceConstant;
import vn.studynow.mathservice.model.MathConcept;
import vn.studynow.mathservice.repository.MathConceptRepository;
import vn.studynow.mathservice.util.CallApiUtil;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.SearchServiceUtils;

import java.util.*;

@Repository
public class MathConceptRepoImpl implements MathConceptRepository {

    private final Logger log = LoggerFactory.getLogger(MathConceptRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public List<MathConcept> getMathConceptRecommend(String knowledgeName) throws Exception {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("knowledge_name", knowledgeName);

            ResponseEntity responseEntity = CallApiUtil.callApi(
                    applicationProperties.getSearchService().getApiMathConcept()
                    , params
                    , HttpMethod.GET
                    , null
            );

            if (SearchServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), SearchServiceConstant.DATA);

                return objectMapper.readValue(data, new TypeReference<List<MathConcept>>() {});
            } else {
                throw new Exception("Invalid response!");
            }

        } catch (Exception e) {
            log.error("Error at getMathConceptRecommend:" + e.getMessage());

            throw e;
        }
    }

    @Override
    public Optional<MathConcept> findById(String id) throws Exception {
        try {
            Map<String, String> params = new HashMap<>();

            String url = applicationProperties.getSearchService().getApiMathConcept() + id;

            ResponseEntity responseEntity = CallApiUtil.callApi(
                    url
                    , params
                    , HttpMethod.GET
                    , null
            );

            if (SearchServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), SearchServiceConstant.DATA);

                MathConcept mathConcept = objectMapper.readValue(data, MathConcept.class);

                return Optional.of(mathConcept);
            } else {
                throw new Exception("Invalid response!");
            }

        } catch (Exception e) {
            log.error("Error at findById:" + e.getMessage());

            throw e;
        }
    }
}
