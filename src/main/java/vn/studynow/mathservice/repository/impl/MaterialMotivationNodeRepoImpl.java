package vn.studynow.mathservice.repository.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.config.ApplicationProperties;
import vn.studynow.mathservice.common.constant.ServiceConstant;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.repository.MaterialMotivationNodeRepository;
import vn.studynow.mathservice.util.JsonParserUtils;
import vn.studynow.mathservice.util.LearningServiceUtils;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Repository
public class MaterialMotivationNodeRepoImpl implements MaterialMotivationNodeRepository {

    private final Logger log = LoggerFactory.getLogger(MaterialMotivationNodeRepoImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public Optional<MaterialEntranceExamMotivationNode> getMaterialEntranceExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body) {

        String url = applicationProperties.getLearningService().getApiGetEntranceExamMotivation();

        MaterialEntranceExamMotivationNode materialEntranceExamMotivationNode = null;
        try {
            materialEntranceExamMotivationNode = getMaterialExamMotivationNode(userId, deviceId, deviceType, cycleNodeLevel2Uid, cycleNodeLevel2TypesName, cycleNodeUid, cycleNodeTypesName, totalQuestion, correctAnswer, materialCode, timeLimit, timeElapsed, url, body, MaterialEntranceExamMotivationNode.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(materialEntranceExamMotivationNode);
    }

    // Motivation of final LOL2's exam
    @Override
    public Optional<MaterialFinalExamMotivationNode> getMaterialFinalExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body){

        String url = applicationProperties.getLearningService().getApiGetFinalExamMotivation();

        MaterialFinalExamMotivationNode materialFinalExamMotivationNode =
                null;
        try {
            materialFinalExamMotivationNode = getMaterialExamMotivationNode(userId, deviceId, deviceType,
                    cycleNodeLevel2Uid, cycleNodeLevel2TypesName, cycleNodeUid,
                    cycleNodeTypesName, totalQuestion, correctAnswer,
                    materialCode, timeLimit, timeElapsed, url, body, MaterialFinalExamMotivationNode.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(materialFinalExamMotivationNode);

    }

    private <T extends MaterialMotivationNode> T getMaterialExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, String url, Object body, Class<T> valueType) throws Exception{
        try {
            Map<String, String> params = new HashMap<>();
            LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);

            params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_LEVEL2_UID, cycleNodeLevel2Uid);
            params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_LEVEL2_TYPES_NAME, cycleNodeLevel2TypesName);
            params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_NODE_UID, cycleNodeUid);
            params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_TYPES_NAME, cycleNodeTypesName);
            params.put(ServiceConstant.LearningService.Request.PARAMETER_MATERIAL_CODE, materialCode);
            params.put(ServiceConstant.LearningService.Request.PARAMETER_TIME_LIMIT, Integer.toString(timeLimit));
            params.put(ServiceConstant.LearningService.Request.PARAMETER_TIME_ELAPSED, Integer.toString(timeElapsed));
            params.put(ServiceConstant.LearningService.Request.PARAMETER_TOTAL_QUESTION, Integer.toString(totalQuestion));
            params.put(ServiceConstant.LearningService.Request.PARAMETER_CORRECT_ANSWER, Integer.toString(correctAnswer));

            ResponseEntity responseEntity = LearningServiceUtils.callApi(
                    url
                    , params
                    , HttpMethod.POST
                    , body
            );

            if (LearningServiceUtils.isApiSuccess(responseEntity)) {
                String data = JsonParserUtils.getJsonString(
                        Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_EXAM_MOTIVATION_BODY_DATA);

                return objectMapper.readValue(data, valueType);
            } else {
                throw new Exception("Invalid response!");
            }
        } catch (Exception e) {
            log.error("Error at getMaterialExamMotivationNode:" + e.getMessage());

            throw e;
        }
    }

    @Override
    public Optional<MaterialPractice4MotivationNode> getMaterialPractice4MotivationNode(String userId, String deviceId, String deviceType, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer answeredQuestion, Integer correctAnswer, String materialCode, Integer timeElapsed, Object body) {

        Map<String, String> params = new HashMap<>();
        LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);

        params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_NODE_UID, cycleNodeUid);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_CYCLE_TYPES_NAME, cycleNodeTypesName);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_TOTAL_QUESTION, Integer.toString(totalQuestion));
        params.put(ServiceConstant.LearningService.Request.PARAMETER_ANSWERED_QUESTION, Integer.toString(answeredQuestion));
        params.put(ServiceConstant.LearningService.Request.PARAMETER_CORRECT_ANSWER, Integer.toString(correctAnswer));
        params.put(ServiceConstant.LearningService.Request.PARAMETER_MATERIAL_CODE, materialCode);
        params.put(ServiceConstant.LearningService.Request.PARAMETER_TIME_ELAPSED, Integer.toString(timeElapsed));

        ResponseEntity responseEntity = LearningServiceUtils.callApi(
                applicationProperties.getLearningService().getApiGetPracticeMotivation()
                , params
                , HttpMethod.POST
                , body
        );

        if (LearningServiceUtils.isApiSuccess(responseEntity)) {
            String data = JsonParserUtils.getJsonString(
                    Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_PRACTICE_MOTIVATION_BODY_DATA);

            MaterialPractice4MotivationNode materialPractice4MotivationNode = null;
            try {
                materialPractice4MotivationNode = objectMapper.readValue(data, MaterialPractice4MotivationNode.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return Optional.ofNullable(materialPractice4MotivationNode);
        }

        return null;

    }

    @Override
    public Optional<MaterialPractice61MotivationNode> getMaterialPractice61MotivationNode(PracticeMotivationParam practiceMotivationParam, Object body) {
        try {
            String url = applicationProperties.getLearningService().getApiGetLearningNodeLevel61Motivation();

            MaterialPractice61MotivationNode materialPractice61MotivationNode = getMaterialPracticeMotivationNode(
                    url, practiceMotivationParam, body, MaterialPractice61MotivationNode.class);

            return Optional.ofNullable(materialPractice61MotivationNode);
        } catch (Exception e) {
            log.error("error at getMaterialPractice61MotivationNode: " + e.getMessage());

            throw e;
        }
    }

    @Override
    public Optional<MaterialPractice62MotivationNode> getMaterialPractice62MotivationNode(PracticeMotivationParam practiceMotivationParam, Object body) {
        String url = applicationProperties.getLearningService().getApiGetLearningNodeLevel62Motivation();
        MaterialPractice62MotivationNode materialPractice62MotivationNode =
                getMaterialPracticeMotivationNode( url, practiceMotivationParam,  body, MaterialPractice62MotivationNode.class);
        return Optional.ofNullable(materialPractice62MotivationNode);
    }

    @Override
    public Optional<MaterialPractice63MotivationNode> getMaterialPractice63MotivationNode(PracticeMotivationParam practiceMotivationParam,  Object body){
        String url = applicationProperties.getLearningService().getApiGetLearningNodeLevel63Motivation();

        MaterialPractice63MotivationNode materialPractice63MotivationNode =
                getMaterialPracticeMotivationNode( url, practiceMotivationParam,
                        body, MaterialPractice63MotivationNode.class);

        return Optional.ofNullable(materialPractice63MotivationNode);
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class PracticeMotivationParam {
        String userId;
        String deviceId;
        String deviceType;
        String cycleNodeUid;
        String cycleNodeTypesName;
        String lol4CycleUid;  //lol4_cycle_uid
        String lol4CycleTypesName;    // lol4_cycle_types_name
        Integer totalQuestion;
        Integer answeredQuestion;
        Integer correctAnswer;
        String materialCode;
        Integer timeElapsed;
        String url;
        String eventTimestamp =  Long.toString((new Timestamp(System.currentTimeMillis())).getTime() / 1000);
        String studyType;
        Long code;

    }


    public <T extends MaterialMotivationNode> T getMaterialPracticeMotivationNode(String url, PracticeMotivationParam practiceMotivationParam, Object body, Class<T> valueType){
        Map params =   objectMapper.convertValue(practiceMotivationParam, Map.class);
        ResponseEntity responseEntity = LearningServiceUtils.callApi(
                url
                , params
                , HttpMethod.POST
                , body
        );

        if (LearningServiceUtils.isApiSuccess(responseEntity)) {
            String data = JsonParserUtils.getJsonString(
                    Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_PRACTICE_MOTIVATION_BODY_DATA);

            try {
                return objectMapper.readValue(data, valueType);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public Optional<MaterialEntranceExamMotivationPopupNode> getMaterialEntranceExamMotivationPopupNode(String userId, String deviceId, String deviceType, String typesName, String uid) {

        Map<String, String> params = new HashMap<>();
        LearningServiceUtils.setCommonParams(params, userId, deviceId, deviceType);
        LearningServiceUtils.setCommonMaterialParams(params, typesName, uid);

        ResponseEntity responseEntity = LearningServiceUtils.callApi(
                applicationProperties.getLearningService().getApiGetEntranceExamPopup()
                , params
                , HttpMethod.GET
                , null
        );

        if (LearningServiceUtils.isApiSuccess(responseEntity)) {
            String data = JsonParserUtils.getJsonString(
                    Objects.requireNonNull(responseEntity.getBody()).toString(), ServiceConstant.LearningService.Response.API_GET_ENTRANCE_EXAM_POPUP_BODY_DATA);

            MaterialEntranceExamMotivationPopupNode materialEntranceExamMotivationPopupNode = null;
            try {
                materialEntranceExamMotivationPopupNode = objectMapper.readValue(data, MaterialEntranceExamMotivationPopupNode.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return Optional.ofNullable(materialEntranceExamMotivationPopupNode);
        }

        return null;
    }

}
