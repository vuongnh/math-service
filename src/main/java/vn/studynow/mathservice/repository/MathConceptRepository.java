package vn.studynow.mathservice.repository;

import vn.studynow.mathservice.model.MathConcept;

import java.util.List;
import java.util.Optional;

public interface MathConceptRepository {

    List<MathConcept> getMathConceptRecommend(String knowledgeName) throws Exception;

    Optional<MathConcept> findById(String id) throws Exception;
}
