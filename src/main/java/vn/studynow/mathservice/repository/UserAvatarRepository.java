package vn.studynow.mathservice.repository;

import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.UserAvatar;

import java.util.List;

@Repository
public interface UserAvatarRepository {
    List<UserAvatar> getUserAvatar() throws Exception;
}
