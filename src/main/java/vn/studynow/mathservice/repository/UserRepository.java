package vn.studynow.mathservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.profile.User;


@Repository
public interface UserRepository extends MongoRepository<User,String> {
}
