package vn.studynow.mathservice.repository;

import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.KnowledgeLevel0Node;
import vn.studynow.mathservice.model.KnowledgeLevel4Node;

import java.util.List;


@Repository
public interface LearningNodeRepository {
    List<KnowledgeLevel0Node> getLearningCycleLevel2();
    List<KnowledgeLevel4Node> getLearningPath(String cycleId, String typesName, String uid);
}
