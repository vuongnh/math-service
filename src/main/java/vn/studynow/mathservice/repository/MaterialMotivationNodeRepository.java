package vn.studynow.mathservice.repository;

import org.springframework.stereotype.Repository;
import vn.studynow.mathservice.model.*;
import vn.studynow.mathservice.repository.impl.MaterialMotivationNodeRepoImpl;

import java.util.Optional;


@Repository
public interface MaterialMotivationNodeRepository {
    Optional<MaterialPractice4MotivationNode> getMaterialPractice4MotivationNode(String userId, String deviceId, String deviceType, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer answeredQuestion, Integer correctAnswer, String materialCode, Integer timeElapsed, Object body);
    Optional<MaterialPractice61MotivationNode> getMaterialPractice61MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body);
    Optional<MaterialPractice62MotivationNode> getMaterialPractice62MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body);
    Optional<MaterialPractice63MotivationNode> getMaterialPractice63MotivationNode(MaterialMotivationNodeRepoImpl.PracticeMotivationParam practiceMotivationParam, Object body);
    Optional<MaterialEntranceExamMotivationNode> getMaterialEntranceExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body);
    Optional<MaterialFinalExamMotivationNode> getMaterialFinalExamMotivationNode(String userId, String deviceId, String deviceType, String cycleNodeLevel2Uid, String cycleNodeLevel2TypesName, String cycleNodeUid, String cycleNodeTypesName, Integer totalQuestion, Integer correctAnswer, String materialCode, Integer timeLimit, Integer timeElapsed, Object body);
    Optional<MaterialEntranceExamMotivationPopupNode> getMaterialEntranceExamMotivationPopupNode(String userId, String deviceId, String deviceType, String typesName, String uid);
}
