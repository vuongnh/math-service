package vn.studynow.mathservice.repository;

import vn.studynow.mathservice.model.MaterialExamNode;

import java.util.Optional;

public interface MaterialExamNodeRepository {

    Optional<MaterialExamNode> getEntranceExam(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception;
    Optional<MaterialExamNode> getFinalExam(String userId, String deviceId, String deviceType, String typesName, String uid) throws Exception;

}
