package vn.studynow.mathservice.repository.learning;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.studynow.mathservice.domain.learning.entities.BookmarkedLOL4;

public interface BookmarkedLOL4Repository extends MongoRepository<BookmarkedLOL4, String> {
    Page<BookmarkedLOL4> findByUserIdAndBookmarkedIs(String userId, boolean isBookmarked, Pageable pageable);


}
