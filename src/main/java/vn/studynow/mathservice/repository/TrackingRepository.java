package vn.studynow.mathservice.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface TrackingRepository {
    void trackingGetEntranceExam(String userId, String deviceId) throws Exception;
}
