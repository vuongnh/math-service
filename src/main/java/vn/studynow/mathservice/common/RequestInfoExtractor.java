package vn.studynow.mathservice.common;


/*
 vai trò :
 - Extract request info to Map
 - Request Info :
    + Query
    + Body
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class RequestInfoExtractor {

    private Map<String, Object> requestMap = new HashMap<>();
    private Map<String, Object> queryInfo = new HashMap<>();
    private Map<String, Object> bodyInfo = new HashMap<>();

    public void extract(HttpServletRequest request, HttpMethod method, String body) throws JsonProcessingException {
        if (method.matches("GET")) {
            String queryString = request.getQueryString();
            requestMap.putAll(extractQueryString(queryString));
        }

        if (method.matches("POST")) {
            requestMap.putAll(extractBody(body));
        }
    }

    private Map<String, String> extractQueryString(String queryString) {
        Map<String, String> queryExtracted = new HashMap<>();

        String[] pair = queryString.split("&");

        for (String item : pair) {
            String[] keyValue = item.split("=");
            queryExtracted.put(keyValue[0], keyValue[1]);
        }

        return queryExtracted;
    }

    private Map extractBody(String body) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return  objectMapper.readValue(body, Map.class);
    }

    public Map<String, Object> getData() {
        return requestMap;
    }
}
