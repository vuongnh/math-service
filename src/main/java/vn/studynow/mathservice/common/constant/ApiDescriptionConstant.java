package vn.studynow.mathservice.common.constant;

public class ApiDescriptionConstant {

    public static final String GET_LEARNING_CYCLE_LEVEL_2_DESC =  "- pass_status: trạng thái học của node LOL2\n" +
            "2: Đã pass (Đã làm xong final exam + pass hết LOL4)\n" +
            "1: Đang học (Đã làm xong entrance exam + chưa làm final exam)\n" +
            "0: Chưa học";

    public static final String GET_LEARNING_CYCLE_LEVEL_4_DESC =  "- is_available: trạng thái hiển thị của node LOL4\n" +
            "0: Không hiển thị\n" +
            "1: Có thể hiển thị\n" +
            "\n" +
            "- pass_status: trạng thái học của node LOL4\n" +
            "1: Đã pass\n" +
            "0: Chưa học\n" +
            "-1: Đã học -> fail practice -> đang học LOL6\n" +
            "-2: Đã học -> fail practice -> xong LOL6 -> đang học lại LOL4";

    public static final String GET_LEARNING_CYCLE_LEVEL_6_DESC =  "- pass_status: trạng thái học của node LOL6\n" +
            "1: Đã pass\n" +
            "0: Chưa học\n" +
            "-1: Đã học -> fail practice\n" +
            "-2: Đã học -> fail practice -> xong LOL6 -> đang học lại LOL4";

    public static final String GET_PRACTICE_MOTIVATION_DESC =  "- is_pass: kết quả làm bài practice\n" +
            "1: Đã pass\n" +
            "0: Không pass";
}
