package vn.studynow.mathservice.common.constant;


public class MaterialConstant {

    public static final String TYPES_NAME_LECTURE_GROUP  = "Material.LectureGroup";
    public static final String TYPES_NAME_PRACTICE_GROUP  = "Material.PracticeGroup";
    public static final String TYPES_NAME_EXAM  = "Material.Exam";
    public static final String TYPES_NAME_EXAM_DRAG_DROP  = "Material.DragAndDrop";

    public static final String TYPE_ENTRANCE_EXAM  = "Entrance Exam";
    public static final String TYPE_FINAL_EXAM  = "Final Exam";

    public static final Integer ANSWER_QUIZ_TRUE_STATUS  = 1;
    public static final Integer ANSWER_QUIZ_WRONG_STATUS  = 0;

    public static final Float MAX_POINT = 10f;

    public static class Properties {
        public static final String TYPES_NAME = "types_name";
    }
}
