package vn.studynow.mathservice.common.constant;


public class ServiceConstant {

    public static class LearningService {

        public static class Request {
            public static final String PARAMETER_USER_ID = "user_id";
            public static final String PARAMETER_DEVICE_ID = "device_id";
            public static final String PARAMETER_DEVICE_TYPE = "device_type";
            public static final String PARAMETER_TIMESTAMP = "event_timestamp";
            public static final String PARAMETER_TYPES_NAME = "types_name";
            public static final String PARAMETER_UID = "uid";
            public static final String PARAMETER_CYCLE_NODE_UID = "cycle_node_uid";
            public static final String PARAMETER_CYCLE_TYPES_NAME = "cycle_node_types_name";
            public static final String PARAMETER_CYCLE_LEVEL4_UID = "lol4_cycle_uid";
            public static final String PARAMETER_CYCLE_LEVEL4_TYPES_NAME = "lol4_cycle_types_name";
            public static final String PARAMETER_CYCLE_LEVEL2_UID = "lol2_cycle_uid";
            public static final String PARAMETER_CYCLE_LEVEL2_TYPES_NAME = "lol2_cycle_types_name";
            public static final String PARAMETER_TOTAL_QUESTION = "total_question";
            public static final String PARAMETER_ANSWERED_QUESTION = "answered_question";
            public static final String PARAMETER_CORRECT_ANSWER = "correct_answer";
            public static final String PARAMETER_MATERIAL_CODE = "material_code";
            public static final String PARAMETER_TIME_LIMIT = "time_limit";
            public static final String PARAMETER_TIME_ELAPSED = "time_elapsed";
        }

        public static class Response {
            public static final String MESSAGE = "message";
            public static final String SUCCESS = "success";

            public static final String API_GET_LEARNING_NODE_LEVEL0_BODY_DATA = "cycle";
            public static final String API_GET_LEARNING_PATH_BODY_DATA = "lol4_nodes";
            public static final String API_GET_MATERIALS_BODY_DATA = "material_nodes";
            public static final String API_GET_MATERIAL_DETAIL_BODY_DATA = "material";
            public static final String API_GET_EXAM_MOTIVATION_BODY_DATA = "motivation";
            public static final String API_GET_PRACTICE_MOTIVATION_BODY_DATA = "motivation";
            public static final String API_GET_EXAM_BODY_DATA = "exam";
            public static final String API_GET_ENTRANCE_EXAM_POPUP_BODY_DATA = "popup";
            public static final String API_GET_LEARNING_NODE_LEVEL6_BODY_DATA = "nodes";
            public static final String API_GET_USER_AVATAR = "materials";
        }
    }

    public static class TrackingService {

        public static class Request {
            public static final String PARAMETER_USER_ID = "user_id";
            public static final String PARAMETER_DEVICE_ID = "device_id";
            public static final String PARAMETER_TIMESTAMP = "event_timestamp";
            public static final String PARAMETER_DEVICE_TYPE = "device_type";
            public static final String PARAMETER_GRADE = "grade";
            public static final String PARAMETER_SUBJECT_ID = "subject_id";
            public static final String PARAMETER_VERSION = "version";


            public static final String PARAMETER_LOL0_UID = "lol0_uid";
            public static final String PARAMETER_LOL1_UID = "lol1_uid";
            public static final String PARAMETER_LOL2_UID = "lol2_uid";
        }

        public static class Response {
            public static final String MESSAGE = "message";
            public static final String SUCCESS = "success";

            public static final String API_GET_LEARNING_NODE_LEVEL0_BODY_DATA = "cycle";
        }
    }
}
