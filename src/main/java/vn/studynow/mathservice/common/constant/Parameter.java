package vn.studynow.mathservice.common.constant;

public class Parameter {

    public static final String USER_ID = "user-id";
    public static final String DEVICE_ID = "device-id";
    public static final String DEVICE_TYPE = "device-type";

    public static final String TYPES_NAME = "types_name";
    public static final String UID = "uid";
    public static final String CYCLE_ID = "cycle_id";

    public static final String EVENT_TIMESTAMP = "event_timestamp";
    public static final String GRADE = "grade";
    public static final String SUBJECT_ID = "subject_id";

    public static final String CODE = "code";

    public static final String LOL0_UID = "lol0_uid";
    public static final String LOL1_UID = "lol1_uid";
    public static final String LOL2_UID = "lol2_uid";
    public static final String LOL3_UID = "lol3_uid";

    public static final String SEARCH_KEY = "key";
    public static final String ID = "id";

    public static final String IS_RELEARN = "is_relearn";
    public static final String MATERIAL_ID = "material_id";

    public static final String SUB = "sub";

    public static final String REQUEST_PARAM_USER_ID = "user_id";
    public static final String REQUEST_PARAM_DEVICE_ID = "device_id";
    public static final String REQUEST_PARAM_DEVICE_TYPE = "device_type";
    public static final String SEARCH_NAME = "search_name";

    public static final String EVENT_NAME = "event_name";
    public static final String EVENT_DATE = "event_date";
    public static final String LOL4_UID = "lol4_uid";
    public static final String LOL6_1_UID = "lol6_1_uid";
    public static final String LOL6_2_UID = "lol6_2_uid";
    public static final String LOL6_3_UID = "lol6_3_uid";
    public static final String VERSION = "version";
    public static final String STUDY_TYPE = "study_type";

    public static final String TIME_LIMIT = "time_limit";
    public static final String NUM_QUESTION = "num_question";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
}
