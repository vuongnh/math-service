package vn.studynow.mathservice.common.constant;


public class LearningNodeConstant {
    public static final Integer LEARNING_PATH_IS_AVAILABLE  = 1;
    public static final Integer LEARNING_PATH_PASSED  = 1;

    public static final Integer LEARNING_NODE_LEVEL2_PASSED  = 2;
    public static final Integer LEARNING_NODE_LEVEL2_LEARNING  = 1;
    public static final Integer LEARNING_NODE_LEVEL2_NOT_LEARN  = 0;

    public static final String TYPES_NAME_KNOWLEDGE61  = "Knowledge.Six_one";
    public static final String TYPES_NAME_KNOWLEDGE62  = "Knowledge.Six_two";
    public static final String TYPES_NAME_KNOWLEDGE63  = "Knowledge.Six_three";

    public static class Properties {
        public static final String KNOWLEDGE = "knowledge";
        public static final String TYPES_NAME = "types_name";

    }
}
