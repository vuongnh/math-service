package vn.studynow.mathservice.common.constant;


public class TrackingConstant {

    public static final String LOL2_INPUT_CLICK_START  = "LOL2_INPUT_CLICK_START";
    public static final String LOL2_INPUT_GET_EXAM  = "LOL2_INPUT_GET_EXAM";
    public static final String LOL2_INPUT_SUBMIT_EXAM  = "LOL2_INPUT_SUBMIT_EXAM";
    public static final String LOL2_INPUT_CLICK_NEXT_LOL2  = "LOL2_INPUT_CLICK_NEXT_LOL2";
    public static final String LOL2_INPUT_CLICK_BUTTON_CONTINUE_PRACTICE  = "LOL2_INPUT_CLICK_BUTTON_CONTINUE_PRACTICE";
    public static final String LOL2_INPUT_CLICK_BUTTON_VIEW_DETAIL_ANSWER  = "LOL2_INPUT_CLICK_BUTTON_VIEW_DETAIL_ANSWER";

    public static final String LOL4_CLICK_START  = "LOL4_CLICK_START";
    public static final String LOL4_START_VIDEO  = "LOL4_START_VIDEO";
    public static final String LOL4_END_VIDEO  = "LOL4_END_VIDEO";
    public static final String LOL4_GET_PRACTICE  = "LOL4_GET_PRACTICE";
    public static final String LOL4_SUBMIT_PRACTICE  = "LOL4_SUBMIT_PRACTICE";
    public static final String LOL4_CLICK_BUTTON_CONTINUE_PRACTICE  = "LOL4_CLICK_BUTTON_CONTINUE_PRACTICE";
    public static final String LOL4_CLICK_BUTTON_VIEW_DETAIL_ANSWER  = "LOL4_CLICK_BUTTON_VIEW_DETAIL_ANSWER";
    public static final String LOL4_CLICK_BUTTON_VIEW_DETAIL_ANSWER_CONTINUE  = "LOL4_VIEW_ANSWER_CONTINUE";
    public static final String LOL4_DONE_LOL4  = "LOL4_DONE_LOL4";

    public static final String LOL6_1_CLICK_START  = "LOL6_1_CLICK_START";
    public static final String LOL6_1_START_VIDEO  = "LOL6_1_START_VIDEO";
    public static final String LOL6_1_END_VIDEO  = "LOL6_1_END_VIDEO";
    public static final String LOL6_1_GET_QUIZ  = "LOL6_1_GET_QUIZ";
    public static final String LOL6_1_SUBMIT_QUIZ  = "LOL6_1_SUBMIT_QUIZ";
    public static final String LOL6_1_VIEW_SOLUTION_DETAIL  = "LOL6_1_VIEW_SOLUTION_DETAIL";
    public static final String LOL6_1_VIEW_SOLUTION_DETAIL_CONTINUE  = "LOL6_1_VIEW_SOLUTION_DETAIL_CONTINUE";
    public static final String LOL6_1_DONE  = "LOL6_1_DONE";

    public static final String LOL6_2_CLICK_START  = "LOL6_2_CLICK_START";
    public static final String LOL6_2_START_VIDEO  = "LOL6_2_START_VIDEO";
    public static final String LOL6_2_END_VIDEO  = "LOL6_2_END_VIDEO";
    public static final String LOL6_2_GET_QUIZ  = "LOL6_2_GET_QUIZ";
    public static final String LOL6_2_SUBMIT_QUIZ  = "LOL6_2_SUBMIT_QUIZ";
    public static final String LOL6_2_VIEW_SOLUTION_DETAIL  = "LOL6_2_VIEW_SOLUTION_DETAIL";
    public static final String LOL6_2_VIEW_SOLUTION_DETAIL_CONTINUE  = "LOL6_2_VIEW_SOLUTION_DETAIL_CONTINUE";
    public static final String LOL6_2_DONE  = "LOL6_2_DONE";

    public static final String LOL6_3_CLICK_START  = "LOL6_3_CLICK_START";
    public static final String LOL6_3_START_VIDEO  = "LOL6_3_START_VIDEO";
    public static final String LOL6_3_END_VIDEO  = "LOL6_3_END_VIDEO";
    public static final String LOL6_3_GET_QUIZ  = "LOL6_3_GET_QUIZ";
    public static final String LOL6_3_SUBMIT_QUIZ  = "LOL6_3_SUBMIT_QUIZ";
    public static final String LOL6_3_VIEW_SOLUTION_DETAIL  = "LOL6_3_VIEW_SOLUTION_DETAIL";
    public static final String LOL6_3_VIEW_SOLUTION_DETAIL_CONTINUE  = "LOL6_3_VIEW_SOLUTION_DETAIL_CONTINUE";
    public static final String LOL6_3_DONE  = "LOL6_3_DONE";

    public static final String LOL2_OUTPUT_GET_EXAM  = "LOL2_OUTPUT_GET_EXAM";
    public static final String LOL2_OUTPUT_SUBMIT_EXAM  = "LOL2_OUTPUT_SUBMIT_EXAM";
    public static final String LOL2_OUTPUT_CLICK_NEXT_LOL2  = "LOL2_OUTPUT_CLICK_NEXT_LOL2";
    public static final String LOL2_OUTPUT_CLICK_BUTTON_VIEW_DETAIL_ANSWER  = "LOL2_OUTPUT_CLICK_BUTTON_VIEW_DETAIL_ANSWER";
}
